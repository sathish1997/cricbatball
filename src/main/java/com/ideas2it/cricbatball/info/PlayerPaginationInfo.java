package com.ideas2it.cricbatball.info;

import java.util.List;

import org.json.JSONArray;

public class PlayerPaginationInfo {
    
    private int page;
    private int totalPages;
    private JSONArray players;
    private List<PlayerInfo> playersInfo;

    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }    

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setPlayers(JSONArray players) {
        this.players = players;
    }

    public JSONArray getPlayers() {
        return players;
    }

    public void setPlayersInfo(List<PlayerInfo> playersInfo) {
        this.playersInfo = playersInfo;
    }

    public List<PlayerInfo> getPlayersInfo() {
        return playersInfo;
    }
}
