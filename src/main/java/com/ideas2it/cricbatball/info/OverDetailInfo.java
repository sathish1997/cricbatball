package com.ideas2it.cricbatball.info;

import com.ideas2it.cricbatball.common.BallResult;

public class OverDetailInfo {

    private int id;
    private int runs;
    private int ballNumber;
    private int firstPlayerRuns;
    private int secondPlayerRuns;
    private int firstPlayerballs;
    private int secondPlayerBalls;
    private int firstPlayerSixes;
    private int firstPlayerFours;
    private int secondPlayerSixes;
    private int secondPlayerFours;
    private int overCountInfo;
    private int scoreCountInfo;
    private int wicketCountInfo;
    private int extrasCountInfo;
    private BallResult ballResult;
    private PlayerInfo firstBatsman;
    private PlayerInfo secondBatsman;
    private TeamInfo firstTeam;
    private TeamInfo secondTeam;
    private PlayerInfo bowler;
    private MatchInfo match;
    private OverInfo overInfo;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getRuns() {
        return runs;
    }

    public void setBallNumber(int ballNumber) {
        this.ballNumber = ballNumber;
    }

    public int getBallNumber() {
        return ballNumber;
    }

    public void setFirstPlayerRuns(int firstPlayerRuns) {
        this.firstPlayerRuns = firstPlayerRuns;
    }

    public int getFirstPlayerRuns() {
        return firstPlayerRuns;
    }

    public void setSecondPlayerRuns(int secondPlayerRuns) {
        this.secondPlayerRuns = secondPlayerRuns;
    }

    public int getSecondPlayerRuns() {
        return secondPlayerRuns;
    }

    public void setFirstPlayerballs(int firstPlayerballs) {
        this.firstPlayerballs = firstPlayerballs;
    }

    public int getFirstPlayerballs() {
        return firstPlayerballs;
    }

    public void setSecondPlayerBalls(int secondPlayerBalls) {
        this.secondPlayerBalls = secondPlayerBalls;
    }

    public int getSecondPlayerBalls() {
        return secondPlayerBalls;
    }

    public void setFirstPlayerSixes(int firstPlayerSixes) {
        this.firstPlayerSixes = firstPlayerSixes;
    }

    public int getFirstPlayerSixes() {
        return firstPlayerSixes;
    }

    public void setFirstPlayerFours(int firstPlayerFours) {
        this.firstPlayerFours = firstPlayerFours;
    }

    public int getFirstPlayerFours() {
        return firstPlayerFours;
    }

    public void setSecondPlayerSixes(int secondPlayerSixes) {
        this.secondPlayerSixes = secondPlayerSixes;
    }

    public int getSecondPlayerSixes() {
        return secondPlayerSixes;
    }

    public void setSecondPlayerFours(int secondPlayerFours) {
        this.secondPlayerFours = secondPlayerFours;
    }

    public int getSecondPlayerFours() {
        return secondPlayerFours;
    }

    public void setOverCountInfo(int overCountInfo) {
        this.overCountInfo = overCountInfo;
    }

    public int getOverCountInfo() {
        return this.overCountInfo;
    }

    public void setScoreCountInfo(int scoreCountInfo) {
        this.scoreCountInfo = scoreCountInfo;
    }

    public int getScoreCountInfo() {
        return scoreCountInfo;
    }

    public void setWicketCountInfo(int wicketCountInfo) {
        this.wicketCountInfo = wicketCountInfo;
    }

    public int getWicketCountInfo() {
        return wicketCountInfo;
    }

    public void setExtrasCountInfo(int extrasCountInfo) {
        this.extrasCountInfo = extrasCountInfo;
    }

    public int getExtrasCountInfo() {
        return extrasCountInfo;
    }

    public void setBallResult(BallResult ballResult) {
        this.ballResult = ballResult;
    }

    public BallResult getBallResult() {
        return ballResult;
    }

    public void setFirstBatsman(PlayerInfo firstBatsman) {
        this.firstBatsman = firstBatsman;
    }

    public PlayerInfo getFirstBatsman() {
        return firstBatsman;
    }

    public void setSecondBatsman(PlayerInfo secondBatsman) {
        this.secondBatsman = secondBatsman;
    }

    public PlayerInfo getSecondBatsman() {
        return secondBatsman;
    }

    public void setBowler(PlayerInfo bowler) {
        this.bowler = bowler;
    }

    public PlayerInfo getBowler() {
        return bowler;
    }

    public void setFirstTeam(TeamInfo firstTeam) {
        this.firstTeam = firstTeam;
    }

    public TeamInfo getFirstTeam() {
        return firstTeam;
    }

    public void setSecondTeam(TeamInfo secondTeam) {
        this.secondTeam = secondTeam;
    }

    public TeamInfo getSecondTeam() {
        return secondTeam;
    }

    public void setMatch(MatchInfo match) {
        this.match = match;
    }

    public MatchInfo getMatch() {
        return match;
    }

    public void setOverInfo(OverInfo overInfo) {
        this.overInfo = overInfo;
    }

    public OverInfo getOverInfo() {
        return overInfo;
    }
}
