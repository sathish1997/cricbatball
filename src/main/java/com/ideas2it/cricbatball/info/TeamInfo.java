package com.ideas2it.cricbatball.info;

import java.util.List;
import java.util.Set;
import java.util.HashSet;

import org.json.JSONArray;

import com.ideas2it.cricbatball.common.Country;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.entities.Team;

/**
 * Used to store the multiples values 
 * that are to be passed to team controller as Single Argument.
 *
 * @author    M.Sathish Varma created on 10th August 2019.
 */
public class TeamInfo {

    private int id;
    private String name;
    private Country country;
    Set<PlayerInfo> players = new HashSet<PlayerInfo>();
 
    /**
     * Getter and Setter methods.
     */

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Country getCountry() {
        return this.country;
    }

   public void setPlayers(Set<PlayerInfo> players) {
       this.players = players;
   }

   public Set<PlayerInfo> getPlayers() {
       return this.players;
   }
}
