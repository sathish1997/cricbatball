package com.ideas2it.cricbatball.info;

import java.util.List;

import org.json.JSONArray;

public class TeamPaginationInfo {
    
    private int id;
    private int page;
    private int totalPages;
    private JSONArray teams;
    private List<TeamInfo> teamsInfo;
    private int[] roleCount;
    private String status;
    private TeamInfo teamInfo;
    private List<PlayerInfo> playersNotInTeam;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }    

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTeams(JSONArray teams) {
        this.teams = teams;
    }

    public JSONArray getTeams() {
        return teams;
    }

    public void setTeamsInfo(List<TeamInfo> teamsInfo) {
        this.teamsInfo = teamsInfo;
    }

    public List<TeamInfo> getTeamsInfo() {
        return teamsInfo;
    }

    public void setRoleCount(int[] roleCount) {
        this.roleCount = roleCount;
    }

    public int[] getRoleCount() {
        return this.roleCount;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setTeamInfo(TeamInfo teamInfo) {
        this.teamInfo = teamInfo;
    }

    public TeamInfo getTeamInfo() {
        return teamInfo;
    }

    public void setPlayersNotInTeam(List<PlayerInfo> playersNotInTeam) {
        this.playersNotInTeam = playersNotInTeam;
    }

    public List<PlayerInfo> getPlayersNotInTeam() {
        return this.playersNotInTeam;
    }
}
