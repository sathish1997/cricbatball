package com.ideas2it.cricbatball.info;

import java.util.Set;
import java.util.HashSet;

import com.ideas2it.cricbatball.entities.OverDetail;
import com.ideas2it.cricbatball.entities.Match;

public class OverInfo {

    private int id;
    private int overNumber;
    private int runs;
    private int wicketCount;
    private int foursCount;
    private int sixesCount;
    private int noBallCount;
    private int widesCount;
    private int byesCount;
    private int legByesCount;
    private MatchInfo match;
    private Set<OverDetailInfo> overDetails;

    public void setId(int id) {
        this.id = id;
    }
 
    public int getId() {
        return id;
    }

    public void setOverNumber(int overNumber) {
        this.overNumber = overNumber;
    }
 
    public int getOverNumber() {
        return overNumber;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }
 
    public int getRuns() {
        return runs;
    }

    public void setWicketCount(int wicketCount) {
        this.wicketCount = wicketCount;
    }
 
    public int getWicketCount() {
        return wicketCount;
    }

    public void setFoursCount(int foursCount) {
        this.foursCount = foursCount;
    }
 
    public int getFoursCount() {
        return foursCount;
    }

    public void setSixesCount(int sixesCount) {
        this.sixesCount = sixesCount;
    }
 
    public int getSixesCount() {
        return sixesCount;
    }

    public void setNoBallCount(int noBallCount) {
        this.noBallCount = noBallCount;
    }
 
    public int getNoBallCount() {
        return noBallCount;
    }

    public void setWidesCount(int widesCount) {
        this.widesCount = widesCount;
    }
 
    public int getWidesCount() {
        return widesCount;
    }

    public void setByesCount(int byesCount) {
        this.byesCount = byesCount;
    }
 
    public int getByesCount() {
        return byesCount;
    }

    public void setLegByesCount(int legByesCount) {
        this.legByesCount = legByesCount;
    }
 
    public int getLegByesCount() {
        return legByesCount;
    }

    public void setMatch(MatchInfo match) {
        this.match = match;
    }
 
    public MatchInfo getMatch() {
        return match;
    }

    public void setOverDetails(Set<OverDetailInfo> overDetails) {
        this.overDetails = overDetails;
    }
 
    public Set<OverDetailInfo> getOverDetails() {
        return overDetails;
    }
}
