package com.ideas2it.cricbatball.info;

import java.util.List;

import com.ideas2it.cricbatball.common.Country;
import com.ideas2it.cricbatball.entities.Contact;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.entities.Team;
import com.ideas2it.cricbatball.utils.AgeUtil;

/**
 * Used to store the multiples values to be used in the player controller.
 *
 * @author    M.Sathish Varma created on 10th August 2019.
 */
public class PlayerInfo {

    private int contactId;
    private int pincode;
    private long mobileNumber;
    private String address;
    private int playerId;
    private boolean status;
    private int id;
    private String age;
    private String battingStyle;
    private String bowlingStyle;
    private Country country;
    private String dateOfBirth;
    private String name;
    private String role;
    private String imagePath;
    private TeamInfo teamInfo;
 
    /**
     * Getter and Setter methods.
     */

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public int getContactId() {
        return this.contactId;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public int getPincode() {
        return this.pincode;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public long getMobileNumber() {
        return this.mobileNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return this.address;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getStatus() {
        return this.status; 
    }

    public void setId(int id) {
        this.id = id;
    }
   
    public int getId() {
        return this.id;
    }
   
    public void setAge(String age) {
        this.age = age;
    }

    public String getAge() {
        return AgeUtil.calculateAge(this.dateOfBirth);
    }

    public void setBattingStyle(String battingStyle) {
        this.battingStyle = battingStyle;
    }

    public String getBattingStyle() {
        return this.battingStyle;
    }
 
    public void setBowlingStyle(String bowlingStyle) {
        this.bowlingStyle = bowlingStyle;
    }

    public String getBowlingStyle() {
        return this.bowlingStyle;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDateOfBirth() {
        return this.dateOfBirth;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return this.role;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setTeamInfo(TeamInfo teamInfo) {
        this.teamInfo = teamInfo;
    }

    public TeamInfo getTeamInfo() {
        return teamInfo;
    }    
}
