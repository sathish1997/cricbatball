package com.ideas2it.cricbatball.info;

import java.util.List;

import org.json.JSONArray;

import com.ideas2it.cricbatball.entities.Match;
import com.ideas2it.cricbatball.entities.Team;

/**
 * Used to store the multiples values to be used in the matchcontroller.
 *
 * @author    M.Sathish Varma created on 10th August 2019.
 */
public class MatchPaginationInfo {

    private int page;
    private int totalPages;
    private JSONArray matches;
    public MatchInfo matchInfo;
    private List<MatchInfo> matchesInfo;
    private List<TeamInfo> teamsForMatch;
    private TeamInfo firstTeam;
    private TeamInfo secondTeam;

    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }    

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setMatches(JSONArray matches) {
        this.matches = matches;
    }

    public JSONArray getMatches() {
        return matches;
    }

    public void setMatchInfo(MatchInfo matchInfo) {
        this.matchInfo = matchInfo;
    }

    public MatchInfo getMatchInfo() {
        return matchInfo;
    }

    public void setMatchesInfo(List<MatchInfo> matchesInfo) {
        this.matchesInfo = matchesInfo;
    }

    public List<MatchInfo> getMatchesInfo() {
        return matchesInfo;
    }

    public void setTeamsForMatch(List<TeamInfo> teamsForMatch) {
        this.teamsForMatch = teamsForMatch;
    }

    public List<TeamInfo> getTeamsForMatch() {
        return teamsForMatch;
    }

    public void setFirstTeam(TeamInfo firstTeam) {
        this.firstTeam = firstTeam;
    }

    public TeamInfo getFirstTeam() {
        return firstTeam;
    }

    public void setSecondTeam(TeamInfo secondTeam) {
        this.secondTeam = secondTeam;
    }

    public TeamInfo getSecondTeam() {
        return secondTeam;
    }
}
