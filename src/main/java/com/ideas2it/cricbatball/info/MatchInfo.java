package com.ideas2it.cricbatball.info;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.ideas2it.cricbatball.common.MatchType;
import com.ideas2it.cricbatball.entities.Match;
import com.ideas2it.cricbatball.entities.Team;

/**
 * Used to store the multiples values to be used in the matchcontroller.
 *
 * @author    M.Sathish Varma created on 10th August 2019.
 */
public class MatchInfo {

    private int id;
    private String name;
    private String location;
    public String startingTime;
    private Date dateOfPlay;
    private MatchType matchType;
    private Set<TeamInfo> teams;

    /**
     * Getter and Setter methods.
     */

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return this.location;
    }

    public void setStartingTime(String startingTime) {
        this.startingTime = startingTime;
    }

    public String getStartingTime() {
        return this.startingTime;
    }

    public void setDateOfPlay(Date dateOfPlay) {
        this.dateOfPlay = dateOfPlay;
    }

    public Date getDateOfPlay() {
        return this.dateOfPlay;
    }

    public void setMatchType(MatchType matchType) {
        this.matchType = matchType;
    }

    public MatchType getMatchType() {
        return this.matchType;
    }

    public void setTeams(Set<TeamInfo> teams) {
        this.teams = teams;
    }

    public Set<TeamInfo> getTeams() {
        return this.teams;
    }
}
