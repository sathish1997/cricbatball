package com.ideas2it.cricbatball.service;

import java.util.List;
import java.util.Set;

import org.json.JSONArray;

import com.ideas2it.cricbatball.common.Country;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.entities.Team;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.info.PlayerInfo;
import com.ideas2it.cricbatball.info.TeamInfo;
import com.ideas2it.cricbatball.info.TeamPaginationInfo;

/**
 * used to make communication to the database.
 *
 * @author    M.Sathish Varma
 */
public interface TeamService {

    /**
     * Used to store the team information.
     *
     * @param name - name of the team.
     * @param country - country of the team.
     * @param status - the status is initially set false , set true
     * if all the players are added to the team correctly.
     * @throws CricBatBallException - user defined exception.
     */
    TeamPaginationInfo storeTeamInformation(TeamInfo teamInfo) 
            throws CricBatBallException;

    /**
     * Used to retrieve all the players information.
     *
     * @return - sends back the players retieved from the database
     * @throws CricBatBallException - user defined exception.
     */
    List<PlayerInfo> retrievePlayers(Country country)
            throws CricBatBallException;

    /**
     * used to the selected players to the team.
     *
     * @param playersId - id of the player to be added.
     * @param teamId - id of the team added to the player.
     * @throws CricBatBallException - user defined exception.
     */
    List<Player> fetchPlayers(String[] playerIds) 
            throws CricBatBallException;

    /**
     * Used to get the teams from the database.
     *
     * @param offset - contains id of the match before the starting position.
     * @param limit - number of records to be fetched.
     * @throws CricBatBallException - user defined exception.
     */
    List<TeamInfo> getTeams(int offset, int limit)
            throws CricBatBallException;
    
    /**
     * Used to update the team status.
     *
     * @param team - team whose information to be updated.
     * @param roleCount - containing the count of the batsman, bowler,
     * wicket-keeper, allrounder.
     * @throws CricBatBallException - user defined exception.
     */
    String updateTeamStatus(Team team, int[] roleCount)
            throws CricBatBallException;

    /**
     * Used to update team information in the database.
     *
     * @param team - team whose information to be updated.
     * @throws CricBatBallException - user defined exception.
     */
    void updateTeam(Team team) throws CricBatBallException;

    /**
     * Used to update team information like name, adding and removing players.
     *
     * @param id - id is used to find and add or remove the players team.
     * @param addPlayersId - contains id of players 
     *                       whose information are to be added to the team.
     * @param removePlayersId - contains id of players 
     *                       whose information are to be removed from the team.  
     * @param name - name of the team to be updated.
     */
    void updatePlayersInTeam(int id, String[] addPlayersId, String[]
            removePlayersId, String name) throws CricBatBallException;

    /**
     * used to remove the players from teh team.
     *
     * @param teamId -  unique id of the team.
     * @param playersId - id of players to be removed from the team.
     * @param name - name of the team to be updated. 
     * @throws CricBatBallException - user defined exception.
     */
    void removePlayers(int teamId, String[] playersId) 
            throws CricBatBallException;

    /**
     * used to remove the team from the database.
     *
     * @param teamId - the unique id of team whose information has to be deleted
     * @throws CricBatBallException - user defined exception.
     */
    void removeTeam(int teamId) throws CricBatBallException;

    /**
     * Used to get the particular with the id of that team.
     *
     * @param teamId - id of the team to be fetched.
     * @throws CricBatBallException - user defined exception.
     */
    Team retrieveTeam(int teamId) throws CricBatBallException;

   /**
    * used to return the teams whose status is complete.
    *
    * @return - sends back the list of the team who have complete status.
    * @throws CricBatBallException - user defined exception.
    */
   List<Team> getTeamsByStatus() throws CricBatBallException;

    /**
     * Used to retrieve player with the help of the teamId
     *
     * @param teamId - denotes which teams players has to be fetched.
     * @return team - sends back the information of the team.
     * @throws CricBatBallException - user defined exception.
     */
    Team retrievePlayersById(int teamId)
        throws CricBatBallException;

    /**
     * Used to add the players to the team.
     *
     * @param id -  id of the team to which the player to be added.
     * @param playersId - containing the id of the player to be added.
     * @throws CricBatBallException - user defined exception.
     */
    void addPlayersToTeam(int id, String[] playersId)
            throws CricBatBallException;

    /**
     * Used to get the particular with the id of that team.
     *
     * @param teamId - id of the team to be fetched.
     * @throws CricBatBallException - user defined exception.
     */
    Team retrieveTeamForMatches(int teamId) throws CricBatBallException;

    /**
     * Used To Count the role of the player in the match.
     *
     * @param players - players in the team.
     * @return - sends back the array of count of the role of player in team.
     * @throws CricBatBallException - user defined exception.
     */
    int[] getRoleCount(Set<Player> players) throws CricBatBallException;

    /**
     * Gets the total number of records from the database.
     *
     * @return - sends back number of records in the database.
     * @throws CricBatBallException - user defined exception.
     */
    int totalTeams() throws CricBatBallException;

    /**
     * Used the send back list of team object as the json array.
     *
     * @param teams - list of teams to be converted.
     * @return - sends back the list as the json array.
     * @throws CricBatBallException - user defined exception.
     */
    JSONArray getTeamsForPagination(List<TeamInfo> teams)
            throws CricBatBallException;

    /**
     * Used to get the player information for pagination and 
     * Also contains page information.
     *
     * @param page - the page number is used to fetch the player information.
     * @return playerInfo - sends back the information used for pagination.
     */
    TeamPaginationInfo getPaginationInfo(int page) throws CricBatBallException;

    /**
     * Used to get multiple values of team related information like role count,
     * status and send the to the controller as single value.
     *
     * @param id - id of the team whose information to be wanted.
     * @return teamInfo - containing multiple values of team.
     */
    TeamPaginationInfo getTeamInformation(int id) throws CricBatBallException;
}
