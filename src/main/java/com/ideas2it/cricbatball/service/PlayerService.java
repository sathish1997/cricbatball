package com.ideas2it.cricbatball.service;

import java.io.IOException;
import java.util.List;

import org.json.JSONArray; 
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import javax.servlet.ServletException;

import com.ideas2it.cricbatball.common.Country;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.info.PlayerInfo;
import com.ideas2it.cricbatball.info.PlayerPaginationInfo;

/**
 * Used by contorller to manage the data from DAO,
 *
 *@author M.Sathish Varma created on 13th June 2019.
 */
public interface PlayerService {

    /**
     * used to insert information of players into the database.
     *
     * @param player - player information to be updated. 
     * @param pincode - pincode to be updated.
     * @return - the player information sent back.
     * @throws CricBatBallException - user defined exception.
     */
     PlayerInfo storePlayerInformation(PlayerInfo playerInfo, 
            CommonsMultipartFile file) throws CricBatBallException, IOException;

    /**
     * removing player information into the map.
     *
     * @param offset - contains id of the match before the starting position.
     * @param limit - number of records to be fetched.
     * @param id - id of player.
     * @throws CricBatBallException - user defined exception.
     */
    List<PlayerInfo> viewPlayers(int offset, int limit)
            throws CricBatBallException;

    /**
     * used to add profile picture to the player.
     *
     * @param limit - determines number of player to be retrieved from database.
     * @param offset - position of the record.
     * @return - list of players fetched from database using limit and offset.
     * @throws CricBatBallException - user defined exception.
     */
    void addImagePath(Player player, CommonsMultipartFile filePart)
            throws CricBatBallException, IOException;

    /**
     * used to retrieve player information from the database.
     *
     * @param limit - determines number of player to be retrieved from database.
     * @param offset - position of the record.
     * @return - list of players fetched from database using limit and offset.
     * @throws CricBatBallException - user defined exception.
     */
    PlayerInfo viewPlayer(int id)
            throws CricBatBallException;

    /**
     * Updating the existing player information stored in the database.
     *
     * param player - containing information to be updated.
     * @throws CricBatBallException - user defined exception.
     */ 
    void updatePlayerInformation(Player player) throws CricBatBallException;

    /**
     * Contains the player information to be updated.
     *
     * @param playerInfo - contains the updated inforamtion of the player.
     * @param file - contains the profile picture of player to be updated.
     */   
    PlayerInfo updatePlayer(PlayerInfo playerInfo, CommonsMultipartFile file) 
            throws CricBatBallException, IOException;

   /**
    * Used to delete the player from the database.
    *
    * @param id - id to be deleted.
    * @throws CricBatBallException - user defined exception.
    */ 
   void deletePlayer(int id) throws CricBatBallException;

   /**
    * Gets the total number of records from the database.
    *
    * @return - sends the total number of records in the database.
     * @throws CricBatBallException - user defined exception.
    */
    int totalPlayers() throws CricBatBallException;

    /**
     *  Used to get the players in the country specified.
     *
     * @return - sends back the player's list in the country specified.
     * @throws CricBatBallException - user defined exception.
     */
    List<PlayerInfo> retrievePlayersByCountry(Country country) 
            throws CricBatBallException;

    /**
     * used to assign the players with their team.
     *
     * @param playersId - contains tha players to be added to the team.
     * @throws CricBatBallException - user defined exception.
     */
    List<Player> fetchPlayers(String[] playerIds) 
            throws CricBatBallException;

    /**
     * Used the send back list of player object as the json array.
     *
     * @param players - list of players to be converted.
     * @return - sends back the list of players as the json array.
     * @throws CricBatBallException - user defined exception.
     */ 
    JSONArray getPlayersForPagination(List<PlayerInfo> players)
            throws CricBatBallException;
   
    /**
     * Used to get the player information for pagination and 
     * Also contains page information.
     *
     * @param page - the page number is used to fetch the player information.
     * @return playerInfo - sends back the information used for pagination.
     */
    PlayerPaginationInfo getPaginationInfo(int page) throws CricBatBallException;
}
