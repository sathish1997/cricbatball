package com.ideas2it.cricbatball.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.cricbatball.common.BallResult;
import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.dao.OverDAO;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.entities.Match;
import com.ideas2it.cricbatball.entities.Over;
import com.ideas2it.cricbatball.entities.OverDetail;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.entities.Team;
import com.ideas2it.cricbatball.info.MatchInfo;
import com.ideas2it.cricbatball.info.MatchPaginationInfo;
import com.ideas2it.cricbatball.info.TeamInfo;
import com.ideas2it.cricbatball.info.OverDetailInfo;
import com.ideas2it.cricbatball.service.MatchService;
import com.ideas2it.cricbatball.service.OverService;
import com.ideas2it.cricbatball.service.PlayerService;
import com.ideas2it.cricbatball.service.TeamService;
import com.ideas2it.cricbatball.utils.CommonUtil;

/**
 * Used to provide the processed data to the controller.
 *
 * @author    M.Sathish Varma created on 6th July 2019.
 */
@Service
public class OverServiceImpl implements OverService {

    private MatchService matchService;
    private OverDAO overDAO;
    private PlayerService playerService;
    private TeamService teamService;

    @Autowired
    public OverServiceImpl(PlayerService playerService, 
            MatchService matchService, OverDAO overDAO, 
            TeamService teamService) {
        this.matchService = matchService;
        this.overDAO = overDAO;
        this.playerService = playerService;
        this.teamService = teamService;
    }

    @Override
    public OverDetailInfo getPlayersToStart(String[] batsmanIds, 
            String[] bowlerId, int matchId, int firstTeamId, int secondTeamId) 
            throws CricBatBallException {
        int id;
        Over over = new Over();
        int overCount = overDAO.getOverCountByMatchId(matchId);
        OverDetailInfo overDetailInfo = new OverDetailInfo();
        Match match = matchService.retrieveMatch(matchId);
        over.setMatch(match);
        over.setOverNumber(++overCount);
        id = overDAO.insertOver(over);
        over.setId(id);
        overDetailInfo.setFirstTeam(CommonUtil
                .convertTeamToTeamInformation(teamService
                .retrievePlayersById(firstTeamId)));
        overDetailInfo.setSecondTeam(CommonUtil
                .convertTeamToTeamInformation(teamService
                .retrievePlayersById(secondTeamId)));
        overDetailInfo.setOverInfo(CommonUtil.convertOverToOverInfo(over));
        List<Player> batsmen = playerService.fetchPlayers(batsmanIds);
        List<Player> bowler = playerService.fetchPlayers(bowlerId);
        if (null != batsmen.get(0)) {
            overDetailInfo.setFirstBatsman(CommonUtil
                    .convertPlayerToPlayerInfo(batsmen.get(0)));
        }
        if (null != batsmen.get(1)) {
            overDetailInfo.setSecondBatsman(CommonUtil
                    .convertPlayerToPlayerInfo(batsmen.get(1)));
        }
        if (null != bowler.get(0)) {
            overDetailInfo.setBowler(CommonUtil
                    .convertPlayerToPlayerInfo(bowler.get(0)));
        }
        return overDetailInfo;
    }

    @Override
    public Over fetchOver(int overId) throws CricBatBallException {
        return overDAO.retrieveOver(overId);
    }

    @Override
    public void updateOver(Over over) throws CricBatBallException {
        overDAO.updateOver(over);
    }

    @Override
    public Over addOverDetailToOver(int overId, OverDetail overDetail) 
            throws CricBatBallException {
        Set<OverDetail> overDetails = new HashSet<OverDetail>();
        Over over = overDAO.retrieveOver(overId);
        overDetail.setOver(over);
        overDetails = over.getOverDetails();
        int ballNumber = overDetails.size() + 1 - over.getWidesCount() 
                        - over.getNoBallCount();
        if (0 == overDetails.size()) {
            overDetail.setBallNumber(1);
        } else if (6 >= ballNumber){
            overDetail.setBallNumber(ballNumber);
        } else if (6 < ballNumber) {
            over = createNextOver(over.getMatch());
            overDetails = over.getOverDetails();
            overDetail.setBallNumber(1);
        }
        overDetails.add(overDetail);
        storeOverDetailToOver(over, overDetail);
        over.setOverDetails(overDetails);
        updateOver(over);
        return over;
    }

    @Override
    public void storeOverDetailToOver(Over over, OverDetail overDetail) {
        int runs = over.getRuns();
        int wicketCount = over.getWicketCount();
        int noBallCount = over.getNoBallCount();
        int widesCount = over.getWidesCount();
        int legByesCount = over.getLegByesCount();
        int byesCount = over.getByesCount();
        int sixesCount = over.getSixesCount();
        int foursCount = over.getFoursCount();
        int ballNumber = overDetail.getBallNumber();
        BallResult ballResult = overDetail.getBallResult();
        switch (ballResult) {
            case Wide:
                ++widesCount;
                ++runs;
                over.setWidesCount(widesCount);
                overDetail.setBallNumber(--ballNumber);
                break;
            case No_Ball:
                ++noBallCount;
                ++runs;
                overDetail.setBallNumber(--ballNumber);
                over.setNoBallCount(noBallCount);
                break;
            case Byes:
                ++byesCount;
                ++runs;
                over.setByesCount(byesCount);
                break;
            case Leg_Byes:
                ++legByesCount;
                ++runs;
                over.setLegByesCount(legByesCount);
                break;
            case Wicket:
                ++wicketCount;
                over.setWicketCount(wicketCount);
                break;
            case Four:
                ++foursCount;
                runs += 4;
                over.setFoursCount(foursCount);
                break;
            case Six:
                ++sixesCount;
                runs += 6;
                over.setSixesCount(sixesCount);
                break;
            default:
                runs += BallResult.fetchRunsFromBallResult(ballResult);
                break;
        }
        over.setRuns(runs);
    }

    @Override
    public Over createNextOver(Match match) throws CricBatBallException {
        int id;
        int overCount = overDAO.getOverCountByMatchId(match.getId());
        Over nextOver = new Over();
        nextOver.setMatch(match);
        nextOver.setOverNumber(++overCount);
        id = overDAO.insertOver(nextOver);
        nextOver = overDAO.retrieveOver(id);
        return nextOver;
    }

    @Override
    public TeamInfo retrieveNextBowler(int teamId, int bowlerId) 
            throws CricBatBallException {
        Team team = teamService.retrievePlayersById(teamId);
        return CommonUtil.convertTeamToTeamInformation(team);
    }

    @Override
    public void getOverDetailInformation(OverDetailInfo
            overDetailInfo, int bowlerId) throws CricBatBallException {
        Over over = createNextOver(matchService.retrieveMatch(overDetailInfo
                       .getOverInfo().getMatch().getId()));
        overDetailInfo.setFirstTeam(CommonUtil
                .convertTeamToTeamInformation(teamService
                .retrievePlayersById(overDetailInfo.getFirstTeam().getId())));
        overDetailInfo.setSecondTeam(CommonUtil
                .convertTeamToTeamInformation(teamService
                .retrievePlayersById(overDetailInfo.getSecondTeam().getId())));
        overDetailInfo.setFirstBatsman(playerService.viewPlayer(overDetailInfo
                .getFirstBatsman().getId()));
        overDetailInfo.setSecondBatsman(playerService.viewPlayer(overDetailInfo
                .getSecondBatsman().getId()));
        overDetailInfo.setBowler(playerService.viewPlayer(bowlerId));
        overDetailInfo.setOverInfo(CommonUtil.convertOverToOverInfo(over));
    }

    @Override
    public TeamInfo retrieveNextBatsman(int wicketId, OverDetailInfo 
            overDetailInfo) throws CricBatBallException {
        int teamId;
        Team team;
        if (wicketId == overDetailInfo.getFirstBatsman().getId()) {
           overDetailInfo.getFirstBatsman().setId(0); 
           overDetailInfo.setFirstPlayerRuns(0);
           overDetailInfo.setFirstPlayerballs(0);
           overDetailInfo.setFirstPlayerSixes(0);
           overDetailInfo.setFirstPlayerFours(0);
        } else {
           overDetailInfo.getSecondBatsman().setId(0); 
           overDetailInfo.setSecondPlayerRuns(0);
           overDetailInfo.setSecondPlayerBalls(0);
           overDetailInfo.setSecondPlayerSixes(0);
           overDetailInfo.setSecondPlayerFours(0);
        }
        teamId = overDetailInfo.getFirstTeam().getId();
        team = teamService.retrievePlayersById(teamId);
        return CommonUtil.convertTeamToTeamInformation(team);
    }

    @Override
    public void getOverDetailInfo(OverDetailInfo
            overDetailInfo, int batsmanId) throws CricBatBallException {
        overDetailInfo.setFirstTeam(CommonUtil
                .convertTeamToTeamInformation(teamService
                .retrievePlayersById(overDetailInfo.getFirstTeam().getId())));
        overDetailInfo.setSecondTeam(CommonUtil
                .convertTeamToTeamInformation(teamService
                .retrievePlayersById(overDetailInfo.getSecondTeam().getId())));
        if (0 == overDetailInfo.getFirstBatsman().getId()) {
            overDetailInfo.setFirstBatsman(playerService
                    .viewPlayer(batsmanId));
            overDetailInfo.setSecondBatsman(playerService
                    .viewPlayer(overDetailInfo.getSecondBatsman().getId()));
        } else {
            overDetailInfo.setSecondBatsman(playerService
                    .viewPlayer(batsmanId));
            overDetailInfo.setFirstBatsman(playerService
                    .viewPlayer(overDetailInfo.getFirstBatsman().getId()));
        }
        overDetailInfo.setBowler(playerService.viewPlayer(overDetailInfo
                .getBowler().getId()));
    }
}
