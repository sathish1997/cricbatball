package com.ideas2it.cricbatball.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.common.MatchType;
import com.ideas2it.cricbatball.dao.MatchDAO;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.exception.MatchNotFoundException;
import com.ideas2it.cricbatball.entities.Match;
import com.ideas2it.cricbatball.entities.Team;
import com.ideas2it.cricbatball.info.MatchInfo;
import com.ideas2it.cricbatball.info.MatchPaginationInfo;
import com.ideas2it.cricbatball.info.TeamInfo;
import com.ideas2it.cricbatball.service.MatchService;
import com.ideas2it.cricbatball.service.TeamService;
import com.ideas2it.cricbatball.utils.CommonUtil;
import com.ideas2it.cricbatball.utils.DateUtil;

/**
 * Used to provide the processed data to the controller.
 *
 * @author    M.Sathish Varma created on 6th July 2019.
 */
@Service
public class MatchServiceImpl implements MatchService {
    
    private MatchDAO matchDAO;
    private TeamService teamService;

    @Autowired
    public MatchServiceImpl(TeamService teamService, MatchDAO matchDAO) {
        this.teamService = teamService;
        this.matchDAO = matchDAO;
    }

    @Override
    public MatchPaginationInfo insertMatch(String date, MatchInfo matchInfo) 
            throws CricBatBallException {
        MatchPaginationInfo matchPaginationInfo = new MatchPaginationInfo();
        Date dateOfPlay = DateUtil.convertToDate(date);
        matchInfo.setDateOfPlay(dateOfPlay);
        Match match = CommonUtil.convertMatchInfoToMatch(matchInfo);
        matchInfo.setId(matchDAO.insertMatch(match));
        matchPaginationInfo.setMatchInfo(matchInfo);
        matchPaginationInfo.setTeamsForMatch(getTeamsForMatch(matchInfo
                .getId()));
        return matchPaginationInfo;
    }

    @Override
    public Match retrieveMatch(int matchId) throws CricBatBallException {
        return matchDAO.retrieveMatch(matchId);
    }

    @Override
    public MatchInfo retrieveMatchToView(int id) throws CricBatBallException {
        return CommonUtil.convertMatchToMatchInfo(retrieveMatch(id));
    }

    @Override
    public List<MatchInfo> retrieveMatches(int page, int limit)
            throws CricBatBallException {
        List<MatchInfo> matchesInfo = new ArrayList<MatchInfo>();
        List<Match> matches = matchDAO.retrieveMatches((page - 1) * limit, 
                                  limit);
        if (null  != matches) {
            for (Match match : matches) {
                matchesInfo.add(CommonUtil.convertMatchToMatchInfo(match));
            }
        }
        return matchesInfo;
    }

    @Override
    public void updateMatch(Match match) throws CricBatBallException {
        matchDAO.updateMatch(match);
    }

    @Override
    public void deleteMatch(int matchId) throws CricBatBallException {
        Match match = retrieveMatch(matchId);
        matchDAO.deleteMatch(match);
    }

    @Override
    public void updateMatchInformation(String date, int id, String[] teamsId,
            MatchInfo matchInfo) throws CricBatBallException {
        Match match = retrieveMatch(id);
        match.setName(matchInfo.getName());
        match.setLocation(matchInfo.getLocation());
        match.setStartingTime(matchInfo.getStartingTime());        
        match.setMatchType(matchInfo.getMatchType());
        updateMatch(match);
        addTeams(match, teamsId);
     }

    @Override
    public List<Team> fetchTeams() throws CricBatBallException {
        return teamService.getTeamsByStatus();
    }

    @Override
    public void addTeamsToMatch(int id, String[] teamsForMatch) 
            throws CricBatBallException {
        Match match = retrieveMatch(id);
        addTeams(match, teamsForMatch);        
    } 

    @Override
    public void addTeams(Match match, String[] teamsId) 
            throws CricBatBallException {
        List<Team> teamsToBeAdded = new ArrayList<Team>();
        if (null != teamsId) {
            for (String id : teamsId) {
                teamsToBeAdded.add(teamService.retrieveTeam(Integer
                        .parseInt(id)));
            }
            match.setTeams(new HashSet<Team>(teamsToBeAdded));
            updateMatch(match);
        }
    }

    @Override
    public List<TeamInfo> getTeamsForMatch(int matchId) 
            throws CricBatBallException {
        boolean isSameDate = Boolean.FALSE;
        List<TeamInfo> teamsForMatch = new ArrayList<TeamInfo>();
        Match match = retrieveMatch(matchId);
        Date dateOfPlay = match.getDateOfPlay();
        List<Team> teams = teamService.getTeamsByStatus();
        for (Team team : teams) {
            for (Match matchOfTeam : team.getMatches()) {
                if (matchOfTeam.getDateOfPlay().equals(dateOfPlay) && 
                        (null != matchOfTeam.getDateOfPlay())) {
                    isSameDate = Boolean.TRUE;
                    break;
                }
            }
            if (!isSameDate){
                teamsForMatch.add(CommonUtil.convertTeamToTeamInfo(team)); 
            }
            isSameDate = Boolean.FALSE;
        }
        return teamsForMatch;
    }

    public MatchPaginationInfo getTeamsToUpdateMatch(int matchId)  
            throws CricBatBallException {
        MatchPaginationInfo matchPaginationInfo = new MatchPaginationInfo();
        matchPaginationInfo.setMatchInfo(CommonUtil
                .convertMatchToMatchInfo(retrieveMatch(matchId)));
        matchPaginationInfo.setTeamsForMatch(getTeamsForMatch(matchId));
        return matchPaginationInfo;
    }

    @Override
    public MatchPaginationInfo getPaginationInfo(int page)
            throws CricBatBallException {
        int recordsPerPage = 5;
        MatchPaginationInfo matchPaginationInfo = new MatchPaginationInfo();
        int pageCount = (int) Math.ceil(totalMatches() * 1.0 / recordsPerPage);
        matchPaginationInfo.setPage(page);
        matchPaginationInfo.setTotalPages(pageCount);
        matchPaginationInfo.setMatchesInfo(retrieveMatches(page, 
                recordsPerPage));
        matchPaginationInfo.setMatches(getMatchesForPagination(
                matchPaginationInfo.getMatchesInfo()));       
        return matchPaginationInfo;
    }

    @Override
    public int totalMatches() throws CricBatBallException {
        return matchDAO.totalMatches();
    }

    @Override
    public JSONArray getMatchesForPagination(List<MatchInfo> matches)
            throws CricBatBallException {
        JSONArray matchInformation = new JSONArray();
        JSONObject match;
        if (null != matches) {
            for (MatchInfo matchInfo : matches) {
                match = new JSONObject();
                match.put(Constants.ID, matchInfo.getId());
                match.put(Constants.MATCHNAME, matchInfo.getName());
                match.put(Constants.LOCATION, matchInfo.getLocation());
                match.put(Constants.DATEOFPLAY, DateUtil
                        .convertToString(matchInfo.getDateOfPlay()));
                match.put(Constants.MATCHTYPE, matchInfo.getMatchType());
                match.put(Constants.STARTINGTIME, matchInfo
                        .getStartingTime());
                matchInformation.put(match);
            }
        }
        return matchInformation;
    }

    @Override
    public MatchPaginationInfo fetchTeamsToStartMatch(int id) 
            throws CricBatBallException {
        int count = 0;
        MatchPaginationInfo matchPaginationInfo = new MatchPaginationInfo();
        Team teamsForMatch;
        Match match = retrieveMatch(id);
        matchPaginationInfo.setMatchInfo(CommonUtil
                .convertMatchToMatchInfo(match));
        Set<Team> teams = match.getTeams();
        for (Team team : teams) {
            count++;
            if (1 == count) {
                teamsForMatch = teamService.retrievePlayersById(team.getId());
                matchPaginationInfo.setFirstTeam(CommonUtil.
                        convertTeamToTeamInformation(teamsForMatch));
            } else if (2 == count) {
                teamsForMatch = teamService.retrievePlayersById(team.getId());
                matchPaginationInfo.setSecondTeam(CommonUtil.
                        convertTeamToTeamInformation(teamsForMatch));
            }
        }
        return matchPaginationInfo; 
    }
}
