package com.ideas2it.cricbatball.service.impl;

import java.util.ArrayList;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.dao.UserDAO;
import com.ideas2it.cricbatball.entities.User;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.exception.EmailNotFoundException;
import com.ideas2it.cricbatball.exception.ExceededFailedAttemptException;
import com.ideas2it.cricbatball.exception.PasswordNotMatchingException;
import com.ideas2it.cricbatball.info.UserInfo;
import com.ideas2it.cricbatball.service.UserService;
import com.ideas2it.cricbatball.utils.CommonUtil;
import com.ideas2it.cricbatball.utils.EncryptWord;

@Service
public class UserServiceImpl implements UserService {
    private UserDAO userDAO;

    @Autowired
    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public boolean createUser(UserInfo userInfo) throws CricBatBallException {        
        userInfo.setPassword(EncryptWord.getEncryptedPassword(userInfo
                .getPassword()));
        return userDAO.insertUser(CommonUtil.convertUserInfoToUser(userInfo));
    }

    @Override
    public boolean checkUser(String emailId, String password)
            throws CricBatBallException {
        if (1 == userDAO.isUserExists(emailId, EncryptWord
                .getEncryptedPassword(password))) {
            return Boolean.TRUE;
        } else {
            checkFailedAttempts(emailId);
            return Boolean.FALSE;
        }
    }
    
    @Override
    public void checkFailedAttempts(String emailId)
            throws CricBatBallException {
        int attempts = 0;
        User user = getUser(emailId);        
        if (null != user) {
            attempts = user.getFailedAttempts();
            user.setFailedAttempts(++attempts);                
            if (3 <= attempts) {
                user.setFailedAttempts(0);
                user.setStatus(Boolean.FALSE);
                updateUserInfo(user);
                throw new ExceededFailedAttemptException(Constants
                        .ATTEMPTS_WARNING);
            }
            updateUserInfo(user);
            throw new PasswordNotMatchingException(Constants.INVALID_PASSWORD);           
        } else {
            throw new EmailNotFoundException(Constants.INVALID_EMAIL);
        }
    }

    @Override
    public User getUser(String emailId) throws CricBatBallException {
        return userDAO.getUserInfo(emailId);
    }
    
    @Override
    public void updateUserInfo(User user) throws CricBatBallException {
        userDAO.updateUserInfo(user);
    }
}
