package com.ideas2it.cricbatball.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.cricbatball.common.Country;
import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.exception.TeamNotFoundException;
import com.ideas2it.cricbatball.dao.TeamDAO;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.entities.Team;
import com.ideas2it.cricbatball.info.PlayerInfo;
import com.ideas2it.cricbatball.info.TeamInfo;
import com.ideas2it.cricbatball.info.TeamPaginationInfo;
import com.ideas2it.cricbatball.service.PlayerService;
import com.ideas2it.cricbatball.service.TeamService;
import com.ideas2it.cricbatball.utils.CommonUtil;

/**
 * used to make communication to the database and process the team data sent.
 *
 * @author    M.Sathish Varma
 */
@Service
public class TeamServiceImpl implements TeamService {

    private PlayerService playerService; 
    private TeamDAO teamDAO;

    @Autowired
    public TeamServiceImpl(PlayerService playerService, TeamDAO teamDAO) {
        this.teamDAO = teamDAO;
        this.playerService = playerService;
    }

    @Override
    public TeamPaginationInfo storeTeamInformation(TeamInfo teamInfo) 
            throws CricBatBallException {
        int id;
        TeamPaginationInfo teamPaginationInfo = new TeamPaginationInfo();
        id = teamDAO.insertTeam(CommonUtil.convertTeamInfoToTeam(teamInfo));
        teamPaginationInfo.setId(id);
        teamPaginationInfo.setTeamInfo(teamInfo);
        teamPaginationInfo.setPlayersNotInTeam(retrievePlayers(teamInfo
                .getCountry()));
        return teamPaginationInfo;
    }

    @Override
    public List<PlayerInfo> retrievePlayers(Country country)
            throws CricBatBallException {
        return playerService.retrievePlayersByCountry(country);
    }

    @Override
    public List<Player> fetchPlayers(String[] playerIds) 
            throws CricBatBallException {
        return playerService.fetchPlayers(playerIds);
    }

    @Override
    public List<TeamInfo> getTeams(int page, int limit)
            throws CricBatBallException {
        List<TeamInfo> teamsInfo = new ArrayList<TeamInfo>();
        List<Team> teams = teamDAO.getTeams((page - 1) * limit, limit);
        for (Team team : teams) {
            teamsInfo.add(CommonUtil.convertTeamToTeamInfo(team));
        }
        return teamsInfo;
    }

    @Override
    public String updateTeamStatus(Team team, int[] roleCount)
            throws CricBatBallException {
        if (11  == roleCount[4]) {
            if (3 <= roleCount[0] && 3 <= roleCount[1] && 1 == roleCount[2]) {
                team.setStatus(Boolean.TRUE);
                updateTeam(team);
                return Constants.COMPLETE;
            } else {
                team.setStatus(Boolean.FALSE);
                updateTeam(team);
                return Constants.INCOMPLETE;
            }
        } else {
            team.setStatus(Boolean.FALSE);
            updateTeam(team);
            return Constants.INCOMPLETE;
        }
    }

    @Override
    public void updateTeam(Team team) throws CricBatBallException {
        teamDAO.updateTeam(team);
    }

    public void updateTeamName(int teamId, String name) 
            throws CricBatBallException {
        Team team = retrievePlayersById(teamId);
        team.setName(name); 
        updateTeam(team);
    }

    @Override
    public void updatePlayersInTeam(int id, String[] addPlayerIds, String[]
            removePlayerIds, String name) throws CricBatBallException {
        updateTeamName(id, name);
        addPlayersToTeam(id, addPlayerIds);
        removePlayers(id, removePlayerIds); 
    }

    @Override
    public void removePlayers(int teamId, String[] playersId) 
            throws CricBatBallException {
        Set<Player> players;
        Team team = retrievePlayersById(teamId);
        players = team.getPlayers();
        if (null != playersId) {
            for (String id : playersId) {
                for (Player player : players) {
                    if (player.getId() == Integer.parseInt(id)) {
                        players.remove(player);
                        break;
                    }
                }
            } 
            team.setPlayers(players);
            updateTeam(team);
        }
     }

    @Override
    public void removeTeam(int teamId) throws CricBatBallException {
        teamDAO.removeTeam(teamId);
    }

    @Override
    public Team retrieveTeam(int teamId) throws CricBatBallException {
        return teamDAO.retrieveTeam(teamId);
    }

   @Override
   public List<Team> getTeamsByStatus() throws CricBatBallException {
       return teamDAO.getTeamsByStatus();
   }

    @Override
    public Team retrievePlayersById(int teamId)
        throws CricBatBallException {
        return teamDAO.retrievePlayersById(teamId);
    }

    @Override
    public void addPlayersToTeam(int id, String[] playerIds)
            throws CricBatBallException{      
        Set<Player> playersInfo;
        List<Player> playersForTeam;
        Team team = new Team();
        if (null != playerIds) {   
            playersForTeam = fetchPlayers(playerIds);
            team = retrievePlayersById(id);      
            playersInfo = team.getPlayers();
            playersInfo.addAll(playersForTeam);
            team.setPlayers(playersInfo);            
            updateTeam(team);
        }
    }

    @Override
    public Team retrieveTeamForMatches(int teamId) throws CricBatBallException {
        return teamDAO.retrieveTeam(teamId);
    }

    @Override
    public int[] getRoleCount(Set<Player> players) throws CricBatBallException {
        int[] roleCount = new int[5];
        int batsmanCount = 0;
        int bowlerCount = 0;
        int wicketKeeperCount = 0;
        int totalCount = 0;
        int allRounderCount = 0;
        if (null != players) {
            for (Player player : players){
                if (player.getRole().equals(Constants.BATSMAN)) {
                    ++batsmanCount;
                } else if (player.getRole().equals(Constants.BOWLER)) {
                    ++bowlerCount;
                } else if (player.getRole().equals(Constants.WICKET_KEEPER)) {
                    ++wicketKeeperCount;
                } else if (player.getRole().equals(Constants.ALL_ROUNDER)) {
                    ++allRounderCount;
                }
            }
            totalCount = players.size();
            roleCount[0] =  batsmanCount;
            roleCount[1] = bowlerCount;
            roleCount[2] = wicketKeeperCount;
            roleCount[3] = allRounderCount;
            roleCount[4] = totalCount;
            return roleCount;
        } else {
            roleCount[0] =  batsmanCount;
            roleCount[1] = bowlerCount;
            roleCount[2] = wicketKeeperCount;
            roleCount[3] = allRounderCount;
            roleCount[4] = totalCount;
            return roleCount;
        }
    }

    @Override
    public int totalTeams() throws CricBatBallException {
        return teamDAO.totalTeams();
    }

    @Override
    public JSONArray getTeamsForPagination(List<TeamInfo> teams)
            throws CricBatBallException {
        JSONArray teamInformation = new JSONArray();
        JSONObject team = new JSONObject();
        for (TeamInfo teamInfo : teams) {
                team = new JSONObject();
                team.put(Constants.ID, teamInfo.getId());
                team.put(Constants.TEAMNAME, teamInfo.getName());
                team.put(Constants.COUNTRY, teamInfo.getCountry());
                teamInformation.put(team);
        }
        return teamInformation;
    }

    @Override
    public TeamPaginationInfo getPaginationInfo(int page) throws CricBatBallException {
        int recordsPerPage = 5;
        int pageCount = (int) Math.ceil(totalTeams() * 1.0 / recordsPerPage);
        TeamPaginationInfo teamPaginationInfo = new TeamPaginationInfo();
        teamPaginationInfo.setPage(page);
        teamPaginationInfo.setTotalPages(pageCount);
        teamPaginationInfo.setTeamsInfo(getTeams(page, recordsPerPage));
        if (null != teamPaginationInfo.getTeamsInfo()) {
            teamPaginationInfo.setTeams(getTeamsForPagination(teamPaginationInfo
                    .getTeamsInfo()));
        }
        return teamPaginationInfo;
    }

    @Override
    public TeamPaginationInfo getTeamInformation(int id) 
            throws CricBatBallException {
        TeamPaginationInfo teamPaginationInfo = new TeamPaginationInfo();
        Team team = retrievePlayersById(id);
        TeamInfo teamInfo = CommonUtil.convertTeamToTeamInformation(team);
        teamPaginationInfo.setTeamInfo(teamInfo);
        teamPaginationInfo.setRoleCount(getRoleCount(team.getPlayers()));
        teamPaginationInfo.setStatus(updateTeamStatus(team, 
                teamPaginationInfo.getRoleCount()));
        teamPaginationInfo.setPlayersNotInTeam(retrievePlayers(team
                .getCountry()));
        return teamPaginationInfo;   
    }
}
