package com.ideas2it.cricbatball.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import javax.servlet.ServletException;  

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.common.Country;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.exception.PlayerNotFoundException;
import com.ideas2it.cricbatball.dao.PlayerDAO;
import com.ideas2it.cricbatball.entities.Contact;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.entities.Team;
import com.ideas2it.cricbatball.info.PlayerInfo;
import com.ideas2it.cricbatball.info.PlayerPaginationInfo;
import com.ideas2it.cricbatball.service.PlayerService;
import com.ideas2it.cricbatball.utils.AgeUtil;
import com.ideas2it.cricbatball.utils.CommonUtil;

/**
 * Used by contorller to manage the data from DAO and process the data.
 *
 * @author M.Sathish Varma created on 13th June 2019.
 */
@Service
public class PlayerServiceImpl implements PlayerService {
    
    private PlayerDAO playerDAO;
 
    @Autowired
    public PlayerServiceImpl(PlayerDAO playerDAO) {
        this.playerDAO = playerDAO;
    }

    @Override
    public PlayerInfo storePlayerInformation(PlayerInfo playerInfo, 
            CommonsMultipartFile file) throws CricBatBallException, 
            IOException {
         Player player = CommonUtil.convertPlayerInfoToPlayer(playerInfo);
         player = playerDAO.insertPlayer(player);
         addImagePath(player, file);
         return CommonUtil.convertPlayerToPlayerInfo(player);
    }

    @Override
    public List<PlayerInfo> viewPlayers(int page, int limit)
            throws CricBatBallException {
        List<PlayerInfo> playersInfo = new ArrayList<PlayerInfo>();
        List<Player> players = playerDAO.fetchPlayers((page - 1) * limit, 
                                  limit);
        if (null != players) {
            for (Player player : players) {
                playersInfo.add(CommonUtil.convertPlayerToPlayerInfo(player));
            }
        }
        return playersInfo;
    }

    @Override
    public void addImagePath(Player player, CommonsMultipartFile file)
            throws CricBatBallException, IOException {
        Player playerToBeUpdated = playerDAO.fetchPlayer(player.getId());
        player.setTeam(playerToBeUpdated.getTeam());
        if (!file.getOriginalFilename().isEmpty()) {
            BufferedOutputStream outputStream = new BufferedOutputStream(
                    new FileOutputStream(Constants.IMAGE_PATH + player.getId() +
                    file.getOriginalFilename()));
            outputStream.write(file.getBytes());
            outputStream.flush();
            outputStream.close();
            String path = Constants.LOCALHOST_PATH + player.getId() 
                             + file.getOriginalFilename();
            player.setImagePath(path);
        }
        updatePlayerInformation(player);
    }

    @Override
    public PlayerInfo viewPlayer(int id) throws CricBatBallException {
        PlayerInfo playerInfo = CommonUtil
                                   .convertPlayerToPlayerInfo(playerDAO
                                   .fetchPlayer(id));
        return playerInfo;
    }

    @Override
    public void updatePlayerInformation(Player player)
            throws CricBatBallException {
        playerDAO.updatePlayerInformation(player);
    }

    @Override
    public PlayerInfo updatePlayer(PlayerInfo playerInfo, CommonsMultipartFile 
            file) throws CricBatBallException, IOException {
        addImagePath(CommonUtil.convertPlayerInfoToPlayer(playerInfo), file);
        return viewPlayer(playerInfo.getId());
    }

    @Override
    public void deletePlayer(int id) throws CricBatBallException {
        Player player = playerDAO.fetchPlayer(id);
        player.setStatus(Boolean.FALSE);
        updatePlayerInformation(player);
    }

   @Override
    public int totalPlayers() throws CricBatBallException {
        return playerDAO.totalPlayers();
    }

    @Override
    public List<PlayerInfo> retrievePlayersByCountry(Country country) 
            throws CricBatBallException {
        List<PlayerInfo> playersInfo = new ArrayList<PlayerInfo>();
        List<Player> players = playerDAO.retrievePlayersByCountry(country);
        for (Player player : players) {
            playersInfo.add(CommonUtil.convertPlayerToPlayerInfo(player));
        }
        return playersInfo;
    } 

    @Override
    public List<Player> fetchPlayers(String[] playerIds) 
            throws CricBatBallException {
        List<Integer> ids = new ArrayList<Integer>();
        for(String id : playerIds) {
            ids.add(Integer.valueOf(id));
        }
        return playerDAO.fetchPlayersById(ids);
    }

    @Override
    public JSONArray getPlayersForPagination(List<PlayerInfo> players)
            throws CricBatBallException {
        JSONArray playerInformation = new JSONArray();
        JSONObject player;
        for (PlayerInfo playerInfo :players) {
                player = new JSONObject();
                player.put(Constants.ID, playerInfo.getId());
                player.put(Constants.NAMES, playerInfo.getName());
                player.put(Constants.ROLE, playerInfo.getRole());
                player.put(Constants.COUNTRY, playerInfo.getCountry());
                player.put(Constants.BATTING_STYLE,
                        playerInfo.getBattingStyle());
                player.put(Constants.BOWLING_STYLE, playerInfo
                        .getBowlingStyle());
                player.put(Constants.DATE_OF_BIRTH, playerInfo
                        .getDateOfBirth());
                playerInformation.put(player);
            }
        return playerInformation;       
    }

    @Override
    public PlayerPaginationInfo getPaginationInfo(int page) 
            throws CricBatBallException {
        int recordsPerPage = 5;
        int pageCount = (int) Math.ceil(totalPlayers() * 1.0 / recordsPerPage);
        PlayerPaginationInfo playerPaginationInfo = new PlayerPaginationInfo();
        playerPaginationInfo.setPage(page);
        playerPaginationInfo.setTotalPages(pageCount);
        playerPaginationInfo.setPlayersInfo(viewPlayers(page, recordsPerPage));
        if (null != playerPaginationInfo.getPlayersInfo()) {
            playerPaginationInfo.setPlayers(getPlayersForPagination
                    (playerPaginationInfo.getPlayersInfo()));
        }
        return playerPaginationInfo;
    }
}
