package com.ideas2it.cricbatball.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.json.JSONObject; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.common.BallResult;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.entities.Match;
import com.ideas2it.cricbatball.entities.Over;
import com.ideas2it.cricbatball.entities.OverDetail;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.info.MatchInfo;
import com.ideas2it.cricbatball.info.MatchPaginationInfo;
import com.ideas2it.cricbatball.info.OverDetailInfo;
import com.ideas2it.cricbatball.service.MatchService;
import com.ideas2it.cricbatball.service.OverService;
import com.ideas2it.cricbatball.service.OverDetailService;
import com.ideas2it.cricbatball.service.PlayerService;
import com.ideas2it.cricbatball.utils.CommonUtil;

/**
 * Used to provide the processed data to the controller.
 *
 * @author    M.Sathish Varma created on 6th July 2019.
 */
@Service
public class OverDetailServiceImpl implements OverDetailService {

    private MatchService matchService;
    private OverService overService;
    private PlayerService playerService;

    @Autowired
    public OverDetailServiceImpl(PlayerService playerService, 
            MatchService matchService, OverService overService) {
        this.playerService = playerService;
        this.matchService = matchService;
        this.overService = overService;
    }

    @Override
    public JSONObject getOverDetailInfo(int batsmanId, int bowlerId, 
            int matchId, int overId, int strikerId, int secondBatsmanId) 
            throws CricBatBallException {
        int count = 0;
        String ballResult; 
        Over over;
        OverDetail overDetail = new OverDetail();
        OverDetailInfo overDetailInfo = new OverDetailInfo();
        Player bowler = CommonUtil.convertPlayerInfoToPlayer(playerService
                            .viewPlayer(bowlerId));
        Match match = matchService.retrieveMatch(matchId);
        overDetail.setBowler(bowler);
        overDetail.setMatch(match);
        overDetail.setBallResult(fetchBallResult(4));
        ballResult = overDetail.getBallResult().toString();
        overDetail.setRuns(BallResult.fetchRunsFromBallResult(overDetail
                .getBallResult()));
        if (ballResult.equals("One") || ballResult.equals("Three") ||
                ballResult.equals("Five") || ballResult.equals("Byes") ||
                ballResult.equals("Leg_Byes")) {
            if (strikerId == batsmanId) {
                strikerId = batsmanId;
            } else {
                strikerId = secondBatsmanId;
            }
        }
        Player batsman = CommonUtil.convertPlayerInfoToPlayer(playerService
                            .viewPlayer(strikerId));
        overDetail.setBatsman(batsman);
        over = overService.addOverDetailToOver(overId, overDetail);
        return getBallInformation(over, overDetail, strikerId);
    }

    @Override
    public BallResult fetchBallResult(int limit) {
        int randomNumber = 0;
        int firstIndex;
        int secondIndex;
        Random random = new Random();
        String[][] randomResult = {{"Dot", "Dot", "Dot", "One", "One"},
                                          {"One", "Two", "Wicket", "Three", 
                                          "Four"}, {"Six", "Two", "One", "Dot", 
                                          "Wide"}, {"No_Ball", "Dot", 
                                          "Leg_Byes", "Byes", "Wicket"}, 
                                          {"Four", "Wide", "Byes", "Six", 
                                          "Leg_Byes"}};
        firstIndex = random.nextInt(limit);
        secondIndex = random.nextInt(limit);
        return BallResult.valueOf(randomResult[firstIndex][secondIndex]);
    }

    @Override
    public JSONObject getBallInformation(Over over, OverDetail overDetail, 
            int strikerId) {
        JSONObject ballInfo = new JSONObject();
        ballInfo.put("overRuns", over.getRuns());
        ballInfo.put("wickets", over.getWicketCount());
        ballInfo.put("fours", over.getFoursCount());
        ballInfo.put("sixes", over.getSixesCount());
        ballInfo.put("overNumber", over.getOverNumber());
        ballInfo.put("ballRuns", overDetail.getRuns());
        ballInfo.put("strikerId", strikerId);
        ballInfo.put("ballNumber", overDetail.getBallNumber());
        ballInfo.put("ballResult", overDetail.getBallResult());
        return ballInfo;
    }
}
