package com.ideas2it.cricbatball.service;

import com.ideas2it.cricbatball.common.BallResult;
import com.ideas2it.cricbatball.entities.Match;
import com.ideas2it.cricbatball.entities.Over;
import com.ideas2it.cricbatball.entities.OverDetail;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.info.OverInfo;
import com.ideas2it.cricbatball.info.OverDetailInfo;
import com.ideas2it.cricbatball.info.MatchInfo;
import com.ideas2it.cricbatball.info.OverInfo;
import com.ideas2it.cricbatball.info.PlayerInfo;
import com.ideas2it.cricbatball.info.TeamInfo;

/**
 * Used to process the data sent from the controller and sends information back.
 *
 * @author    M.Sathish Varma created on 6th July 2019.
 */
public interface OverService {

    /**
     * Used to get over Id, batsman, bowler and match information.
     * @param batsmanIds[] - contains the selected batsman id from the team
     *                       players available.
     * @param bowlerId[] - contains the selected bowler id from the team 
                         - players available.
     * @param matchId - contains the id of match where the over is bowled.
     * @param firstTeamId - team Id of the batsman.
     * @param secondTeamId - teamId of the bowler.
     * @return - sends batsman, bowler, over and matchInformation. 
     */
    OverDetailInfo getPlayersToStart(String[] batsmanIds, String[] bowlerId, 
            int matchId, int firstTeamId, int secondTeamId) 
            throws CricBatBallException;

    /**
     * Used to get overInformation with the help of the id.
     *
     * @param id -used to retrieve over information.
     * @return over - sends back over information.
     */
    Over fetchOver(int overId) throws CricBatBallException;

    /**
     * Used to update the over information with help of overDetail.
     *
     * @param over - containing updated over information.
     */
    void updateOver(Over over) throws CricBatBallException;

    /**
     * Used to update the over with over detail information
     * update the over information like runs, count of wides, noball, byes.
     *
     * @param overId - overId used to get the information to be updated.
     * @param overDetail - used to update the over information.
     */
    Over addOverDetailToOver(int overId, OverDetail overDetail) 
            throws CricBatBallException;

    /**
     * Used to count runs and store overdetail information to over information.
     *
     * @param over - over information to be updated with over detail information
     * @param overDetail - information used to update over information.
     */
    void storeOverDetailToOver(Over over, OverDetail overDetail);

    /**
     * Used to create the next Over if previous over is completed.
     *
     * @param match - contains information of match where over is bowled before.
     * @param overDetail - contains generated overdetail for over to be created.
     * @return over -sends the newly created over.
     */
    Over createNextOver(Match match) throws CricBatBallException;

    /**
     * Used to get bowlers in the team and remove bowler 
     * who bowled the previous over.
     * 
     * @param teamId - teamId refers to bowling team used to get bowlers.
     * @param bowlerId - id of the bowler who bowled previous bowler.
     * @return teamInfo - sends the team with bowlers.
     */
    TeamInfo retrieveNextBowler(int teamId, int bowlerId) 
            throws CricBatBallException;

    /**
     * Used to store the previous over information to the next over.
     *
     * @param overDetailInfo - contains the id of match, teams, players.
     * @param bowlerId - id of bowler who has to bowl the next Over.
     */
    void getOverDetailInformation(OverDetailInfo overDetailInfo, int bowlerId) 
            throws CricBatBallException;

    /**
     * Used to get batsman in the team and remove batsman 
     * who is out.
     * 
     * @param wicketId - wicketId refers to batsman who is out.
     * @param overDetailInfo - contains the id of match, teams, players.
     * @return teamInfo - sends the team with batsman.
     */
    TeamInfo retrieveNextBatsman(int wicketId, OverDetailInfo overDetailInfo)
            throws CricBatBallException;

    /**
     * Used to store the previous over information to the next over.
     *
     * @param overDetailInfo - contains the id of match, teams, players.
     * @param batsmanId - id of batsman who is next to bat.
     */
    void getOverDetailInfo(OverDetailInfo overDetailInfo, int batsmanId) 
            throws CricBatBallException;
}

