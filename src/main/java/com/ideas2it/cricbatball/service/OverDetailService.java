package com.ideas2it.cricbatball.service;

import org.json.JSONObject;

import com.ideas2it.cricbatball.common.BallResult;
import com.ideas2it.cricbatball.entities.Over;
import com.ideas2it.cricbatball.entities.OverDetail;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.info.OverInfo;
import com.ideas2it.cricbatball.info.MatchInfo;
import com.ideas2it.cricbatball.info.OverDetailInfo;
import com.ideas2it.cricbatball.info.PlayerInfo;

/**
 * Used to process the data sent from the controller and sends information back.
 *
 * @author    M.Sathish Varma created on 6th July 2019.
 */
public interface OverDetailService {

    /**
     * Used to get overDetailInfo.
     ** @param batsmanId - contains the batsman who is in strike.
     * @param bowlerId - contains bowler id who is going to bowl the ball.
     * @param matchId - contains the id of match where the over is bowled. 
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains overDetail information.
     */
    JSONObject getOverDetailInfo(int batsmanId, int bowlerId, int matchId, 
            int overId, int strikerId, int secondBatsmanId) 
            throws CricBatBallException;

    /**
     * Used to get ballResult by generation of random number.
     * With help of random number ball result is obtained.
     *
     * @param limit - contains the end limit of random number to be generated. 
     * @return ballResult - sends back the obtained ball result.
     */  
    BallResult fetchBallResult(int limit);

    /**
     * Used to convert ball information to json array as response for ajax call.
     *
     * @param overDetail - contains overdetails information to be converted.
     * @return ballInformation - sends over detail information as jsonArray.
     */
    JSONObject getBallInformation(Over over, OverDetail overDetail, 
            int strikerId);
}

