package com.ideas2it.cricbatball.service;

import com.ideas2it.cricbatball.entities.User;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.info.UserInfo;

public interface UserService {

    /**
     * Used to store the user information into the database.
     * @param name -name of the user.
     * @param emailId - emailId of the user to be inserted.
     * @param mobileNumber - contact number of the user  
     * @param role - role of the user.
     * @param gender - gender of the user.
     * @param password - password of the user.
     * @throws CricBatBallException - user defined exception.
     */
    boolean createUser(UserInfo userInfo) throws CricBatBallException;

    /**
     * Used to check whether the user exists or not.
     *
     * @param emailId - email of the user to be checked.
     * @param password - password of the user to be checked.
     * @return - sends back 1 if id exists else returns '0'.
     * @throws CricBatBallException - user defined exception.
     */
    boolean checkUser(String emailId, String password)
            throws CricBatBallException;
    
    /**
     * Used to check the failed attempts of the password.
     *
     * @param emailId - emailId of the user to be checked.
     * @throws CricBatBallException - user defined exception.
     */
    void checkFailedAttempts(String emailId) throws CricBatBallException;

    /**
     * Used to get the user information with the help of the emailId.
     *
     * @param emailId - email of the user used to get the information.
     * @param user - sends back the user information to be updated.
     * @throws CricBatBallException - user defined exception.
     */
    User getUser(String emailId) throws CricBatBallException;

    /**
     * Used to update the userinformation.
     *
     * @param user - contains user information to be updated.
     */
    void updateUserInfo(User user) throws CricBatBallException;
}
