package com.ideas2it.cricbatball.service;

import java.util.List;
import java.util.Date;

import org.json.JSONArray;

import com.ideas2it.cricbatball.common.MatchType;
import com.ideas2it.cricbatball.entities.Match;
import com.ideas2it.cricbatball.entities.Team;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.info.MatchInfo;
import com.ideas2it.cricbatball.info.MatchPaginationInfo;
import com.ideas2it.cricbatball.info.TeamInfo;

/**
 * Used to process the data sent from the controller and sends information back.
 *
 * @author    M.Sathish Varma created on 6th July 2019.
 */
public interface MatchService {

    /**
     * Used to insert the match information into the database.
     *
     * @param match - contains match information to be inserted.  
     * @throws CricBatBallException - user defined exception.
     */
    MatchPaginationInfo insertMatch(String date, MatchInfo matchInfo)
            throws CricBatBallException;

    /**
     * Used to retrieve the team from the database with the help of database.
     *
     * @param matchId - unique id of the team to be displayed.
     * @throws CricBatBallException - user defined exception.
     */
    Match retrieveMatch(int matchId) throws CricBatBallException;

    /**
     * Used to retrieve all the matches in the database.
     *
     * @param offset - contains id of the match before the starting position.
     * @param limit - number of records to be fetched.
     * @throws CricBatBallException - user defined exception.
     */
    List<MatchInfo> retrieveMatches(int offset, int limit)
            throws CricBatBallException;

    /**
     * Used to add the teams with the help of the teamIds.
     *
     * @param id - id of the match to which teams to be added.
     * @param teamIds - consists of ids of team seleted to add for match.  
     */
    void addTeamsToMatch(int id, String[] teamIds) 
            throws CricBatBallException;

    /**
     * Used to retrieve the match information stored into the matchInfo
     * and return the matchInfo.
     *
     * @param id - id used to fetch match information.
     * @param matchInfo - contains the matchInformation to be retrieved.
     */
    MatchInfo retrieveMatchToView(int id) throws CricBatBallException;

    /**
     * Used to update the match information.
     *
     * @param match - containing the information of match to be updated.
     * @throws CricBatBallException - user defined exception.
     */
    void updateMatch(Match match) throws CricBatBallException;

    /**
     * Used to delete the match information.
     *
     * @param matchId - unique id of the match to be deleted.
     * @throws CricBatBallException - user defined exception.
     */
    void deleteMatch(int matchId) throws CricBatBallException;

    /**
     * Used to update the matchInformtion details.
     *
     * @param id - used the get the matchinformation with help of id.
     * @param name - name of the match to be updated.
     * @param location - contains the location of the match to be played.
     * @param dateOfPlay - date of the match to be played.
     * @param startingTime - time of the match to be started.
     * @param matchType - type of match to be played.
     * @param teamsId - id of the team to be added.
     * @throws CricBatBallException - user defined exception.
     */
    void updateMatchInformation(String date, int id, String[] teamsId, 
            MatchInfo matchInfo) throws CricBatBallException;

    /**
     * Used to fetch the teams to complete the team creation.
     *
     * @return - sends back the list team available to create the match.
     * @throws CricBatBallException - user defined exception.
     */
    List<Team> fetchTeams() throws CricBatBallException;

    /**
     * Used to add team to the match.
     *
     * @param matchId - unique id to add the team.
     * @param teamsId - unique id of the team to be added.
     * @throws CricBatBallException - user defined exception.
     */
    void addTeams(Match match, String[] teamsId) 
            throws CricBatBallException;

    /**
     * Used to get the MatchInformation for the purpose of updation.
     *
     * @param matchId - contains the match id to be updated .
     * @return matchPaginationInfo - contains match and team information.
     */
    MatchPaginationInfo getTeamsToUpdateMatch(int matchId)  
            throws CricBatBallException;

    /**
     * Used to check the team is valid to create he match.
     *
     * @param matchId - id used to get the teams.
     * @return - sends back the list of teams.
    * @throws CricBatBallException - user defined exception.
     */
    List<TeamInfo> getTeamsForMatch(int matchId) 
            throws CricBatBallException;

    /**
     * Gets the total number of records from the database.
     *
     * @return - sends back number of records in the database.
     * @throws CricBatBallException - user defined exception.
     */
    int totalMatches() throws CricBatBallException;

    /**
     * Used the send back list of match object as the json array.
     *
     * @param matches - list of matches to be converted.
     * @return - sends back the list as the json array.
     * @throws CricBatBallException - user defined exception.
     */
    JSONArray getMatchesForPagination(List<MatchInfo> matches)
            throws CricBatBallException;

    /**
     * Used to get the match information for pagination and 
     * Also contains page information.
     *
     * @param page - the page number is used to fetch the player information.
     * @return matchInfo - sends back the information used for pagination.
     */
    MatchPaginationInfo getPaginationInfo(int page) throws CricBatBallException;

    /**
     * Used to get the teams information involved in the match.
     *
     * @param id- id used to get the team information from the match.
     */
    MatchPaginationInfo fetchTeamsToStartMatch(int id) 
            throws CricBatBallException;
}
