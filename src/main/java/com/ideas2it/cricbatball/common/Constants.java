package com.ideas2it.cricbatball.common;

/**
 * Contains the constants values of all the entities.
 * The values used often without changing it whenever we use.
 * That values are stored here.
 *
 * @author M.Sathish Varma created on 12th June 2019.
 */
public class Constants {

    public final static int OPTION_ONE = 1;
    public final static int OPTION_TWO = 2;
    public final static int OPTION_THREE = 3;
    public final static int OPTION_FOUR = 4;
    public final static int OPTION_FIVE = 5;
    public final static int OPTION_SIX = 6;
    public final static int OPTION_SEVEN = 7;
    public final static int OPTION_EIGHT = 8;
    public final static int OPTION_NINE = 9;

    // label constants for date.
    public final static String YEARS = " years ";
    public final static String MONTHS = " months ";
    public final static String DAYS = " days ";
    public final static String DATE_FORMAT = "yyyy-MM-dd";
    public final static String ERROR_IN_DATE_CONVERSION = 
            "Error while converting date";

    // label constants for player entity.
    public final static String ID = "id";
    public final static String EDIT_ID = "editId";
    public final static String DELETE_ID = "deleteId";
    public final static String NAME = "name";
    public final static String NAMES = "names";
    public final static String BATTING_STYLE = "battingStyle";
    public final static String BOWLING_STYLE = "bowlingStyle";
    public final static String COUNTRY = "country";
    public final static String DATE_OF_BIRTH = "dateOfBirth"; 
    public final static String ROLE = "role";
    public final static String COMMA = ",";
    public final static String BATSMAN = "Batsman";
    public final static String BOWLER = "Bowler";
    public final static String ALL_ROUNDER = "All-Rounder";
    public final static String WICKET_KEEPER = "Wicket-keeper";    
    public final static String ADDRESS = "address";
    public final static String MOBILE_NUMBER = "mobileNumber";
    public final static String PINCODE = "pincode";
    public final static String PLAYER = "player";
    public final static String PLAYERS = "players";
    public final static String CONTACT = "contact";
    public final static String ERROR = "error";
    public final static String SAVE = "save";
    public final static String SUBMIT = "submit";   
    public final static String FORWARDTOCREATE = "forwardToCreate";
    public final static String VIEWPLAYER = "viewPlayer";
    public final static String VIEWPLAYERS = "viewPlayers";
    public final static String GET_PLAYER = "getPlayer";
    public final static String FETCH_PLAYER = "fetchPlayer";
    public final static String DISPLAY_PLAYERS = "displayPlayers";
    public final static String DISPLAY_PLAYERS_INFO = "displayPlayersInfo";
    public final static String PLAYER_INFO = "playerInfo";
    public final static String PLAYER_INFORMATION = "PlayerInformation";
    public final static String PLAYER_PAGINATION_INFO = "playerPaginationInfo";
    public final static String REDIRECT_DISPLAY_PLAYERS = 
            "redirect:/displayPlayers";
    public final static String VIEWALLPLAYERSINFO = "viewAllPlayersInfo";
    public final static String UPDATEPLAYER = "updatePlayer";
    public final static String UPDATE_PLAYER = "updateplayer";
    public final static String CREATE_PLAYER = "createplayer";
    public final static String DELETE_PLAYER = "deletePlayer";
    public final static String PLAYER_IMAGE = "playerImage";
    public final static String IMAGE_PATH = "/home/ubuntu/playerImages/";
    public final static String LOCALHOST_PATH = 
            "http://localhost:8080/playerImages/";
    public final static String PAGE = "page";
    public final static String TOTALPAGES = "totalPages";
    public final static String CURRENTPAGE = "currentPage";
    public final static String APPLICATION_JSON = "application/json";    
    public final static String INSERT_PLAYER_ERROR_MESSAGE =
            "error while inserting player information ";
    public final static String FETCHING_PLAYER_ERROR_MESSAGE =
            "error while fetching player information ";
    public final static String FETCHING_PLAYERS_ERROR_MESSAGE =
            "error while fetching all player information ";
    public final static String UPDATING_PLAYER_ERROR_MESSAGE =
            "error while updating player information ";
    public final static String REMOVING_PLAYER_ERROR_MESSAGE = 
            "error while deleting player information ";
    public final static String PLAYER_COUNTING_ERROR_MESSAGE =
            "error while counting total player ";
    public final static String ERROR_GETTING_PLAYER_BY_COUNTRY = 
            "error while getting player by country ";
    public final static String ERROR_GETTING_PLAYERS_BY_ID = 
            "error while getting players information by id ";
    public final static String MESSAGE = 
            "player created successfully !!!!";
    public final static String SUCCESS_MESSAGE = "successMessage";
    public final static String UPDATE_INFO = 
            "player updated successfully !!!!";
    public final static String UPDATE_MESSAGE = "updateMessage";

    // label for error constants for player
    public final static String ERROR_IN_SWITCH =
            "error in controller switch";

    // label for user entity
    public final static String CREATE_USER = "createUser";
    public final static String USER_INFO = "userInfo";
    public final static String CHECK_USER = "checkUser";
    public final static String LOGOUT = "logOut";
    public final static String EMAILID = "emailId";
    public final static String INDEX = "index";
    public final static String PASSWORD = "password";    
    public final static String LOGIN = "login";   
    public final static String MANAGE = "manage";   
    public final static String FORWARDTOCREATEUSER = "forwardToCreateUser";
    public final static String WRONG_DATA = "wrongData";
    public final static String INVALID_PASSWORD = "invalid password!!!";
    public final static String FAILED_ATTEMPTS = "failedAttempts";
    public final static String ATTEMPTS_WARNING = "You are Blocked!!!";
    public final static String INVALID_EMAIL = "invalid email!!!";
    public final static String DUPLICATE = "duplicate";
    public final static String EMAIL_EXISTS = "Email already exists";
    public final static String INSERT_USER_ERROR_MESSAGE = 
            "error while inserting user information ";
    public final static String FETCHING_USER_ERROR_MESSAGE = 
            "error while getting user information having emailId";
    public final static String CHECKING_USER_ERROR_MESSAGE =
            "error while checking user existence ";
    public final static String UPDATING_USER_ERROR_MESSAGE = 
            "error while updating user information ";

    // label constants for team entity
    public final static String CREATE_TEAM = "createTeam";
    public final static String ADD_PLAYERS = "addPlayers";
    public final static String GET_TEAMS = "getTeams";
    public final static String GET_TEAMS_INFO = "getTeamsInfo";
    public final static String GET_TEAM = "getTeam";
    public final static String TEAM_INFO = "teamInfo";
    public final static String UPDATE_TEAM = "updateTeam";
    public final static String UPDATETEAM = "updateTeamInfo";
    public final static String DELETE_TEAM = "deleteTeam";
    public final static String TEAMS = "teams";
    public final static String TEAM = "team";
    public final static String TEAMNAME = "teamName";
    public final static String TEAMID = "teamid";
    public final static String BATSMAN_COUNT = "batsmanCount";
    public final static String BOWLER_COUNT = "bowlerCount";
    public final static String WICKET_KEEPER_COUNT = "wicketKeeperCount";
    public final static String ALL_ROUNDER_COUNT = "allRounderCount";
    public final static String TOTAL_COUNT = "totalCount";
    public final static String STATUS = "status";
    public final static String COMPLETE = "complete";
    public final static String INCOMPLETE ="incomplete";
    public final static String PLAYERS_ID = "playersid";
    public final static String TEAM_ID = "teamId";
    public final static String PLAYERSNOTINTEAM = "playersNotInTeam";
    public final static String TEAM_NAME = "teamname";
    public final static String ADDPLAYERS = "addPlayers";
    public final static String REMOVEPLAYERS = "removePlayers";  
    public final static String INSERT_TEAM_ERROR_MESSAGE =
            "Error while saving team";
    public final static String FETCHING_TEAM_ERROR_MESSAGE =
            "error while getting team information ";
    public final static String FETCHING_TEAMS_ERROR_MESSAGE = 
            "error while getting all teams information "; 
    public final static String UPDATING_TEAM_ERROR_MESSAGE =
            "error while updating team information ";
    public final static String REMOVING_TEAM_ERROR_MESSAGE = 
            "error while removing team information ";
    public final static String TEAM_COUNTING_ERROR_MESSAGE = 
            "error while getting total teams count "; 
    public final static String ERROR_GETTING_TEAM_BY_STATUS =   
            "error while getting team by status for match ";
    public final static String ERROR_GETTING_PLAYERS_IN_TEAM =  
            "error while getting players in the team ";

    // label constants for match entity.   
    public final static String FORWARD_TO_CREATE_MATCH = "forwardToCreateMatch";   
    public final static String FORWARD_TO_UPDATE_MATCH = "forwardToUpdateMatch";
    public final static String REDIRECT_GETMATCHES = "redirect:/getMatches";
    public final static String CREATEMATCH = "createMatch";
    public final static String DISPLAY_TEAM_DETAILS = "displayTeamDetails"; 
    public final static String ADDTEAMS = "addTeams";
    public final static String GETMATCHES = "getMatches";
    public final static String GETMATCH = "getMatch";
    public final static String VIEWMATCHES = "viewMatches";
    public final static String VIEWMATCH = "viewMatch";
    public final static String GETMATCHESINFO = "getMatchesInfo";
    public final static String UPDATEMATCH = "updateMatch";
    public final static String MATCH_INFO = "matchInfo";
    public final static String MATCH_PAGINATION_INFO = "matchPaginationInfo";
    public final static String UPDATEMATCHINFO = "updateMatchInfo";
    public final static String DELETEMATCH = "deleteMatch";
    public final static String LOCATION = "location";
    public final static String DATEOFPLAY = "dateOfPlay";
    public final static String MATCHTYPE = "matchType";
    public final static String STARTINGTIME = "startingTime";
    public final static String DATE = "date";
    public final static String MATCH = "match";
    public final static String MATCH_DATE = "matchDate";
    public final static String MATCHID = "matchId";
    public final static String TEAMSFORMATCH = "teamsForMatch";
    public final static String MATCHES = "matches";
    public final static String MATCHNAME = "matchName";    
    public final static String INSERT_MATCH_ERROR_MESSAGE = 
            "error while inserting match information ";
    public final static String FETCHING_MATCH_ERROR_MESSAGE = 
            "error while retrieving match information " ;    
    public final static String FETCHING_MATCHES_ERROR_MESSAGE = 
            "error while retrieving all match information ";    
    public final static String UPDATING_MATCH_ERROR_MESSAGE = 
            "error while updating match information ";    
    public final static String REMOVING_MATCH_ERROR_MESSAGE = 
            "error while removing match information ";    
    public final static String MATCH_COUNTING_ERROR_MESSAGE = 
            "error while retrieving total matches count";
    public final static String START_PLAY = "startPlay";

    // label constants for over and overdetail entity.
    public final static String BATSMAN_ID = "batsmanId";
    public final static String BATSMAN_IDS = "batsmanIds";
    public final static String BOWLER_ID = "bowlerId";
    public final static String BOWLER_IDS = "bowlerIds";
    public final static String SELECT_PLAYERS = "selectPlayers";
    public final static String START_MATCH = "/startMatch";
    public final static String SCORECARD_PAGE = "scorecardPage";
    public final static String OVER_ID = "overId";
    public final static String INSERT_OVER_ERROR_MESSAGE = 
            "error while inserting over Information";
    public final static String ERROR_IN_SESSION = "error in session";
    public final static String OVER_COUNTING_ERROR_MESSAGE = 
            "error while over number counting of matchId ";
    public static final String FETCHING_OVER_ERROR_MESSAGE = 
            "error while fetching over information with id ";
    public static final String UPDATING_OVER_ERROR_MESSAGE = 
            "error while updating over with id";
    public static final String CHOOSE_BATSMAN = "chooseBatsman";
    public static final String CHOOSE_BOWLER = "chooseBowler";
    public static final String CONTINUE_PLAY = "continuePlay";
    public static final String CONTINUE_PLAY_With_BATSMAN = 
            "continuePlayWithBatsman";
    public static final String GET_BALL_RESULT = "getBallResult";
    public static final String STRIKER_ID = "strikerId";
    public static final String SECOND_BATSMAN_ID = "secondBatsmanId";
    public static final String FIRST_TEAM_ID = "firstTeamId";
    public static final String SECOND_TEAM_ID = "secondTeamId";
    public static final String OVER_DETAIL_INFO = "overDetailInfo";
    public static final String SELECT_BOWLER = "selectBowler";
    public static final String SELECT_BATSMAN = "selectBatsman";
    public final static String WICKET_ID = "wicketId";
 }
