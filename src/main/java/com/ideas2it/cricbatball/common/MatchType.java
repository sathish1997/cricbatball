package com.ideas2it.cricbatball.common;

/**
 * Used to select the match format type for creation of match.
 *
 * @author    M.Sathish Varma created on 5th July 2019.
 */

public enum MatchType {
    overs_20,
    overs_50,
    Test;
}
