package com.ideas2it.cricbatball.common;

/**
 * Contains the country which are finite in number who plays the cricket.
 * Player and team can only select the country available in this enum class.
 *
 * @author M.Sathish Varma.
 */
public enum Country {

    India, 
    Australia,
    England,
    South_Africa,
    West_Indies,
    Pakistan,
    Bangladesh,
    Sri_Lanka,
    Afghanistan,
    New_Zealand;
}
