package com.ideas2it.cricbatball.common;

/**
 * Contains the predicted results of the ball bowled in an over.
 *
 * @author M.Sathish Varma.
 */
public enum BallResult {

    Dot,
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Wide,
    No_Ball,
    Leg_Byes,
    Byes,
    Wicket;

    public static BallResult fetchBallresult(int index) {
        switch (index) {
            case 0 :
                return BallResult.values()[index];
            case 1 :
                return BallResult.values()[index];
            case 2 :
                return BallResult.values()[index];
            case 3 :
                return BallResult.values()[index];
            case 4:
                return BallResult.values()[index];
            case 5:
                return BallResult.values()[index];
            case 6:
                return BallResult.values()[index];
            case 7:
                return BallResult.values()[index];
            case 8:
                return BallResult.values()[index];
            case 9:
                return BallResult.values()[index];
            case 10:
                return BallResult.values()[index];
            case 11:
                return BallResult.values()[index];
            default:
                return BallResult.values()[0];
        }
    }

    public static int fetchRunsFromBallResult(BallResult ballResult) {
        switch (ballResult) {
            case Dot:
                return 0;
            case Wicket:
                return 0;
            case One:
                return 1;
            case Two:
                return 2;
            case Three:
                return 3;
            case Four:
                return 4;
            case Five:
                return 5;
            case Six:
                return 6;
            default:
                return 1;
        }
    }
}
