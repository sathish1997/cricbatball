package com.ideas2it.cricbatball.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import com.ideas2it.cricbatball.entities.Contact;
import com.ideas2it.cricbatball.entities.Match;
import com.ideas2it.cricbatball.entities.Over;
import com.ideas2it.cricbatball.entities.OverDetail;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.entities.Team;
import com.ideas2it.cricbatball.entities.User;
import com.ideas2it.cricbatball.info.OverInfo;
import com.ideas2it.cricbatball.info.OverDetailInfo;
import com.ideas2it.cricbatball.info.MatchInfo;
import com.ideas2it.cricbatball.info.PlayerInfo;
import com.ideas2it.cricbatball.info.TeamInfo;
import com.ideas2it.cricbatball.info.UserInfo;

/**
 * Contains conversion of info object to entities and vice versa.
 * It also used to contain operation that are common to all the entities.
 *
 * @author M.Sathish Varma
 */
public class CommonUtil {

    /**
     * Used to extract the match information from matchInfo and store it 
     * in the match.
     *
     * @param matchInfo - contains the match information to be extracted.
     * @return match - contains the extracted match information.
     */
    public static Match convertMatchInfoToMatch(MatchInfo matchInfo) {
        Match match = new Match();
        Set<Team> teams = new HashSet<Team>();
        match.setId(matchInfo.getId());
        if (null != matchInfo.getName()) {
            match.setName(matchInfo.getName());
        }
        if (null != matchInfo.getLocation()) {
            match.setLocation(matchInfo.getLocation());
        }
        if (null != matchInfo.getDateOfPlay()) {
            match.setDateOfPlay(matchInfo.getDateOfPlay());
        }
        if (null != matchInfo.getStartingTime()) {
            match.setStartingTime(matchInfo.getStartingTime());
        }
        if (null != matchInfo.getMatchType()) {
            match.setMatchType(matchInfo.getMatchType());
        }
        if (null != matchInfo.getTeams()) {
            for (TeamInfo team : matchInfo.getTeams()) {
                teams.add(convertTeamInfoToTeam(team));
            }
            match.setTeams(teams);
        }
        return match;
    }

    /**
     * Used to store the match information into matchInfo.
     *
     * @param match - contains the match information to be extracted.
     * @return matchInfo - contains the extracted match information.
     */
    public static MatchInfo convertMatchToMatchInfo(Match match) {      
        MatchInfo matchInfo = new MatchInfo();
        Set<TeamInfo> teams = new HashSet<TeamInfo>();
        matchInfo.setId(match.getId());
        if (null != match.getName()) {
            matchInfo.setName(match.getName());
        }
        if (null != match.getLocation()) {
            matchInfo.setLocation(match.getLocation());
        }
        if (null != match.getDateOfPlay()) {
            matchInfo.setDateOfPlay(match.getDateOfPlay());
        }
        if (null != match.getStartingTime()) {
            matchInfo.setStartingTime(match.getStartingTime());
        }
        if (null != match.getMatchType()) {
            matchInfo.setMatchType(match.getMatchType());
        }
        if (null != match.getTeams()) {
            for (Team team : match.getTeams()) {
                teams.add(convertTeamToTeamInfo(team));
            }
            matchInfo.setTeams(teams);
        }
        return matchInfo;
    }

    /**
     * Used to store the match information into matchInfo.
     *
     * @param match - contains the match information to be extracted.
     * @return matchInfo - contains the extracted match information.
     */
    public static MatchInfo convertMatchToMatchInformation(Match match) {      
        MatchInfo matchInfo = new MatchInfo();
        Set<TeamInfo> teams = new HashSet<TeamInfo>();
        matchInfo.setId(match.getId());
        if (null != match.getName()) {
            matchInfo.setName(match.getName());
        }
        if (null != match.getLocation()) {
            matchInfo.setLocation(match.getLocation());
        }
        if (null != match.getDateOfPlay()) {
            matchInfo.setDateOfPlay(match.getDateOfPlay());
        }
        if (null != match.getStartingTime()) {
            matchInfo.setStartingTime(match.getStartingTime());
        }
        if (null != match.getMatchType()) {
            matchInfo.setMatchType(match.getMatchType());
        }
        if (null != match.getTeams()) {
            for (Team team : match.getTeams()) {
                teams.add(convertTeamToTeamInformation(team));
            }
            matchInfo.setTeams(teams);
        }
        return matchInfo;
    }

    /**
     * Used to extract the player information from playerInfo and store it 
     * in the player.
     *
     * @param playerInfo - contains the match information to be extracted.
     * @return player - contains the extracted player information.
     */ 
    public static Player convertPlayerInfoToPlayer(PlayerInfo playerInfo) {
        Contact contact = new Contact();
        Player player = new Player();
        if (null != player.getContact()) {
            contact = player.getContact();
        }
        player.setStatus(Boolean.TRUE);
        player.setId(playerInfo.getId());
        if (null != playerInfo.getName()) {
            player.setName(playerInfo.getName());
        }
        if (null != playerInfo.getRole()) {
            player.setRole(playerInfo.getRole());
        }
        if (null != playerInfo.getBattingStyle()) {
            player.setBattingStyle(playerInfo.getBattingStyle());
        }
        if (null != playerInfo.getBowlingStyle()) {
            player.setBowlingStyle(playerInfo.getBowlingStyle());
        }
        if (null != playerInfo.getCountry()) {
            player.setCountry(playerInfo.getCountry());
        }
        if (null != playerInfo.getDateOfBirth()) {
            player.setDateOfBirth(playerInfo.getDateOfBirth());
        }
        if (null != playerInfo.getImagePath()) {
            player.setImagePath(playerInfo.getImagePath());
        }
        if (null != playerInfo.getAddress()) {
            contact.setAddress(playerInfo.getAddress());
        }
        if (null != playerInfo.getTeamInfo()) {
            player.setTeam(convertTeamInfoToTeam(playerInfo
                    .getTeamInfo()));
        }
        contact.setId(playerInfo.getContactId());
        contact.setMobileNumber(playerInfo.getMobileNumber());
        contact.setPincode(playerInfo.getPincode());
        player.setContact(contact);
        contact.setPlayer(player);
        player.setContact(contact);
        return player;
    }
    
    /**
     * Used to store the player information into playerInfo.
     *
     * @param player - contains the player information to be extracted.
     * @return playerInfo - contains the extracted player information.
     */
    public static PlayerInfo convertPlayerToPlayerInfo(Player player) {
        PlayerInfo playerInfo = new PlayerInfo();
        Contact contact = player.getContact();
        playerInfo.setId(player.getId());
        if (null != player.getName()) {
            playerInfo.setName(player.getName());
        }
        if (null != player.getRole()) {
            playerInfo.setRole(player.getRole());
        }
        if (null != player.getBattingStyle()) {
            playerInfo.setBattingStyle(player.getBattingStyle());
        }
        if (null != player.getBowlingStyle()) {
            playerInfo.setBowlingStyle(player.getBowlingStyle());
        }
        if (null != player.getCountry()) {
            playerInfo.setCountry(player.getCountry());
        }
        if (null != player.getDateOfBirth()) {
            playerInfo.setDateOfBirth(player.getDateOfBirth());
        }
        if (null != player.getImagePath()) {
            playerInfo.setImagePath(player.getImagePath());
        }
        playerInfo.setContactId(contact.getId());
        if (null != contact.getAddress()) {
            playerInfo.setAddress(contact.getAddress());
        }
        playerInfo.setMobileNumber(contact.getMobileNumber());
        playerInfo.setPincode(contact.getPincode());
        return playerInfo;
    }

    /**
     * Used to extract the team information from teamInfo and store it 
     * in the team.
     *
     * @param playerInfo - contains the match information to be extracted.
     * @return player - contains the extracted player information.
     */
    public static Team convertTeamInfoToTeam(TeamInfo teamInfo) {
        Team team = new Team();
        if (null != teamInfo.getName()) {
            team.setName(teamInfo.getName());
        }
        if (null != teamInfo.getCountry()) {
            team.setCountry(teamInfo.getCountry());
        }
        team.setId(teamInfo.getId());
        return team;
    }

    /**
     * Used to store the team information into teamInfo.
     *
     * @param team - contains the team information to be extracted.
     * @return teamInfo - contains the extracted team information.
     */
    public static TeamInfo convertTeamToTeamInfo(Team team) {
        TeamInfo teamInfo = new TeamInfo();
        List<PlayerInfo> players = new ArrayList<PlayerInfo>();
        if (null != team.getName()) {
            teamInfo.setName(team.getName());
        }
        if (null != team.getCountry()) {
            teamInfo.setCountry(team.getCountry());
        }        
        teamInfo.setId(team.getId());
        return teamInfo;
    }

    /**
     * Used to store the team information into teamInfo.
     *
     * @param team - contains the team information to be extracted.
     * @return teamInfo - contains the extracted team information.
     */
    public static TeamInfo convertTeamToTeamInformation(Team team) {
        TeamInfo teamInfo = new TeamInfo();
        List<PlayerInfo> players = new ArrayList<PlayerInfo>();
        if (null != team.getName()) {
            teamInfo.setName(team.getName());
        }
        if (null != team.getCountry()) {
            teamInfo.setCountry(team.getCountry());
        }
        if (null != team.getPlayers()) {
            for (Player player : team.getPlayers()) {
                players.add(convertPlayerToPlayerInfo(player));
            }
            teamInfo.setPlayers(new HashSet<PlayerInfo>(players));
        }
        teamInfo.setId(team.getId());
        return teamInfo;
    }

    /**
     * Used to extract the user information from the userInfo object.
     * The Extracted data is used to insert the information into the database.
     * 
     * @param userInfo - contains the user information to be inserted
     */
    public static User convertUserInfoToUser(UserInfo userInfo) {
        User user = new User();
        user.setStatus(Boolean.TRUE);
        user.setMobileNumber(userInfo.getMobileNumber());
        if (null != userInfo.getName()) {
            user.setName(userInfo.getName());
        }
        if (null != userInfo.getEmailId()) {
            user.setEmailId(userInfo.getEmailId());
        }
        if (null != userInfo.getRole()) {
            user.setRole(userInfo.getRole());
        }
        if (null != userInfo.getGender()) {
            user.setGender(userInfo.getGender());
        }
        if (null != userInfo.getPassword()) {
            user.setPassword(userInfo.getPassword());
        }
        return user;
    }

    /**
     * Used to extract the overDetail information from overDetailInfo object.
     * The Extracted data is used to insert the information into the database.
     * 
     * @param overDetailInfo - contains overDetail information to be inserted.
     * @return overDetail - sends back overDetail information.
     */
    public static OverDetail convertOverDetailInfoToOverDetail(OverDetailInfo 
            overDetailInfo) {
        OverDetail overDetail = new OverDetail();
        overDetail.setId(overDetailInfo.getId());
        overDetail.setRuns(overDetailInfo.getRuns());
        overDetail.setBallNumber(overDetailInfo.getBallNumber());
        if (null != overDetailInfo.getBallResult()) {
            overDetail.setBallResult(overDetailInfo.getBallResult());
        }
        if (null != overDetailInfo.getFirstBatsman()) {
            overDetail.setBatsman(convertPlayerInfoToPlayer(overDetailInfo
                    .getFirstBatsman()));
        }
        if (null != overDetailInfo.getSecondBatsman()) {
            overDetail.setBatsman(convertPlayerInfoToPlayer(overDetailInfo
                    .getSecondBatsman()));
        }
        if (null != overDetailInfo.getBowler()) {
            overDetail.setBowler(convertPlayerInfoToPlayer(overDetailInfo
                    .getBowler()));
        }
        if (null != overDetailInfo.getMatch()) {
            overDetail.setMatch(convertMatchInfoToMatch(overDetailInfo
                    .getMatch()));
        }
        if (null != overDetailInfo.getOverInfo()) {
            overDetail.setOver(convertOverInfoToOver(overDetailInfo
                    .getOverInfo()));
        }
        return overDetail;
    }

    /**
     * Used to store the overDetail information into overDetailInfo.
     *
     * @param overDetail - contains the overDetail information to be extracted.
     * @return overDetailInfo - contains the extracted overDetail information.
     */
    public static OverDetailInfo convertOverDetailToOverDetailInfo(OverDetail 
            overDetail) {
        OverDetailInfo overDetailInfo = new OverDetailInfo();
        overDetailInfo.setId(overDetail.getId());
        overDetailInfo.setRuns(overDetail.getRuns());
        overDetailInfo.setBallNumber(overDetail.getBallNumber());
        if (null != overDetail.getBallResult()) {
            overDetailInfo.setBallResult(overDetail.getBallResult());
        }
        if (null != overDetail.getBatsman()) {
            overDetailInfo.setFirstBatsman(convertPlayerToPlayerInfo(overDetail
                    .getBatsman()));
        }
        if (null != overDetail.getBowler()) {
            overDetailInfo.setBowler(convertPlayerToPlayerInfo(overDetail
                    .getBowler()));
        }
        if (null != overDetail.getMatch()) {
            overDetailInfo.setMatch(convertMatchToMatchInfo(overDetail
                    .getMatch()));
        }
        if (null != overDetail.getOver()) {
            overDetailInfo.setOverInfo(convertOverToOverInfo(overDetail
                    .getOver()));
        }
        return overDetailInfo;
    }

    /**
     * Used to extract the overDetail information from overDetailInfo object.
     * The Extracted data is used to insert the information into the database.
     * 
     * @param overDetailInfo - contains overDetail information to be inserted.
     * @return overDetail - sends back overDetail information.
     */
    public static Over convertOverInfoToOver(OverInfo overInfo) {
        Over over = new Over();
        over.setId(overInfo.getId());
        over.setOverNumber(overInfo.getOverNumber());
        over.setRuns(overInfo.getRuns());
        over.setWicketCount(overInfo.getWicketCount());
        over.setFoursCount(overInfo.getFoursCount());
        over.setSixesCount(overInfo.getSixesCount());
        over.setSixesCount(overInfo.getSixesCount());
        over.setNoBallCount(overInfo.getNoBallCount());
        over.setWidesCount(overInfo.getWidesCount());
        over.setByesCount(overInfo.getByesCount());
        over.setLegByesCount(overInfo.getLegByesCount());
        if (null != overInfo.getMatch()) {
            over.setMatch(convertMatchInfoToMatch(overInfo.getMatch()));
        }
        return over;
    }

    /**
     * Used to store the over information into overInfo.
     *
     * @param over - contains the over information to be extracted.
     * @return overInfo - contains the extracted over information.
     */
    public static OverInfo convertOverToOverInfo(Over over) {
        OverInfo overInfo = new OverInfo();
        Set<OverDetailInfo> overDetails = new HashSet<OverDetailInfo>();
        overInfo.setId(over.getId());
        overInfo.setOverNumber(over.getOverNumber());
        overInfo.setRuns(over.getRuns());
        overInfo.setWicketCount(over.getWicketCount());
        overInfo.setFoursCount(over.getFoursCount());
        overInfo.setSixesCount(over.getSixesCount());
        overInfo.setSixesCount(over.getSixesCount());
        overInfo.setNoBallCount(over.getNoBallCount());
        overInfo.setWidesCount(over.getWidesCount());
        overInfo.setByesCount(over.getByesCount());
        overInfo.setLegByesCount(over.getLegByesCount());
        if (null != over.getMatch()) {
            overInfo.setMatch(convertMatchToMatchInfo(over.getMatch()));
        }
        if (null != over.getOverDetails()) {
            for (OverDetail overDetail : over.getOverDetails()) {
                overDetails.add(convertOverDetailToOverDetailInfo(overDetail));
            }
            overInfo.setOverDetails(overDetails);
        }
        return overInfo;
    }
}
