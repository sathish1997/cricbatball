package com.ideas2it.cricbatball.utils;

import java.time.LocalDate;
import java.time.Period;

import com.ideas2it.cricbatball.common.Constants;

/**
 * Used to do calculation of age.
 *
 * @author    M.Sathish Varma created on 12th June 2019.
 */
public class AgeUtil {

    /**
     * calculating age using date of birth provided by the user.
     *
     * @param birthDate - the date of birth for which age to be calculated.
     * return - sends back age in years, months and days format.
     */
    public static String calculateAge(String birthDate) {
        LocalDate localDate = LocalDate.parse(birthDate);
        LocalDate currentDate = LocalDate.now();
        Period age = Period.between(localDate, currentDate);
        return (age.getYears() + Constants.YEARS + age.getMonths() + 
                Constants.MONTHS + age.getDays() + Constants.DAYS);
    }
}
