package com.ideas2it.cricbatball.utils;

import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;

import com.ideas2it.cricbatball.exception.CricBatBallException;

public class EncryptWord {

    /**
     * Used to convert the password into encrypted password using md5.
     * It provides while storing the password in the database.
     *
     * @param password - password to be convert to encrypted format.
     * @return - sends back the password in the encrypted format.
     * @throws CricBatBallException - user defined exception.
     */
    public static String getEncryptedPassword(String password)
            throws CricBatBallException {
        byte[] defaultBytes = password.getBytes();
        try {
            MessageDigest md5MsgDigest = MessageDigest.getInstance("MD5");
            md5MsgDigest.reset();
            md5MsgDigest.update(defaultBytes);
            byte messageDigest[] = md5MsgDigest.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte element : messageDigest) {
                String hex = Integer.toHexString(0xFF & element);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            password = hexString + "";
        } catch (NoSuchAlgorithmException e) {
            throw new CricBatBallException("error while encrypting", e);
        }
        return password;
    }
}
