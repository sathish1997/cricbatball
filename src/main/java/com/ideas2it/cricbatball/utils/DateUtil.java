package com.ideas2it.cricbatball.utils;

import java.text.DateFormat;  
import java.text.ParseException;
import java.text.SimpleDateFormat;  
import java.util.Date;

import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.common.Constants;

/**
 * Used to convert the string to date and vice versa.
 *
 *@author    M.Sathish Varma created on 9thJuly 2019.
 */
public class DateUtil {
    
    /**
     * used to convert the String to date.
     *
     * @param date - date to be converted.
     */
    public static Date convertToDate(String date) throws CricBatBallException {
        Date convertedDate = new Date();
        try {
            convertedDate = new SimpleDateFormat(Constants.DATE_FORMAT)
                               .parse(date);
        } catch(ParseException parseException) {
            throw new CricBatBallException(Constants.ERROR_IN_DATE_CONVERSION,
                    parseException);
        }
        return convertedDate;
    }

    /**
     * Used to convert the Date to Date.
     *
     * @param date - date to be converted to string.
     */
    public static String convertToString(Date date) {
        String convertedDate = new String();
        DateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
        convertedDate =dateFormat.format(date);
        return convertedDate; 
    }
}
