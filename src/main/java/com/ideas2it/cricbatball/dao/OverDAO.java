package com.ideas2it.cricbatball.dao;

import java.util.List;

import com.ideas2it.cricbatball.entities.Over;
import com.ideas2it.cricbatball.exception.CricBatBallException;

/**
 * Used to store the match information into the database.
 *
 * @author    M.Sathish Varma
 */
public interface OverDAO {

    /**
     * Used to insert the over inforamtion into the database.
     *
     * @param over - contains the information to be stored.
     * @return - sends back the id generated to the match.
     * @throws CricBatBallException - user defined exception.
     */
    int insertOver(Over over) throws CricBatBallException;

    /**
     * Used to get the recently Inserted over count.
     *
     * @param matchId - used to get over count of the recently inserted data.
     * @return count - count of over of the last inserted data.
     */
    int getOverCountByMatchId(int matchId) throws CricBatBallException; 

    /**
     * Used to get over information with help of id.
     *
     * @param id - unique id used to fetch over information.
     * @return over - sends back over information.
     */
    Over retrieveOver(int id) throws CricBatBallException;

    /**
     * Used to update the over information with help of overDetail.
     *
     * @param over - containing updated over information.
     */
    void updateOver(Over over) throws CricBatBallException;
}
