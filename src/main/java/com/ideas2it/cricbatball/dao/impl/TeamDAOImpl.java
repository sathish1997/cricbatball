package com.ideas2it.cricbatball.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import org.hibernate.cfg.Configuration;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.apache.log4j.Logger; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.common.Country;
import com.ideas2it.cricbatball.dao.TeamDAO;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.entities.Match;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.entities.Team;

/**
 * Used to manage team database.
 */
@Repository
public class TeamDAOImpl implements TeamDAO {

    private static final Logger logger = Logger.getLogger(TeamDAO.class);
    private SessionFactory sessionFactory;

    @Autowired
    public TeamDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int  insertTeam(Team team) throws CricBatBallException {
        int id;
        Session session = null;
        try {  
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();  
            id = (Integer) session.save(team);
            transaction.commit();
        } catch (HibernateException e) {
            logger.error(Constants.INSERT_TEAM_ERROR_MESSAGE + 
                    team.getId() + e.getMessage());
            throw new CricBatBallException(Constants.INSERT_TEAM_ERROR_MESSAGE,
                    e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return id;
    }

    @Override
    public List<Team> getTeams(int offset, int limit) 
            throws CricBatBallException {
        List<Team> teams = new ArrayList<Team>();
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from Team");
            query.setFirstResult(offset).setMaxResults(limit);
            teams = query.list();
        } catch (HibernateException e) {
            logger.error(Constants.FETCHING_TEAMS_ERROR_MESSAGE
                    + e.getMessage());        
            throw new CricBatBallException(Constants
                    .FETCHING_TEAMS_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return teams;
    }

    @Override
    public Team retrieveTeam(int teamId) throws CricBatBallException {
        Team team = new Team();
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            team = (Team) session.get(Team.class, teamId);
            transaction.commit();
        } catch (HibernateException e) {
            logger.error(Constants.FETCHING_TEAM_ERROR_MESSAGE + teamId
                    + e.getMessage());        
            throw new CricBatBallException(Constants.FETCHING_TEAM_ERROR_MESSAGE
                    + teamId, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return team;
    }

    @Override
    public void updateTeam(Team team) throws CricBatBallException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.update(team);
            transaction.commit();
        } catch (HibernateException | IllegalArgumentException 
                e) {
            logger.error(Constants.UPDATING_TEAM_ERROR_MESSAGE
                    + team.getId() + e.getMessage());                
            throw new CricBatBallException(Constants.UPDATING_TEAM_ERROR_MESSAGE
                    + team.getId(), e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
    }

    @Override
    public void removeTeam(int teamId) throws CricBatBallException {
        Session session = null;
        Team team;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            team = (Team) session.get(Team.class, teamId);
            session.delete(team);
            transaction.commit();
        } catch (HibernateException e) {
            logger.error(Constants.REMOVING_TEAM_ERROR_MESSAGE + teamId
                    + e.getMessage());        
            throw new CricBatBallException(Constants.REMOVING_TEAM_ERROR_MESSAGE
                    + teamId, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
    }

    @Override
    public List<Team> getTeamsByStatus() throws CricBatBallException {
        List<Team> teams = new ArrayList<Team>();
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from Team where status = true");
            teams.addAll(query.list());
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_GETTING_TEAM_BY_STATUS
                    + e.getMessage());        
            throw new CricBatBallException(Constants
                    .ERROR_GETTING_TEAM_BY_STATUS, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return teams;
    }

    @Override
    public Team retrievePlayersById(int teamId) 
            throws CricBatBallException {
        Team team = null;
        Session session = null;
        Set<Player> players = new HashSet<Player>();
        try {
            session = sessionFactory.openSession();
            team = (Team) session.get(Team.class, teamId);
            players.addAll(team.getPlayers());
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_GETTING_PLAYERS_IN_TEAM
                    + teamId, e);        
            throw new CricBatBallException(
                    Constants.ERROR_GETTING_PLAYERS_IN_TEAM + teamId, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        team.setPlayers(players);
        return team;
    }

    @Override
    public int totalTeams() throws CricBatBallException {
        int count = 0;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("select count(id) from Team");
            count = ((Long) query.getSingleResult()).intValue();
        } catch (HibernateException e) {
            logger.error(Constants.TEAM_COUNTING_ERROR_MESSAGE
                    + e.getMessage());
            throw new CricBatBallException(
                    Constants.TEAM_COUNTING_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return count;
    }
}
