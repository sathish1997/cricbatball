package com.ideas2it.cricbatball.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionException;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.dao.UserDAO;
import com.ideas2it.cricbatball.entities.User;
import com.ideas2it.cricbatball.exception.CricBatBallException;

/**
 * It is used to maintain the operations of the user in the database.
 *
 * @author    M.Sathish Varma created on 31st July 2019.
 */
@Repository
public class UserDAOImpl implements UserDAO {

    private static final Logger logger = Logger.getLogger(UserDAO.class);
    private SessionFactory sessionFactory;

    @Autowired
    public UserDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public boolean insertUser(User user) throws CricBatBallException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();  
            session.save(user);
            transaction.commit();
        } catch (HibernateException e) {
            logger.error(Constants.INSERT_USER_ERROR_MESSAGE + e.getMessage());            
            throw new CricBatBallException(Constants.INSERT_USER_ERROR_MESSAGE,
                    e);
        } catch (PersistenceException e) {
            return Boolean.FALSE;
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return Boolean.TRUE;
    }

    @Override
    public User getUserInfo(String emailId) throws CricBatBallException {
        Session session = null;
        User user;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery(
                             "from User where email_id =:emailId");
            query.setParameter("emailId", emailId);
            user = (User) query.getSingleResult();
        } catch (HibernateException e) {
            logger.error(Constants.FETCHING_USER_ERROR_MESSAGE
                    + emailId + e.getMessage());            
            throw new CricBatBallException(
                    Constants.FETCHING_USER_ERROR_MESSAGE, e);
        } catch (NoResultException e) {
            user = null;
            return user;
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return user;
    }

    @Override
    public int isUserExists(String emailId, String password) 
            throws CricBatBallException {
        int count = 0;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("select count(id) from User" +
                             " where email_id =:emailId and password=:password"
                             + " and status = 1");
            query.setParameter("emailId", emailId);
            query.setParameter("password", password);
            count = ((Long) query.getSingleResult()).intValue();
        } catch (HibernateException e) {
            logger.error(Constants.CHECKING_USER_ERROR_MESSAGE
                    + e.getMessage());            
            throw new CricBatBallException(Constants.CHECKING_USER_ERROR_MESSAGE
                    + emailId, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return count;
    }

    @Override
    public void updateUserInfo(User user) throws CricBatBallException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();  
            session.update(user);
            transaction.commit();
        } catch (HibernateException e) {
            logger.error(Constants.UPDATING_USER_ERROR_MESSAGE
                    + user.getId() + e.getMessage());            
            throw new CricBatBallException(
                    Constants.UPDATING_USER_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
    }
} 
