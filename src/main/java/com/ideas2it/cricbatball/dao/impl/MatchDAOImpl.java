package com.ideas2it.cricbatball.dao.impl;

import java.util.List;
import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.apache.log4j.Logger; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.entities.Match;
import com.ideas2it.cricbatball.dao.MatchDAO;

/**
 * Used to store the match information into the database.
 *
 * @author    M.Sathish Varma
 */
@Repository
public class MatchDAOImpl implements MatchDAO {

    private static final Logger logger = Logger.getLogger(MatchDAOImpl.class); 
    private SessionFactory sessionFactory;

    @Autowired
    public MatchDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int insertMatch(Match match) throws CricBatBallException {
        int id = 0;
        Session session = null;
        try {
            session  = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            id = (Integer) session.save(match);
            transaction.commit();
        } catch(HibernateException e) {
            logger.error(Constants.INSERT_MATCH_ERROR_MESSAGE 
                    + e.getMessage());            
            throw new CricBatBallException(Constants.INSERT_MATCH_ERROR_MESSAGE,
                    e);
             
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return id;
    }

    @Override
    public Match retrieveMatch(int matchId) throws CricBatBallException {
        Match match;
        Session session = null;
        try {
            session  = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            match = session.get(Match.class, matchId);
            transaction.commit();
        } catch(HibernateException e) {
            logger.error(Constants.FETCHING_MATCH_ERROR_MESSAGE
                    + e.getMessage());            
            throw new CricBatBallException(Constants
                    .FETCHING_MATCH_ERROR_MESSAGE + matchId, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return match;
    }

    @Override
    public List<Match> retrieveMatches(int offset, int limit)
            throws CricBatBallException {
        List<Match> matches = new ArrayList<Match>();
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from Match");
            query.setFirstResult(offset).setMaxResults(limit);
            matches = query.list();
        } catch(HibernateException e) {
            logger.error(Constants.FETCHING_MATCHES_ERROR_MESSAGE
                    + e.getMessage());            
            throw new CricBatBallException(
                    Constants.FETCHING_MATCHES_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return matches;
    }

    @Override
    public void updateMatch(Match match) throws CricBatBallException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.update(match);
            transaction.commit();
        } catch(HibernateException e) {
            logger.error(Constants.UPDATING_MATCH_ERROR_MESSAGE
                    + match.getId() + e.getMessage());            
            throw new CricBatBallException(
                    Constants.UPDATING_MATCH_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
         
    }

    @Override
    public void deleteMatch(Match match) throws CricBatBallException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.delete(match);
            transaction.commit();
        } catch(HibernateException e) {
            logger.error(Constants.REMOVING_MATCH_ERROR_MESSAGE
                    + match.getId() + e.getMessage());            
            throw new CricBatBallException(
                    Constants.REMOVING_MATCH_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
    }

    @Override
    public int totalMatches() throws CricBatBallException {
        int count = 0;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("select count(id) from Match");
            count = ((Long) query.getSingleResult()).intValue();
        } catch (HibernateException e) {
            logger.error(Constants.MATCH_COUNTING_ERROR_MESSAGE 
                    + e.getMessage());            
            throw new CricBatBallException(
                    Constants.MATCH_COUNTING_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return count;
    }
}
