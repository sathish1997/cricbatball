package com.ideas2it.cricbatball.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;  
import org.hibernate.Transaction; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.common.Country;
import com.ideas2it.cricbatball.dao.PlayerDAO;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.entities.Contact;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.entities.Team;

/**
 * Used to connect to the database and manage 
 * the player information in the database.
 *
 * @author    M.Sathish Varma created on 12th June 2019.
 */
@Repository
public class PlayerDAOImpl implements PlayerDAO {

    private static final Logger logger = Logger.getLogger(PlayerDAO.class);
    private SessionFactory sessionFactory;

    @Autowired
    public PlayerDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Player insertPlayer(Player player) throws CricBatBallException {
        int generatedId = 0;
        Session session = null;
        try {  
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();  
            generatedId = (Integer) session.save(player);
            transaction.commit();
        } catch (HibernateException e) {
            logger.error(Constants.INSERT_PLAYER_ERROR_MESSAGE 
                    + e.getMessage());
            throw new CricBatBallException(
                    Constants.INSERT_PLAYER_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return fetchPlayer(generatedId);
    }

    @Override
    public Player fetchPlayer(int id) throws CricBatBallException {
        Player player = null;
        Player playerToStoreTeam = new Player();
        Session session = null;
        try {
            session = sessionFactory.openSession();
            player = (Player) session.get(Player.class, id);
            playerToStoreTeam.setTeam(player.getTeam());
        } catch (HibernateException e) {
            logger.error(Constants.FETCHING_PLAYER_ERROR_MESSAGE + id
                   + e.getMessage());            
            throw new CricBatBallException(
                   Constants.FETCHING_PLAYER_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        player.setTeam(playerToStoreTeam.getTeam());
        return player;
    }
   
    @Override
    public List<Player> fetchPlayers(int offset, int limit) 
            throws CricBatBallException {
        List<Player> players = new ArrayList<Player>();
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from Player where status = 1");
            query.setFirstResult(offset).setMaxResults(limit);
            players = query.list();
        } catch (HibernateException e) {
            logger.error(Constants.FETCHING_PLAYERS_ERROR_MESSAGE
                    + e.getMessage());
            throw new CricBatBallException(
                    Constants.FETCHING_PLAYERS_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return players;
    }

    @Override
    public void updatePlayerInformation(Player player) 
            throws CricBatBallException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.update(player);
            transaction.commit();
        } catch (HibernateException e) {
            logger.error(Constants.UPDATING_PLAYER_ERROR_MESSAGE 
                    + player.getId() + e.getMessage());
            throw new CricBatBallException(
                    Constants.UPDATING_PLAYER_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
    }

    @Override
    public int totalPlayers() throws CricBatBallException {
        int count = 0;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("select count(id) from Player" + 
                             " where status = 1");
            count = ((Long) query.getSingleResult()).intValue();
        } catch (HibernateException e) {
            logger.error(Constants.PLAYER_COUNTING_ERROR_MESSAGE +
                    e.getMessage());
            throw new CricBatBallException(
                    Constants.PLAYER_COUNTING_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return count;
    }

    @Override
    public List<Player> retrievePlayersByCountry(Country country)
            throws CricBatBallException {
        List<Player> players = new ArrayList<Player>();
        Session session = null;
        try {
            session = sessionFactory.openSession();              
            Query query = session.createQuery(" from Player where country = "
                             + ":country and team_id is null and status = 1" );
            query.setParameter("country", country);
            players = query.list();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_GETTING_PLAYER_BY_COUNTRY
                   + e.getMessage());
            throw new CricBatBallException(
                   Constants.ERROR_GETTING_PLAYER_BY_COUNTRY, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return players;
    }

    @Override
    public List<Player> fetchPlayersById(List<Integer> playerIds) 
            throws CricBatBallException {
        List<Player> players = new ArrayList<Player>();
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from Player p where p.id " 
                             + "in :playerIds" );
            query.setParameter("playerIds",playerIds);
            players = query.list();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_GETTING_PLAYERS_BY_ID 
                    + e.getMessage());
            throw new CricBatBallException(Constants
                    .ERROR_GETTING_PLAYERS_BY_ID , e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return players;
    }
}
