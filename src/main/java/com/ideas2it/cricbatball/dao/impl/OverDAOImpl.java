package com.ideas2it.cricbatball.dao.impl;

import java.util.List;
import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.apache.log4j.Logger; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.entities.Over;
import com.ideas2it.cricbatball.dao.OverDAO;

/**
 * Used to store the match information into the database.
 *
 * @author    M.Sathish Varma
 */
@Repository
public class OverDAOImpl implements OverDAO {

    private static final Logger logger = Logger.getLogger(OverDAOImpl.class); 
    private SessionFactory sessionFactory;

    @Autowired
    public OverDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int insertOver(Over over) throws CricBatBallException {
        int id = 0;
        Session session = null;
        try {
            session  = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            id = (Integer) session.save(over);
            transaction.commit();
        } catch(HibernateException e) {
            logger.error(Constants.INSERT_OVER_ERROR_MESSAGE
                    + e.getMessage());            
            throw new CricBatBallException(Constants.INSERT_OVER_ERROR_MESSAGE,
                    e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return id;
    }

    @Override
    public int getOverCountByMatchId(int matchId) throws CricBatBallException {
        int overNumber = 0;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("select max(overNumber) from " + 
                             "Over where match_id = : matchId ");
            query.setParameter(Constants.MATCHID, matchId);
            if (null != query.getSingleResult()) {
                overNumber = (Integer) query.getSingleResult();
            }
        } catch (HibernateException e) {
            logger.error(Constants.OVER_COUNTING_ERROR_MESSAGE + matchId
                    + e.getMessage());            
            throw new CricBatBallException(
                    Constants.OVER_COUNTING_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return overNumber;
    }

    @Override
    public Over retrieveOver(int id) throws CricBatBallException {
        Over over = new Over(); 
        Session session = null;
        try {
            session = sessionFactory.openSession();
            over = (Over) session.get(Over.class, id);
        } catch (HibernateException e) {
            logger.error(Constants.FETCHING_OVER_ERROR_MESSAGE + id
                   + e.getMessage());            
            throw new CricBatBallException(
                   Constants.FETCHING_OVER_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
        return over;
    }

    @Override
    public void updateOver(Over over) throws CricBatBallException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.update(over);
            transaction.commit();
        } catch (Throwable e) {
            logger.error(Constants.UPDATING_OVER_ERROR_MESSAGE 
                    + over.getId() + e.getMessage());
            throw new CricBatBallException(
                    Constants.UPDATING_OVER_ERROR_MESSAGE, e);
        } finally {
            try {
                session.close();
            } catch (SessionException e) {
                throw new CricBatBallException(Constants.ERROR_IN_SESSION, e);
            }
        }
    }
}
