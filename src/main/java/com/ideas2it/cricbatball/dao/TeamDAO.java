package com.ideas2it.cricbatball.dao;

import java.util.List;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.entities.Match;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.entities.Team;
import com.ideas2it.cricbatball.exception.CricBatBallException;

/**
 * Used to manage team database.
 */
public interface TeamDAO {

    /**
     * used to insert data into the team table.
     *
     * @param team - containing the information of the team.
     * @throws CricBatBallException - user defined exception.
     */
    int  insertTeam(Team team) throws CricBatBallException;

    /**
     * retrieves the team that are available.
     *
     * @param offset - contains id of the match before the starting position.
     * @param limit - number of records to be fetched.
     * @return - sends the list of players who are available.
     * @throws CricBatBallException - user defined exception.
     */
    List<Team> getTeams(int offset, int limit) 
            throws CricBatBallException;

    /**
     * used to retrieve team information with the teamId.
     *
     * @param teamId - the id whose information has to be fetched.
     * @return - sends the particular team having the teamId.
     * @throws CricBatBallException - user defined exception.
     */
    Team retrieveTeam(int teamId) throws CricBatBallException;

    /**
     *Used to update the team Name.
     *  
     * @param team - the team for which name to be updated.
     * @throws CricBatBallException - user defined exception.
     */
    void updateTeam(Team team) throws CricBatBallException;

    /**
     * used to remove the team information from the database.
     *
     * @param teamId - the id to be removed.
     * @throws CricBatBallException - user defined exception.
     */
    void removeTeam(int teamId) throws CricBatBallException;

    /**
     * Used to get the team only whose status is true.
     *
     * @return - sends back the list of the teams whose status ic complete.
     * @throws CricBatBallException - user defined exception.
     */
    List<Team> getTeamsByStatus() throws CricBatBallException;

    /**
     * Used to retrieve player with the help of the teamId
     *
     * @param team - denotes which teams players has to be fetched.
     * @throws CricBatBallException - user defined exception.
     */
    Team retrievePlayersById(int teamId) throws CricBatBallException;

    /**
     * Used to get the total number of records in the database.
     *
     * @return count - sends back the count of the records in the database.
     * @throws CricBatBallException - user defined exception.
     */
    int totalTeams() throws CricBatBallException;

}
