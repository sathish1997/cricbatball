package com.ideas2it.cricbatball.dao;

import java.util.List;

import com.ideas2it.cricbatball.common.Country;
import com.ideas2it.cricbatball.entities.Contact;
import com.ideas2it.cricbatball.entities.Player;
import com.ideas2it.cricbatball.entities.Team;
import com.ideas2it.cricbatball.exception.CricBatBallException;

/**
 * Used to connect to the database and manage 
 * the player information in the database.
 *
 * @author    M.Sathish Varma created on 12th June 2019.
 */
public interface PlayerDAO {

    /**
     * Inserting player information into the database and 
     * storing the information in the table created for player.
     *
     * @param player - contains player information details.
     * @return player - sends back the player information created.
     * @throws CricBatBallException - user defined exception.
     */
    Player insertPlayer(Player player) throws CricBatBallException;;

    /**
     * Getting particular player information using the id generated.
     *
     * @param id - generated id for the player in the database.
     * @throws CricBatBallException - user defined exception.
     */
    Player fetchPlayer(int id) throws CricBatBallException;
   
    /**
     * Retieves cretain number of player information first and then
     * after retieves some more until the last record in the databse.
     *
     * @param offset - contains id of the match before the starting position.
     * @param limit - number of records to be fetched.
     * @return - sends back list of player information from the database.
     * @throws CricBatBallException - user defined exception.
     */
    List<Player> fetchPlayers(int offset, int limit) 
            throws CricBatBallException;

    /**
     * updating the player contact details and inforamtion stored in database.
     * 
     * @param player- contains the inforamtion of player to be updated.
     * @throws CricBatBallException - user defined exception.
     */
    void updatePlayerInformation(Player player) 
            throws CricBatBallException;

    /**
     * Used to get the total number of records in the database.
     *
     * @return count - sends back the count of the records in the database.
     * @throws CricBatBallException - user defined exception.
     */
    int totalPlayers() throws CricBatBallException;

    /**
     * Used to retrieve the players in the particular country.
     *
     * @param country - name of the country to find the players.
     * @return - sends back the list of players.
     * @throws CricBatBallException - user defined exception.
     */
    List<Player> retrievePlayersByCountry(Country country)
            throws CricBatBallException;

    /**
     * Getting the players from the database in the String of id available.
     *
     * @param playerId - id of the player to add the teamId.
     * @param teamId - the id to be added to the player.
     * @throws CricBatBallException - user defined exception.
     */
    List<Player> fetchPlayersById(List<Integer> playerIds) 
            throws CricBatBallException;
}
