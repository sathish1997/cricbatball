package com.ideas2it.cricbatball.dao;

import com.ideas2it.cricbatball.entities.User;
import com.ideas2it.cricbatball.exception.CricBatBallException;

/**
 * It is used to maintain the operations of the user in the database.
 *
 * @author    M.Sathish Varma created on 31st July 2019.
 */
public interface UserDAO {

    /**
     * Inserting user information into the database and 
     * storing the information in the table created for user.
     *
     * @param user - contains user information details.
     * @return - sends back true if email already not exists else 
     *           sends false if not exists.
     * @throws CricBatBallException - user defined exception.
     */
    boolean insertUser(User user) throws CricBatBallException;

    /**
     * Used to get the user information with the help of the emailId.
     *
     * @param emailId - email of the user used to get the information.
     * @return user - sends back the user information containing the emailId. 
     * @throws CricBatBallException - user defined exception.
     */
    User getUserInfo(String emailId) throws CricBatBallException;

    /**
     * Used to check whether the user exists or not.
     *
     * @param emailId - email of the user to be checked.
     * @param password - password of the user to be checked.
     * @return - sends back 1 if id exists else returns '0'.
     * @throws CricBatBallException - user defined exception.
     */
    int isUserExists(String emailId, String password) 
            throws CricBatBallException;

    /**
     * Used to update the stored user information in the database.
     *
     * @param user - contains user information to be updated.
     * @throws CricBatBallException - user defined exception.
     */
    void updateUserInfo(User user) throws CricBatBallException;

}
