package com.ideas2it.cricbatball.dao;

import java.util.List;

import com.ideas2it.cricbatball.entities.Match;
import com.ideas2it.cricbatball.exception.CricBatBallException;

/**
 * Used to store the match information into the database.
 *
 * @author    M.Sathish Varma
 */
public interface MatchDAO {

    /**
     * Used to insert the match into the database.
     *
     * @param match - contains the information to be stored.
     * @return - sends back the id generated to the match.
     * @throws CricBatBallException - user defined exception.
     */
    int insertMatch(Match match) throws CricBatBallException;

    /**
     * Used to retrieve particular with the help of the matchId.
     *
     * @param matchId - unique id of the match to be retrieved.
     * @return - sends back the information of the match with the matchId.
     * @throws CricBatBallException - user defined exception.
     */
    Match retrieveMatch(int matchId) throws CricBatBallException;

    /**
     * Used to retrieve all the matches in the database.
     *
     * @param offset - contains id of the match before the starting position.
     * @param limit - number of records to be fetched.
     * @return - sends back the list of matches to the called function.
     * @throws CricBatBallException - user defined exception.
     */
    List<Match> retrieveMatches(int offset, int limit)
            throws CricBatBallException;

    /**
     * Used to update the match inmforamtion stored in the database.
     *
     * @param match - containing information to be updated.
     * @throws CricBatBallException - user defined exception.
     */
    void updateMatch(Match match) throws CricBatBallException;

    /**
     * Used to delete the Match from the database permanently.
     * 
     * @param matchId - unique id of the match to be deleted.
     * @throws CricBatBallException - user defined exception.
     */
    void deleteMatch(Match match) throws CricBatBallException;

    /**
     * Used to get the total number of records in the database.
     *
     * @return count - sends back the count of the records in the database.
     * @throws CricBatBallException - user defined exception.
     */
    int totalMatches() throws CricBatBallException;

}
