package com.ideas2it.cricbatball.configuration;

import java.io.IOException;
import org.apache.log4j.Logger; 

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * CricBatBallFilter used to do the filtering when user login into our system. 
 * forwards to login page if the user is not valid.
 *
 * @author M.Sathish Varma created on 1st August 2019.
 */
public class CricBatBallFilter implements Filter {

    private static final Logger logger = Logger
                                            .getLogger(CricBatBallFilter.class);
    private RequestDispatcher dispatcher;

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws ServletException, IOException {
        boolean isLoggedIn;
        boolean isLoginPage;
        boolean isLoginRequest;
        boolean isSignupRequest;
        boolean isCreateUserRequest;
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession(false);
        try {
        isLoggedIn = (null != session && (null != session
                        .getAttribute("emailId")));
        String loginURI = httpRequest.getContextPath() + "/login";
        String signupURI = httpRequest.getContextPath() 
                              + "/forwardToCreateUser";
        String createUserURI = httpRequest.getContextPath() + "/createUser";
        isSignupRequest = httpRequest.getRequestURI().equals(signupURI);
        isLoginRequest = httpRequest.getRequestURI().equals(loginURI);
        isCreateUserRequest = httpRequest.getRequestURI().equals(createUserURI);
        if (isLoggedIn || isLoginRequest || isSignupRequest 
                || isCreateUserRequest) {
            chain.doFilter(request, response);
        } else {
            RequestDispatcher dispatcher = httpRequest.getRequestDispatcher(
                                              "index.jsp");
            dispatcher.forward(request, response);
        }
        } catch (ServletException e) {
             e.printStackTrace();
             logger.info(e.getMessage());
        }
    }
}
