package com.ideas2it.cricbatball.configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.PropertyConfigurator;

/**
 * Used to initilize logger properties when the application is started.
 *
 * @author M.Sathish Varma.
 */ 
public class Listener implements ServletContextListener {
    
    public void contextInitialized(ServletContextEvent event) {
        PropertyConfigurator.configure("log4j.properties");         
    }

    public void contextDestroyed(ServletContextEvent event) {}
}
