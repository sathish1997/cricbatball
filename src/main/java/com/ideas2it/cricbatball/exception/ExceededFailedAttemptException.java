package com.ideas2it.cricbatball.exception;

/**
 * This exception is thrown when the failed attempt crosses the limit.
 *
 * @author    M.Sathish Varma.
 */
public class ExceededFailedAttemptException extends RuntimeException {

    public ExceededFailedAttemptException(String message) {
        super(message);
    }
}
