package com.ideas2it.cricbatball.exception;

/**
 * This exception is thrown when the email is not found in the database.
 *
 * @author    M.Sathish Varma.
 */
public class EmailNotFoundException extends RuntimeException {

    public EmailNotFoundException(String message) {
        super(message);
    }
}
