package com.ideas2it.cricbatball.exception;

/**
 * Exception occured in the project are catched here and handled.
 * This custom exception handles the checked exception.
 *
 * @author M.Sathish Varma
 */
public class CricBatBallException extends Exception {
    public CricBatBallException(String message, Throwable cause) {
        super(message, cause);
    }

    public CricBatBallException(String message) {
        super(message);
    }
}
