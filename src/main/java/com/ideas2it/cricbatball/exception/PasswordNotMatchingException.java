package com.ideas2it.cricbatball.exception;

/**
 * This exception is thrown when the password doesnot match with the stored one.
 *
 * @author    M.Sathish Varma.
 */
public class PasswordNotMatchingException extends RuntimeException {

    public PasswordNotMatchingException(String message) {
        super(message);
    }
}
