package com.ideas2it.cricbatball.exception;

/**
 * This exception is thrown when the Matchinfo is not found in the database.
 *
 * @author    M.Sathish Varma.
 */
public class MatchNotFoundException extends RuntimeException {

    public MatchNotFoundException(String message) {
        super(message);
    }
}
