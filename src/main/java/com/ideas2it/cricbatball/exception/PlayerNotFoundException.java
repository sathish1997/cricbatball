package com.ideas2it.cricbatball.exception;

/**
 * This exception is thrown when the player info is not in the database.
 *
 * @author    M.Sathish Varma.
 */
public class PlayerNotFoundException extends RuntimeException {

    public PlayerNotFoundException(String message) {
        super(message);
    }
}
