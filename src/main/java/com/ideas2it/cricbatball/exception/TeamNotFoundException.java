package com.ideas2it.cricbatball.exception;

/**
 * This exception is thrown when the team info is not in the database.
 *
 * @author    M.Sathish Varma.
 */
public class TeamNotFoundException extends RuntimeException {

    public TeamNotFoundException(String message) {
        super(message);
    }
}
