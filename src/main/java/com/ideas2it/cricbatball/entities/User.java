package com.ideas2it.cricbatball.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * This is used to hold the user information to login to the system.
 *
 * @author    M.Sathish Varma created on 30th July 2019.
 */
@Entity
@Table(name = "user")
public class User {
    @Id
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @Column(unique = true, name = "email_id")
    private String emailId;

    @Column(name="mobile_number")
    private long mobileNumber;

    @Column(name="role")
    private String role;

    @Column(name="gender")
    private String gender;

    @Column(name="password")
    private String password;

    @Column(name="failed_attempts")
    private int failedAttempts;

    @Column(name="status")
    private boolean status;

    public User() {}

    public User(String name, String emailId, long mobileNumber, String role,
            String gender, String password, boolean status) {
        this.name = name;
        this.emailId = emailId;
        this.mobileNumber = mobileNumber;
        this.role= role;
        this.gender = gender;
        this.password = password;
        this.status = status;
    }

    /**
     * Setter and Getter methods.
     */
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getEmailId() {
        return this.emailId;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public long getMobileNumber() {
        return this.mobileNumber;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return this.gender;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return this.role;
    }

    public void setFailedAttempts(int failedAttempts) {
        this.failedAttempts = failedAttempts;
    }

    public int getFailedAttempts() {
        return this.failedAttempts;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getStatus() {
        return this.status;
    }
}
