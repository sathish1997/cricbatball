package com.ideas2it.cricbatball.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * contains the information of the overs bowled in the match.
 * Store the runs, extras, boundaries in the match.
 *
 */
@Entity 
@Table(name= "over")
public class Over {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    @Column(name = "over_number") 
    private int overNumber;
    @Column(name = "runs") 
    private int runs;
    @Column(name = "wicket_count") 
    private int wicketCount;
    @Column(name = "fours_count") 
    private int foursCount;
    @Column(name = "sixes_count") 
    private int sixesCount;
    @Column(name = "noball_count") 
    private int noBallCount;
    @Column(name = "wides_count") 
    private int widesCount;
    @Column(name = "byes_count") 
    private int byesCount;
    @Column(name = "legbyes_count") 
    private int legByesCount;
    @ManyToOne
    @JoinColumn(name = "match_id")
    private Match match;
    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "over_id")
    private Set<OverDetail> overDetails;

    /**
     * setter and Getter methods.
     */
    public void setId(int id) {
        this.id = id;
    }
 
    public int getId() {
        return id;
    }

    public void setOverNumber(int overNumber) {
        this.overNumber = overNumber;
    }
 
    public int getOverNumber() {
        return overNumber;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }
 
    public int getRuns() {
        return runs;
    }

    public void setWicketCount(int wicketCount) {
        this.wicketCount = wicketCount;
    }
 
    public int getWicketCount() {
        return wicketCount;
    }

    public void setFoursCount(int foursCount) {
        this.foursCount = foursCount;
    }
 
    public int getFoursCount() {
        return foursCount;
    }

    public void setSixesCount(int sixesCount) {
        this.sixesCount = sixesCount;
    }
 
    public int getSixesCount() {
        return sixesCount;
    }

    public void setNoBallCount(int noBallCount) {
        this.noBallCount = noBallCount;
    }
 
    public int getNoBallCount() {
        return noBallCount;
    }

    public void setWidesCount(int widesCount) {
        this.widesCount = widesCount;
    }
 
    public int getWidesCount() {
        return widesCount;
    }

    public void setByesCount(int byesCount) {
        this.byesCount = byesCount;
    }
 
    public int getByesCount() {
        return byesCount;
    }

    public void setLegByesCount(int legByesCount) {
        this.legByesCount = legByesCount;
    }
 
    public int getLegByesCount() {
        return legByesCount;
    }

    public void setMatch(Match match) {
        this.match = match;
    }
 
    public Match getMatch() {
        return match;
    }

    public void setOverDetails(Set<OverDetail> overDetails) {
        this.overDetails = overDetails;
    }
 
    public Set<OverDetail> getOverDetails() {
        return overDetails;
    }
}
