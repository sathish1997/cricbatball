package com.ideas2it.cricbatball.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ideas2it.cricbatball.common.BallResult;

/**
 * Used to display the details of the each ball bowled in the over.
 *
 * @author M.Sathish Varma created on 22nd August 2019 
 */
@Entity 
@Table(name= "over_detail")
public class OverDetail {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", updatable=false, nullable=false)
    private int id;

    @Column(name = "runs")
    private int runs;

    @Enumerated(EnumType.STRING)
    @Column(name = "ball_result")
    private BallResult ballResult;

    @Column(name = "ball_number")
    private int ballNumber;

    @ManyToOne
    @JoinColumn(name = "batsman_id")
    private Player batsman;

    @ManyToOne
    @JoinColumn(name = "bowler_id")
    private Player bowler;

    @ManyToOne
    @JoinColumn(name = "match_id")
    private Match match;

    @ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "over_id")
    private Over over;

    /**
     * Setter and Getter methods.
     */
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getRuns() {
        return runs;
    }

    public void setBallResult(BallResult ballResult) {
        this.ballResult = ballResult;
    }

    public BallResult getBallResult() {
        return ballResult;
    }

    public void setBallNumber(int ballNumber) {
        this.ballNumber = ballNumber;
    }

    public int getBallNumber() {
        return ballNumber;
    }

    public void setBatsman(Player batsman) {
        this.batsman = batsman;
    }

    public Player getBatsman() {
        return batsman;
    }

    public void setBowler(Player bowler) {
        this.bowler = bowler;
    }

    public Player getBowler() {
        return bowler;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Match getMatch() {
        return match;
    }

    public void setOver(Over over) {
        this.over = over;
    }

    public Over getOver() {
        return over;
    }
}
