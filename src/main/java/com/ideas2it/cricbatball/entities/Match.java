package com.ideas2it.cricbatball.entities;

import java.util.Set;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.ideas2it.cricbatball.common.MatchType;
import com.ideas2it.cricbatball.entities.Team;

/**
 * Used to store the information of a match.
 * Inserts match information and access the match information.
 *
 * @author    M.Sathish Varma created on 8th June 2019.
 */
@Entity
@Table(name= "matches")
public class Match {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "location")
    private String location;
    @Column(name = "starting_time")
    public String startingTime;
    @Column(name = "date_of_play")
    private Date dateOfPlay;
    @Enumerated(EnumType.STRING)
    @Column(name = "match_type")
    private MatchType matchType;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "team_match", joinColumns = { 
    @JoinColumn(name = "match_id") }, 
    inverseJoinColumns = {@JoinColumn(name = "team_id") })
    private Set<Team> teams;
    
    public Match() {}

    public Match(String name, String location, Date dateOfPlay,
            String startingTime, MatchType matchType) {
        this.name = name;
        this.location = location;
        this.startingTime = startingTime;
        this.dateOfPlay = dateOfPlay;
        this.matchType = matchType;
    }

    /**
     * getter and setter methods for match.
     */
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return this.location;
    }

    public void setStartingTime(String startingTime) {
        this.startingTime = startingTime;
    }

    public String getStartingTime() {
        return this.startingTime;
    }

    public void setDateOfPlay(Date dateOfPlay) {
        this.dateOfPlay = dateOfPlay;
    }

    public Date getDateOfPlay() {
        return this.dateOfPlay;
    }

    public void setMatchType(MatchType matchType) {
        this.matchType = matchType;
    }

    public MatchType getMatchType() {
        return this.matchType;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    public Set<Team> getTeams() {
        return this.teams;
    }
}
