package com.ideas2it.cricbatball.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * contains information of the player's address and contact details.
 * 
 * @author - M.Sathish Varma created on 19th May 2019
 */
@Entity  
@Table(name="contact")  
public class Contact {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="pincode")
    private int pincode;
    @Column(name="mobile_number")
    private long mobileNumber;
    @Column(name="address")
    private String address;
    @OneToOne
    @JoinColumn(name="player_id") 
    private Player player;

    public Contact() {}

    public Contact(String address, long mobileNumber, int pincode) {
        this.address = address;
        this.mobileNumber = mobileNumber;
        this.pincode = pincode;
    }
 
   /**
     * Setter and getter methods for contact details of  player.
     */ 

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public int getPincode() {
        return this.pincode;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public long getMobileNumber() {
        return this.mobileNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return this.address;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return this.player;
    }

    public boolean equals(Object object) {
        if (object == this) {
            return Boolean.TRUE;
        }
        if (object == null && object.getClass() != this.getClass()) {
            return false;
        }
        Contact contact = new Contact();
        if (contact.getId() == this.getId()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public int hashCode() {
        return this.id;
    }
}
