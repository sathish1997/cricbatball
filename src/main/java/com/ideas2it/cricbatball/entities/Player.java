package com.ideas2it.cricbatball.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ideas2it.cricbatball.common.Country;
import com.ideas2it.cricbatball.utils.AgeUtil;

/**
 * Used to store the information of a player .
 * Inserts player information and access the player information.
 *
 * @author    M.Sathish Varma created on 8th June 2019.
 */
@Entity 
@Table(name= "player")
public class Player {

    @Column(name="status")
    private boolean status;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="batting_style")
    private String battingStyle;

    @Column(name="bowling_style")
    private String bowlingStyle;

    @Enumerated(EnumType.STRING)
    @Column(name="country")
    private Country country;

    @Column(name="date_of_birth")
    private String dateOfBirth;

    @Column(name="name")
    private String name;

    @Column(name="role")
    private String role;

    @Column(name="image_path")
    private String imagePath;

    @OneToOne(cascade=CascadeType.ALL, mappedBy="player")
    private Contact contact;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "team_id")
    private Team team;

    public Player() {}

    public Player(String name, String role, String battingStyle, 
            String bowlingStyle, Country country, String dateOfBirth,
            boolean status, Contact contact) {
        this.name = name;
        this.role = role;
        this.battingStyle = battingStyle;
        this.bowlingStyle = bowlingStyle;
        this.country = country;
        this.dateOfBirth = dateOfBirth;
        this.status = status;
        this.contact = contact;
    }

    /**
     * Getter and Setter methods.
     */

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getStatus() {
        return this.status; 
    }

    public void setId(int id) {
        this.id = id;
    }
   
    public int getId() {
        return this.id;
    }

    public String getAge() {
        return AgeUtil.calculateAge(this.dateOfBirth);
    }

    public void setBattingStyle(String battingStyle) {
        this.battingStyle = battingStyle;
    }

    public String getBattingStyle() {
        return this.battingStyle;
    }
 
    public void setBowlingStyle(String bowlingStyle) {
        this.bowlingStyle = bowlingStyle;
    }

    public String getBowlingStyle() {
        return this.bowlingStyle;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDateOfBirth() {
        return this.dateOfBirth;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return this.role;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Contact getContact() {
        return this.contact;
    }

    public void setTeam(Team team) {
        this.team =team;
    }

    public Team getTeam() {
        return this.team;
    }

    public boolean equals(Object object) {
        if (object == this) {
            return Boolean.TRUE;
        }
        if (object == null && object.getClass() != this.getClass()) {
            return false;
        }
        Player player = new Player();
        if (player.getId() == this.getId() && player.getName() == this.getName()
                && player.getDateOfBirth() == this.getDateOfBirth()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public int hashCode() {
        return this.id;
    }

    public String toString() {
        Contact contact = this.getContact();
        return new StringBuilder().append(this.id).append("\t")
                .append(this.name).append("\t").append(this.role).append("\t")
                .append(this.battingStyle).append("\t")
                .append(this.bowlingStyle).append("\t").append(this.country)
                .append("\t").append(this.dateOfBirth).append("\t")
                .append("\t").append(this.status).append("\t")
                .append(contact.getAddress()).append("\t")
                .append(contact.getMobileNumber()).append("\t") 
                .append(contact.getPincode()).toString();
   }
}

