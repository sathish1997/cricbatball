package com.ideas2it.cricbatball.entities;

import java.util.Set;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ideas2it.cricbatball.common.Country;

/**
 * Used to store, inserts and access the team information.
 *
 * @author M.Sathish Varma created on 21st June 2019.
 */
@Entity 
@Table(name= "team")
public class Team {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name="country")
    private Country country;

    @Column(name="status")
    private boolean status;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "team_id")
    Set<Player> players = new HashSet<Player>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "team_match", joinColumns = { 
    @JoinColumn(name = "team_id") }, 
    inverseJoinColumns = { @JoinColumn(name = "match_id") })
    Set<Match> matches = new HashSet<Match>();

    public Team() {}
    
    public Team(String name, Country country, boolean status) {
        this.name = name;
        this.country = country;
        this.status = status;
    }

    /**
     * Setter and getter methods.
     */
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getStatus() {
        return this.status;
    }

   public void setPlayers(Set<Player> players) {
       this.players = players;
   }

   public Set<Player> getPlayers() {
       return this.players;
   }

   public void setMatches(Set<Match> matches) {
       this.matches = matches;
   }

   public Set<Match> getMatches() {
       return this.matches;
   }

   public boolean equals(Object object) {
        if (object == this) {
            return Boolean.TRUE;
        }
        if (object == null && object.getClass() != this.getClass()) {
            return false;
        }
        Team team = new Team();
        if (team.getId() == this.getId() && team.getName() == 
                this.getName()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public int hashCode() {
        return this.id;
    }

   public String toString() {
       return new StringBuilder().append(this.getId()).append("\t")
                .append(this.getName()).append("\t")
                .append(this.getCountry().toString()).append("\t")
                .append(this.getStatus()).toString();
   }
}
