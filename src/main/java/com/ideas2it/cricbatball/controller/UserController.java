package com.ideas2it.cricbatball.controller;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.ResponseBody; 
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.exception.EmailNotFoundException;
import com.ideas2it.cricbatball.exception.ExceededFailedAttemptException;
import com.ideas2it.cricbatball.exception.PasswordNotMatchingException;
import com.ideas2it.cricbatball.info.UserInfo;
import com.ideas2it.cricbatball.service.UserService;

/**
 * Used to create the user and check whether 
 * the user information is valid or not.
 *
 * @author    M.Sathish Varma created on 30th July 2019.
 */
@Controller
public class UserController {

    private UserService userService;
    private static final Logger logger = Logger.getLogger(UserController.class);
    
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Used to forward to the create page of user. 
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.FORWARDTOCREATEUSER, 
            method = RequestMethod.GET)
    public String forwardToCreateUser(Model model) {
        model.addAttribute(Constants.USER_INFO, new UserInfo());
        return Constants.CREATE_USER;
    }

    /**
     * Used to create the user and store the information in the database.
     * 
     * @param userInfo - contains the user information to be updated.
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.CREATE_USER, method = RequestMethod.POST)    
    public String createUser(@ModelAttribute UserInfo userInfo, Model model) {
        boolean isDuplicate;
        try {
            isDuplicate = userService.createUser(userInfo);
            if (Boolean.FALSE == isDuplicate) {
                model.addAttribute(Constants.DUPLICATE,
                        Constants.EMAIL_EXISTS);
                return Constants.CREATE_USER;
            } else {
                return Constants.INDEX;
            } 
        } catch (CricBatBallException e) {
            model.addAttribute(e.getMessage());
            return Constants.ERROR;
        }
    }

    /**
     * Used to validate the user whether the email and password correct or not.
     * 
     * @param emailId - used to check the the validation of user.
     * @param password - used to check if the user has entered 
     * the valid password if exists.
     * @param request -  request sent from the client through web browser. 
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.LOGIN, method = RequestMethod.POST)
    public String checkUser(@RequestParam(Constants.EMAILID) String emailId,
            @RequestParam(Constants.PASSWORD) String password,
            HttpServletRequest request, Model model) {
        boolean isUserExist;
        int attempts = 0;
        HttpSession session;
        try {
            isUserExist = userService.checkUser(emailId, password);
            if (isUserExist) {
                session = request.getSession();
                session.setAttribute(Constants.EMAILID, emailId);
                return Constants.MANAGE;
            }
        } catch (CricBatBallException e) {
            model.addAttribute(e.getMessage());
            return Constants.ERROR;
        } catch (EmailNotFoundException | PasswordNotMatchingException |
                ExceededFailedAttemptException e) {
            model.addAttribute(Constants.WRONG_DATA,e.getMessage());
            return Constants.INDEX;
        }
        return Constants.MANAGE;
    }
    

    /**
     * Used to invalidate the user session the user if he wants to logout.
     *
     * @param request- contains the user session to be invalidated.
     * @return - contains the page name to be forwarded.
     */ 
    @RequestMapping(value = Constants.LOGOUT, method = RequestMethod.POST)
    public String logoutUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (null != session) {
            session.invalidate();
        }
        return Constants.INDEX;
    }
    
}
