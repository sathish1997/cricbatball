package com.ideas2it.cricbatball.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.multipart.commons.CommonsMultipartFile;  

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.common.Country;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.exception.PlayerNotFoundException;
import com.ideas2it.cricbatball.info.PlayerInfo;
import com.ideas2it.cricbatball.info.PlayerPaginationInfo;
import com.ideas2it.cricbatball.service.PlayerService;
import com.ideas2it.cricbatball.utils.AgeUtil;
import com.ideas2it.cricbatball.utils.DateUtil;


/**
 * Used to create, update, read and delete the player information.
 */
@Controller
public class PlayerController {

    private PlayerService playerService; 

    private static final Logger logger = Logger
                                            .getLogger(PlayerController.class);

    @Autowired 
    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    /**
     * Used to forward to the create page to get the player information.
     *
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.FORWARDTOCREATE, method = RequestMethod
            .GET)
    private String forwardToCreate(Model model) {
        PlayerInfo playerInfo = new PlayerInfo();
        model.addAttribute(Constants.PLAYER_INFO, playerInfo);
        return Constants.CREATE_PLAYER;
    }

    /**
     * Used to store the player information read from the browser.
     *
     * @param model - used to add attribute to the model to forward to page.
     * @param file - contains the profile picture of the player.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.CREATE_PLAYER, method = RequestMethod
            .POST)
    public String createPlayer(@ModelAttribute PlayerInfo playerInfo,
            Model model, @RequestParam(Constants.PLAYER_IMAGE) 
            CommonsMultipartFile  file) {
        try {
            playerInfo = playerService.storePlayerInformation(playerInfo, file);
            model.addAttribute(Constants.PLAYER_INFO, playerInfo);
            model.addAttribute(Constants.SUCCESS_MESSAGE, Constants.MESSAGE);
        } catch (CricBatBallException | IOException e) {
            model.addAttribute(e.getMessage());
            return Constants.ERROR;
        }       
        return Constants.PLAYER_INFORMATION;
    }

    /**
     * Used to get the players for the display of the players in first page.
     * fetches only limited number of players using limit specified.
     *
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.DISPLAY_PLAYERS, method = RequestMethod
            .GET)
    private String displayPlayers(Model model) {
        int page = 1;
        try {
            PlayerPaginationInfo playerPaginationInfo = playerService
                    .getPaginationInfo(page);
            model.addAttribute(Constants.PLAYER_PAGINATION_INFO, 
                    playerPaginationInfo);
        } catch (CricBatBallException e) {
            model.addAttribute(e.getMessage());
            return Constants.ERROR;
        }
        return Constants.DISPLAY_PLAYERS;
    }
        
   /**
     * Used to get the all players information.
     *
     * @param page - used to get the players to be displayed on that page.
     * @param request -  request sent from the client through web browser.
     * @param response - response made for the request.
     * @param model - used to add attribute to the model to forward to page.
     * @throws IOException - if an input or output error is detected 
     *                       when the servlet handles
     */
    @RequestMapping(value = Constants.DISPLAY_PLAYERS_INFO, method 
            = RequestMethod.GET)
    public void displayPlayersInfo(@RequestParam(Constants.PAGE) int page, 
            HttpServletRequest request, HttpServletResponse response,
            Model model) {
        PlayerPaginationInfo playerPaginationInfo;
        try {
            playerPaginationInfo = playerService.getPaginationInfo(page);
            response.setContentType(Constants.APPLICATION_JSON);
            response.getWriter().write(playerPaginationInfo.getPlayers()
                    .toString());
        } catch (CricBatBallException | IOException e) {
            logger.info(e.getMessage());
        }
    }

    /**
     * Used to get player information to forward it to the update player page.
     * The player information is attached with model attribute and
     * forwarded to the update page.
     *
     * @param id - id of the player whose information has to be retrieved.
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.GET_PLAYER, method = RequestMethod.POST)
    public String getPlayer(@RequestParam(Constants.EDIT_ID) int id, 
            Model model) {
        try {
            PlayerInfo playerInfo = playerService.viewPlayer(id);
            model.addAttribute(Constants.PLAYER_INFO, playerInfo);
        } catch (CricBatBallException e) {
            model.addAttribute(e.getMessage());
            return Constants.ERROR;
        } 
        return Constants.UPDATE_PLAYER;
    } 

    /**
     * Used to get player information to forward it to the update player page.
     * The player information is attached with model attribute and
     * forwarded to the update page.
     *
     * @param id - id of the player whose information has to be retrieved.
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.FETCH_PLAYER, method = RequestMethod.GET)
    public String fetchPlayer(@RequestParam(Constants.ID) int id, Model model) {
        try {
            PlayerInfo playerInfo = playerService.viewPlayer(id);
            model.addAttribute(Constants.PLAYER_INFO, playerInfo);           
        } catch (CricBatBallException e) {
            model.addAttribute(e.getMessage());
            return Constants.ERROR;
        } 
        return Constants.PLAYER_INFORMATION;
    }

    /**
     * Used to get the update player information with the help of updated data
     * sent through request by the user.
     *
     * @param file - contains the profile picture of the player to be updated.
     * @param playerInfo - contains the updated player information.
     * @param model - used to add attribute to the model to forward to page.
     * @throws IOException - if an input or output error is detected 
     *                       when the servlet handles
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.UPDATEPLAYER, method = RequestMethod.POST)
    public String updatePlayer(@RequestParam(required = false, name = 
            Constants.PLAYER_IMAGE) CommonsMultipartFile  file, 
            @ModelAttribute(Constants.PLAYER_INFO) PlayerInfo playerInfo, 
            Model model) 
            throws IOException {
        try {
            playerInfo = playerService.updatePlayer(playerInfo, file);
            model.addAttribute(Constants.PLAYER_INFO, playerInfo);
            model.addAttribute(Constants.UPDATE_MESSAGE, Constants.UPDATE_INFO);           
        } catch (CricBatBallException e) {
            model.addAttribute(e.getMessage());
            return Constants.ERROR;
        }      
        return Constants.PLAYER_INFORMATION;        
    } 

    /**
     * Used to remove particular player information from database permanently 
     * with the help of data sent through request.
     *
     * @param id - id of the player whose information to be deleted permanently.
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.DELETE_PLAYER, 
            method = RequestMethod.POST)
    public String deletePlayer(@RequestParam(Constants.DELETE_ID) int id, 
            Model model) {
        try {
            playerService.deletePlayer(id);
        } catch (CricBatBallException e) {
            model.addAttribute(e.getMessage());
            return Constants.ERROR;
        }
        return Constants.REDIRECT_DISPLAY_PLAYERS;
    }
}
