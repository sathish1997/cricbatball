package com.ideas2it.cricbatball.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.ResponseBody;  

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.common.MatchType;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.exception.MatchNotFoundException;
import com.ideas2it.cricbatball.info.MatchInfo;
import com.ideas2it.cricbatball.info.MatchPaginationInfo;
import com.ideas2it.cricbatball.service.MatchService;
import com.ideas2it.cricbatball.utils.DateUtil;

/**
 * Used to manage the match information and add teams to the match.
 *
 * @author    M.Sathish Varma
 */
@Controller
public class MatchController {


    private MatchService matchService; 
    private static final Logger logger = Logger
                                            .getLogger(MatchController.class);

    @Autowired 
    public MatchController(MatchService matchService) {
        this.matchService = matchService;
    }    

    /**
     * Used to forward the page to the create match page.
     *
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.FORWARD_TO_CREATE_MATCH, 
            method = RequestMethod.GET)
    public String divertToCreateMatch(Model model) {
        MatchInfo matchInfo = new MatchInfo();
        model.addAttribute(Constants.MATCH_INFO, matchInfo);
        return Constants.CREATEMATCH;
    }

    /**
     * Used to store the match information read from the browser.
     *
     * @param date - date of the match to be played.
     * @param matchInfo - contains the match information to be saved.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.CREATEMATCH, method = RequestMethod.POST)
    public String saveMatch(@RequestParam(Constants.MATCH_DATE) String date, 
            @ModelAttribute(Constants.MATCH_INFO) MatchInfo matchInfo, 
            Model model) {
        try {
            MatchPaginationInfo matchPaginationInfo = matchService
                    .insertMatch(date, matchInfo);
            date = DateUtil.convertToString(matchInfo.getDateOfPlay());
            model.addAttribute(Constants.DATE, date);
            model.addAttribute(Constants.MATCH_PAGINATION_INFO, 
                    matchPaginationInfo);
        } catch (CricBatBallException e) {
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.ADDTEAMS;
    }

    /**
     * Used to add two teams to the match created.
     *
     * @param id -  match to which teams to be added.
     * @param teamsForMatch - contains the ids of the team to be added.
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.ADDTEAMS, method = RequestMethod.POST)
    public String addTeams(@RequestParam(Constants.MATCHID) int id,
            @RequestParam(required = false, name = Constants.TEAMSFORMATCH) 
            String[] teamsForMatch,Model model) {
        try {
            matchService.addTeamsToMatch(id, teamsForMatch);
        } catch(CricBatBallException e){
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.REDIRECT_GETMATCHES;
    }

    /**
     * Used to get the particular match information 
     * with the help of data sent through request.
     *
     * @param id - id of the match to be retrieved.
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.GETMATCH, method = RequestMethod.GET)
    public String getMatch(@RequestParam(Constants.MATCHID) int id, 
            Model model) {
        MatchInfo matchInfo;
        String date;
        try {
            matchInfo = matchService.retrieveMatchToView(id);
            date = DateUtil.convertToString(matchInfo.getDateOfPlay());
            model.addAttribute(Constants.DATE, date);           
            model.addAttribute(Constants.MATCH_INFO, matchInfo);
        } catch (CricBatBallException e) {
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.VIEWMATCH;
    }

    /**
     * Used to get the all match information from the database.
     *
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.GETMATCHES, method = RequestMethod.GET)
    public String getMatchesInfo(Model model) {
        int page = 1;
        MatchInfo matchInfo;
        try {
            MatchPaginationInfo matchPaginationInfo = matchService
                    .getPaginationInfo(page);
            model.addAttribute(Constants.MATCH_PAGINATION_INFO, 
                    matchPaginationInfo);
        } catch (CricBatBallException e) {
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.VIEWMATCHES;
        
    }

    /**
     * Used to get the all match information from the database.
     *
     * @param - page used to get the list of match from the database. 
     * @param model - used to add attribute to the model to forward to page.
     * @param request -  request sent from the client through web browser.
     * @param response - response made for the request.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.GETMATCHESINFO, 
            method = RequestMethod.GET)    
    public void getMatches(@RequestParam(Constants.PAGE) int page, Model 
            model, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        MatchInfo matchInfo;
        try {
            MatchPaginationInfo matchPaginationInfo = matchService
                           .getPaginationInfo(Integer
                           .parseInt(request.getParameter(Constants.PAGE)));
            response.setContentType(Constants.APPLICATION_JSON);
            response.getWriter().write(matchPaginationInfo.getMatches()
                    .toString());
        } catch (CricBatBallException e) {
            logger.info(e.getMessage());
        }
    }

    /**
     * Used to remove the particular match information 
     * with the help of data sent through request.
     *
     * @param id - id of the match to be deleted. 
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.DELETEMATCH, method = RequestMethod.POST)
    public String deleteMatch(@RequestParam(Constants.MATCHID) int id,
            Model model) {
        try {
            matchService.deleteMatch(id);
        } catch(CricBatBallException e){
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.REDIRECT_GETMATCHES;
    }

    /**
     * Used to forward to the update page.
     *
     * @param id - used to display the match information on the update page. 
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.FORWARD_TO_UPDATE_MATCH,
             method = RequestMethod.POST)
    public String forwardToUpdatePage(@RequestParam(Constants.MATCHID) int id, 
            Model model) {
        String date;
        MatchInfo matchInfo;
        try {
            MatchPaginationInfo matchPaginationInfo = matchService
                    .getTeamsToUpdateMatch(id);
            date = DateUtil.convertToString(matchPaginationInfo.matchInfo
                      .getDateOfPlay());
            model.addAttribute(Constants.DATE, date);
            model.addAttribute(Constants.MATCH_PAGINATION_INFO, 
                    matchPaginationInfo);
            model.addAttribute(Constants.MATCH_INFO, matchPaginationInfo
                    .getMatchInfo());
        } catch (CricBatBallException e) {
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.UPDATEMATCH;
    }

    /**
     * Used to update the particular match information 
     * with the help of data sent through request.
     *
     * @param date - date of the match to be updated.
     * @param id - od of the match to be updated.
     * @param teamsId - teamId to be added to the match.
     * @param matchInfo - contains the updated match information. 
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.UPDATEMATCH, method = RequestMethod.POST)
    public String updateMatch(@RequestParam(Constants.MATCH_DATE) String date, 
            @RequestParam(Constants.MATCHID) int id, 
            @RequestParam(required = false, name = Constants.ADDTEAMS) 
            String[] teamsId, @ModelAttribute(Constants.MATCH_INFO) 
            MatchInfo matchInfo, Model model) {
        try {
            matchService.updateMatchInformation(date, id, teamsId, matchInfo);
        } catch(CricBatBallException e){
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.REDIRECT_GETMATCHES;
    }

    /**
     * Forwards to play page to start the match between two teams.
     * It also forwards teams and players in team information to the page.
     *
     * @param id - id to which the match has to be started. 
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.START_PLAY, method = RequestMethod.POST)
    public String forwardToStartMatch(@RequestParam(Constants.MATCHID) int id,
            Model model) {
        try {
            MatchPaginationInfo matchPaginationInfo = matchService
                    .fetchTeamsToStartMatch(id);
            model.addAttribute(Constants.MATCH_PAGINATION_INFO, 
                    matchPaginationInfo);
        } catch(CricBatBallException e){
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.DISPLAY_TEAM_DETAILS;
    }
}
