package com.ideas2it.cricbatball.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.ResponseBody;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.info.OverInfo;
import com.ideas2it.cricbatball.info.OverDetailInfo;
import com.ideas2it.cricbatball.service.OverDetailService;

/**
 * Contains the overall information of the over bowled.
 *
 * @author    M.Sathish Varma
 */
@Controller 
public class OverDetailController {
    
    public OverDetailService overDetailService;
    public static final Logger logger = Logger
            .getLogger(OverDetailController.class);

    @Autowired
    public OverDetailController (OverDetailService overDetailService) {
        this.overDetailService = overDetailService;
    }

    /**
     * Used to get overDetailInfo for the page as the response.
     *
     * @param batsmanId - contains the batsman who is in strike.
     * @param bowlerId - contains bowler id who is going to bowl the ball.
     * @param matchId - contains the id of match where the over is bowled. 
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.GET_BALL_RESULT, 
            method = RequestMethod.GET)
    public void startMatch(@RequestParam(Constants.OVER_ID) int overId, 
            @RequestParam(Constants.BATSMAN_ID) int batsmanId, 
            @RequestParam(Constants.BOWLER_ID) int bowlerId, 
            @RequestParam(Constants.MATCHID) int matchId,
            @RequestParam(Constants.STRIKER_ID) int strikerId, 
            @RequestParam(Constants.SECOND_BATSMAN_ID) int secondBatsmanId, 
            HttpServletRequest request, HttpServletResponse response, 
            Model model) {
        try {
            JSONObject ballInformation = overDetailService
                                           .getOverDetailInfo(batsmanId, 
                                           bowlerId, matchId, overId, 
                                           strikerId, secondBatsmanId);
            response.setContentType(Constants.APPLICATION_JSON);
            response.getWriter().write(ballInformation.toString());
        } catch(CricBatBallException | IOException e){
            logger.info(e.getMessage());
        }
    }
}
