package com.ideas2it.cricbatball.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.ResponseBody;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.info.OverInfo;
import com.ideas2it.cricbatball.info.OverDetailInfo;
import com.ideas2it.cricbatball.info.TeamInfo;
import com.ideas2it.cricbatball.service.OverService;

/**
 * Contains the overall information of the over bowled.
 *
 * @author    M.Sathish Varma
 */
@Controller 
public class OverController {
    
    public OverService overService;
    public static final Logger logger = Logger.getLogger(OverController.class);

    @Autowired
    public OverController (OverService overService) {
        this.overService = overService;
    }

    /**
     * Used to forward selected batsmans and bowler to start page of the match.
     *
     * @param batsmanIds[] - contains the selected batsman id from the team
     *                       players available.
     * @param bowlerId[] - contains the selected bowler id from the team 
                         - players available.
     * @param matchId - contains the id of match where the over is bowled. 
     * @param model - used to add attribute to the model to forward to page.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.START_MATCH, method = RequestMethod.POST)
    private String startMatch(@RequestParam(Constants.BATSMAN_IDS) 
            String[] batsmanIds, @RequestParam(Constants.BOWLER_IDS) 
            String[] bowlerId, @RequestParam(Constants.MATCHID) int matchId,
            @RequestParam(Constants.FIRST_TEAM_ID) int firstTeamId, 
            @RequestParam(Constants.SECOND_TEAM_ID) int secondTeamId, 
            Model model) {
        try {
            OverDetailInfo overDetailInfo = overService
                                               .getPlayersToStart(batsmanIds, 
                                               bowlerId, matchId, firstTeamId,
                                               secondTeamId);
            model.addAttribute(Constants.OVER_DETAIL_INFO, overDetailInfo);
        } catch(CricBatBallException e){
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.SCORECARD_PAGE;
    }

    /**
     * Used to forward page to choose bowler when the over gets completed.
     *
     * @param overDetailInfo - contains information of over bowled before.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.CHOOSE_BOWLER, 
            method = RequestMethod.POST)
    private String chooseBowler(@ModelAttribute(Constants.OVER_DETAIL_INFO) 
            OverDetailInfo overDetailInfo, Model model) {
        TeamInfo teamInfo;
        try {
            teamInfo = overService.retrieveNextBowler(overDetailInfo
                           .getSecondTeam().getId(), overDetailInfo
                           .getBowler().getId());
            model.addAttribute(Constants.OVER_DETAIL_INFO, overDetailInfo);
            model.addAttribute(Constants.TEAM_INFO, teamInfo);
        } catch(CricBatBallException e){
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.SELECT_BOWLER;
    }

    /**
     * Used to forward page to choose bowler when the over gets completed.
     *
     * @param overDetailInfo - contains information of over bowled before.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.CHOOSE_BATSMAN, 
            method = RequestMethod.POST)
    private String chooseBatsman(@RequestParam(Constants.WICKET_ID) 
            int wicketId, @ModelAttribute(Constants.OVER_DETAIL_INFO) 
            OverDetailInfo overDetailInfo, Model model) {
        TeamInfo teamInfo;
        try {
            teamInfo = overService.retrieveNextBatsman(wicketId, 
                          overDetailInfo);
            model.addAttribute(Constants.OVER_DETAIL_INFO, overDetailInfo);
            model.addAttribute(Constants.TEAM_INFO, teamInfo);
        } catch(CricBatBallException e){
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.SELECT_BATSMAN;
    }

    /**
     * Used to continue the match after the new bowler is selected.
     * Enables you to resume the match.
     *
     * @param overDetailInfo - contains information of the resumed match.
     * @param bowlerId - contains select bowler to bowl next over.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.CONTINUE_PLAY, 
            method = RequestMethod.POST)
    public String resumeMatch(@ModelAttribute(Constants.OVER_DETAIL_INFO) 
            OverDetailInfo overDetailInfo, @RequestParam(Constants.BOWLER_IDS) 
            int bowlerId, Model model) {
        try {
            overService.getOverDetailInformation(overDetailInfo, bowlerId);
            model.addAttribute(Constants.OVER_DETAIL_INFO, overDetailInfo);
        } catch(CricBatBallException e){
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.SCORECARD_PAGE;
    }

    /**
     * Used to continue the match after the new bowler is selected.
     * Enables you to resume the match.
     *
     * @param overDetailInfo - contains information of the resumed match.
     * @param batsmanId - contains selected batsman to bat.
     * @return - contains the page name to be forwarded.
     */
    @RequestMapping(value = Constants.CONTINUE_PLAY_With_BATSMAN, 
            method = RequestMethod.POST)
    public String continueMatch(@ModelAttribute(Constants.OVER_DETAIL_INFO) 
            OverDetailInfo overDetailInfo, @RequestParam(Constants.BATSMAN_IDS) 
            int batsmanId, Model model) {
        try {
            overService.getOverDetailInfo(overDetailInfo, batsmanId);
            model.addAttribute(Constants.OVER_DETAIL_INFO, overDetailInfo);
        } catch(CricBatBallException e){
            model.addAttribute(Constants.ERROR, e.getMessage());
            return Constants.ERROR;
        }
        return Constants.SCORECARD_PAGE;
    }
}
