package com.ideas2it.cricbatball.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam;

import com.ideas2it.cricbatball.common.Constants;
import com.ideas2it.cricbatball.common.Country;
import com.ideas2it.cricbatball.exception.CricBatBallException;
import com.ideas2it.cricbatball.exception.TeamNotFoundException;
import com.ideas2it.cricbatball.info.TeamInfo;
import com.ideas2it.cricbatball.info.TeamPaginationInfo;
import com.ideas2it.cricbatball.service.TeamService;

/**
 * Used to create, read, update and delete team information.
 *
 * @author    M.Sathish Varma
 */
@Controller
public class TeamController {

    private TeamService teamService; 
    private static final Logger logger = Logger.getLogger(TeamController.class);

    @Autowired 
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    /**
     * Used to store the team information read from the browser.
     *
     * @param request -  request sent from the client through web browser.
     * @param response - response made for the request.
     * @throws ServletException - when there is any difficulty in request.
     * @throws IOException - if an input or output error is detected 
     *                       when the servlet handles
     */
    @RequestMapping(value = "/createTeam" , method = RequestMethod.POST)
    public String createTeam(@ModelAttribute TeamInfo teamInfo, Model model) {
        try {
            TeamPaginationInfo teamPaginationInfo = teamService
                    .storeTeamInformation(teamInfo);
            model.addAttribute("teamPaginationInfo", teamPaginationInfo);
        } catch (CricBatBallException e) {
            logger.error(e);
        }
        return "addplayers";
    }

    /**
     * Used to forward to the create team page.
     *
     * @param request -  request sent from the client through web browser.
     * @param response - response made for the request.
     * @throws ServletException - when there is any difficulty in request.
     * @throws IOException - if an input or output error is detected 
     *                       when the servlet handles
     */
    @RequestMapping("/forwardToCreateTeam")
    public String forwardToCreate(Model model) {
        TeamInfo teamInfo = new TeamInfo();
        model.addAttribute("team", teamInfo);
        return "createteam";
    }

    /**
     * Used to get the all teams information from the database.
     *
     * @param request -  request sent from the client through web browser.
     * @param response - response made for the request.
     * @throws ServletException - when there is any difficulty in request.
     * @throws IOException - if an input or output error is detected 
     *                       when the servlet handles
     */
    @RequestMapping(value="fetchTeams", method = RequestMethod.GET)
    public String getTeams(Model model) {
        int page = 1;
        TeamInfo teamInfo;
        RequestDispatcher dispatcher;
        try {  
            TeamPaginationInfo teamPaginationInfo = teamService
                    .getPaginationInfo(page);
            model.addAttribute("teamPaginationInfo", teamPaginationInfo);
        } catch (CricBatBallException e) {
            logger.error(e);
        }
        return "viewTeams";
    }

    /**
     * Used to get the all teams information from the database.
     *
     * @param request -  request sent from the client through web browser.
     * @param response - response made for the request.
     * @throws ServletException - when there is any difficulty in request.
     * @throws IOException - if an input or output error is detected 
     *                       when the servlet handles
     */
    @RequestMapping(value="fetchTeamsInfo", method = RequestMethod.GET)
    public void getTeamsInfo(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        TeamInfo teamInfo;
        RequestDispatcher dispatcher;
        try {
            TeamPaginationInfo teamPaginationInfo = teamService
                          .getPaginationInfo(Integer.parseInt(request
                          .getParameter(Constants.PAGE)));
            response.setContentType(Constants.APPLICATION_JSON);
            response.getWriter().write(teamPaginationInfo.getTeams()
                    .toString());
        } catch (CricBatBallException | IOException e) {
            String error = e.getMessage();
            request.setAttribute(Constants.ERROR, error);
            dispatcher = request.getRequestDispatcher("error.jsp");
            dispatcher.forward(request, response);
        }
    }

    /**
     * Used to get the particular team information  
     * with the help of data sent through request.
     *
     * @param request -  request sent from the client through web browser.
     * @param response - response made for the request.
     * @throws ServletException - when there is any difficulty in request.
     * @throws IOException - if an input or output error is detected 
     *                       when the servlet handles
     */
    @RequestMapping(value = "/getTeam", method = RequestMethod.GET)
    public String getTeam(@RequestParam(Constants.TEAMID) int teamId, 
            Model model) {
        TeamInfo teamInfo;
        try {
            TeamPaginationInfo teamPaginationInfo = teamService
                    .getTeamInformation(teamId);      
            int[] roleCount = teamPaginationInfo.getRoleCount();
            model.addAttribute(Constants.BATSMAN_COUNT, roleCount[0]);
            model.addAttribute(Constants.BOWLER_COUNT, roleCount[1]);
            model.addAttribute(Constants.WICKET_KEEPER_COUNT, 
                     roleCount[2]);
            model.addAttribute(Constants.ALL_ROUNDER_COUNT, roleCount[3]);
            model.addAttribute(Constants.TOTAL_COUNT, roleCount[4]);
            model.addAttribute(Constants.STATUS, teamPaginationInfo
                    .getStatus());            
            model.addAttribute("teamPaginationInfo", teamPaginationInfo);
        } catch (CricBatBallException e) {
            logger.error(e);
        }
        return "viewTeam";
    }

    /**
     * Used to add the players to the team.
     *
     * @param request -  request sent from the client through web browser.
     * @param response - response made for the request.
     * @throws ServletException - when there is any difficulty in request.
     * @throws IOException - if an input or output error is detected 
     *                       when the servlet handles
     */
    @RequestMapping(value="addPlayers", method = RequestMethod.POST)
    public String addPlayers(@RequestParam(required = false, 
            name = "playersid") String[] playersId, @RequestParam(Constants
            .TEAM_ID) int teamId) {
        try {
            teamService.addPlayersToTeam(teamId, playersId);
        } catch (CricBatBallException e) {
            logger.error(e);
        }
        return "redirect:/getTeam?teamid=" + teamId;
    }

    /**
    * Used to forward to the update team information like changing name
    * like adding and removing players.
    *
    * @param request -  request sent from the client through web browser.
    * @param response - response made for the request.
    * @throws ServletException - when there is any difficulty in request.
    * @throws IOException - if an input or output error is detected 
    *                       when the servlet handles
    */
    @RequestMapping(value="forwardToUpdatePage", method = RequestMethod.POST)
    public String forwardToUpdatePage(@RequestParam("editId") int id,
            Model model) {
        TeamInfo teamInfo;
        try {
            TeamPaginationInfo teamPaginationInfo = teamService
                    .getTeamInformation(id);
            model.addAttribute("teamPaginationInfo", teamPaginationInfo);
        } catch (CricBatBallException e) {
            logger.error(e);
        }
        return "updateteam";
    }

    /**
     * Used to update team information like changing name
     * like adding and removing players 
     * with the help of data sent through request.
     *
     * @param request -  request sent from the client through web browser.
     * @param response - response made for the request.
     * @throws ServletException - when there is any difficulty in request.
     * @throws IOException - if an input or output error is detected 
     *                       when the servlet handles
     */
    @RequestMapping(value="updateTeam", method = RequestMethod.POST)
    public String updateTeamInfo(@RequestParam(required = false, name = 
            Constants.TEAMID) int id, @RequestParam(required = false,
            name = Constants.ADDPLAYERS) String[] addPlayersId,
            @RequestParam(required = false, name = Constants
            .REMOVEPLAYERS) String[] removePlayersId, @RequestParam(required 
            = false, name = "teamname") String name) {
        try {
            teamService.updatePlayersInTeam(id, addPlayersId, removePlayersId,
                    name);
        } catch (CricBatBallException e) {
            logger.error(e);
        }
        return "redirect:getTeam?teamid=" + id;
    }

    /**
     * Used to remove the particular team information permanently 
     * with the help of data sent through request.
     *
     * @param request -  request sent from the client through web browser.
     * @param response - response made for the request.
     * @throws ServletException - when there is any difficulty in request.
     * @throws IOException - if an input or output error is detected 
     *                       when the servlet handles
     */
    @RequestMapping(value="deleteTeam", method = RequestMethod.POST)
    public String deleteTeam(@RequestParam("deleteId") int id) {
        try {
            teamService.removeTeam(id);
        } catch (CricBatBallException e) {
            logger.error(e);
        }
        return "redirect:/fetchTeams";
    }
}
