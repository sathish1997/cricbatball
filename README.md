## Project Title

* CricBatBall helps to manage the information of the Player, Match, Teams.
* CricBatBall can help you to create the team of the players and each team containing 11 players.
* CricBatBall can help you to create the match between the two teams.

### PreRequisites

The things needs to be installed to get started.

* Java
* Hibernate
* Mysql

### Installing

* First you need to install the java into your system.
* Next you have to create the database by installing mysql into your system.
* Then create the database and install the hibernate framework.

### Technologies Used

* Front-End: Jsp
* Back-End: Java
* Database Used: Mysql
* FrameWorks Used: Hibernate

### Build With

* Maven - Dependency Management.

### Author

* M.Sathish Varma - Software Engineer Trainee(Ideas2It Technology services).

