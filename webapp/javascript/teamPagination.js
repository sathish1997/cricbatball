function callAjax(pageno, choice, totalPages) {
    document.getElementById('prev').value = pageno;
    document.getElementById('next').value = pageno;    
    if (choice === '-1' && pageno > 0 && pageno <= totalPages) {
        pageno = pageno * 1 - 1;
        document.getElementById('prev').value = pageno;
        document.getElementById('next').value = pageno;
    } else if (choice === '+1' && pageno >= 0 && pageno < totalPages) {
        pageno = pageno * 1 + 1;
        document.getElementById('prev').value = pageno;
        document.getElementById('next').value = pageno;
    }
    httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        console.log('Unable to create XMLHTTP instance');
        return false;
    }
    httpRequest.open('GET', "fetchTeamsInfo?page="+pageno);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onreadystatechange =
    function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status == 200) {
                var array = httpRequest.response;
                var info = document.getElementsByClassName('cards');
                var editId = document.getElementsByClassName('editTeam');
                var deleteId = document.getElementsByClassName('deleteTeam');
                var innerCard = document.getElementsByClassName('teamTable');
                var i;
                for (i=0; i< array.length; i++) {
                    info[i].style.display='inline-block';
                    var linkId = array[i].teamName.toString();
                    var reference = linkId.link("getTeam?teamid="+array[i].id);
                    innerCard[i].innerHTML = "<html><body><table><tr><td>Id:</td><td>"+ array[i].id +
                        "</td></tr><tr><td>Name:</td><td>"+reference+"</td></tr><tr><td>Country:</td><td>"+
                        array[i].country+"</td</tr></table></body></html>";
                    editId[i].value = array[i].id;
                    deleteId[i].value=array[i].id;
                }
                for (i=array.length; i<=5; i++) {
                   info[i].style.display='none';
                }
            } else {
                console.log('Something went wrong..!!');
            }
        }
    }
}

