function callAjax(pageno, choice, totalPages) {
    document.getElementById('prev').value = pageno;
    document.getElementById('next').value = pageno;    
    if (choice === '-1' && pageno > 0 && pageno <= totalPages) {
        pageno = pageno * 1 - 1;
        document.getElementById('prev').value = pageno;
        document.getElementById('next').value = pageno;
    } else if (choice === '+1' && pageno >= 0 && pageno < totalPages) {
        pageno = pageno * 1 + 1;
        document.getElementById('prev').value = pageno;
        document.getElementById('next').value = pageno;
    }
    httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        console.log('Unable to create XMLHTTP instance');
        return false;
    }
    httpRequest.open('GET', "getMatchesInfo?page="+pageno);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onreadystatechange =
    function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status == 200) {
                var array = httpRequest.response;
                var info = document.getElementsByClassName('cards');
                var editId = document.getElementsByClassName('editMatch');
                var deleteId = document.getElementsByClassName('deleteMatch');
                var innerCard = document.getElementsByClassName('matchesTable');
                var i;
                for (i=0; i< array.length; i++) {
                    info[i].style.display='inline-block';
                    var linkId = array[i].id.toString();
                    var reference = linkId.link("getMatch?matchId="+array[i].id);
                    innerCard[i].innerHTML = "<html><body><table><tr><td>Id:</td><td>"+reference+
                        "</td></tr><tr><td>Name:</td><td>"+array[i].matchName
                        +"</td></tr><tr><td>Role:</td><td>"+array[i].location+
                        "</td></tr><tr><td>Date Of Play:</td><td>"+array[i].dateOfPlay
                        +"</td></tr><tr><td>Match Type:"+"</td><td>"+array[i].matchType
                        +"</td><tr><td>Starting Time:</td><td>"+array[i].startingTime+
                        "</td></tr></table></body></html>";
                    editId[i].value = array[i].id;
                    deleteId[i].value = array[i].id;
                }
                for (i=array.length; i<=5; i++) {
                    info[i].style.display='none';
                }
            } else {
                console.log('Something went wrong..!!');
            }
        }
    }
}
