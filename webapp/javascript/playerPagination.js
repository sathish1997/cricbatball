function callAjax(pageno, choice, totalPages) {
    document.getElementById('prev').value = pageno;
    document.getElementById('next').value = pageno;    
    if (choice === '-1' && pageno > 0 && pageno <= totalPages) {
        pageno = pageno - 1;
        document.getElementById('prev').value = pageno;
        document.getElementById('next').value = pageno;
    } else if (choice === '+1' && pageno >= 0 && pageno < totalPages) {
        pageno = pageno * 1 + 1;
        document.getElementById('prev').value = pageno;
        document.getElementById('next').value = pageno;
    }
    httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        console.log('Unable to create XMLHTTP instance');
        return false;
    }
    httpRequest.open('GET', "displayPlayersInfo?page="+pageno);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onreadystatechange =
    function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status == 200) {
                var array = httpRequest.response;
                var info = document.getElementsByClassName('cards');
                var editId = document.getElementsByClassName('editplayer');
                var deleteId = document.getElementsByClassName('deleteplayer');
                var innerCard = document.getElementsByClassName('playersTable');
                var i;
                for (i=0; i< array.length; i++) {
                    info[i].style.display='inline-block';
                    var linkId = array[i].id.toString();
                    var reference = linkId.link("fetchPlayer?id=" + array[i].id);
                    innerCard[i].innerHTML = "<html><body><table><tr><td>Id:</td><td>" + reference +
                        "</td></tr><tr><td>Name:</td><td>" + array[i].names + "</td></tr><tr><td>Role:</td><td>"
                        + array[i].role + "</td></tr><tr><td>Country:</td><td>" + array[i].country + 
                        "</td</tr><tr><td>Batting Style:" + "</td><td>" + array[i].battingStyle + 
                        "</td><tr><td>Bowling Style:</td><td>" + array[i].bowlingStyle +
                        "</td></tr><tr><td>Date Of Birth:</td><td>" + array[i].dateOfBirth + 
                        "</td></tr></table></body></html>";
                    editId[i].value = array[i].id;
                    deleteId[i].value=array[i].id;
                }
                for (i=array.length; i<=5; i++) {
                    info[i].style.display='none';
                }
            } else {
                console.log('Something went wrong..!!');
            }
        }
    }
}

function deletePopup() {
    document.getElementById("modal").style.display = "block";
}
