function getBallresult(overId, firstBatsmanId, secondBatsmanId, bowlerId, matchId, strikerId) {
    httpRequest = new XMLHttpRequest();
    var overid =  document.getElementById("overId");
    var striker =  document.getElementById("strikerId");
    if (!httpRequest) {
        console.log('Unable to create XMLHTTP instance');
        return false;
    }
    httpRequest.open('GET', "getBallResult?overId="+overid.value+"&batsmanId="+firstBatsmanId
            +"&bowlerId="+bowlerId+"&matchId="+matchId+"&strikerId="+striker.value
            +"&secondBatsmanId="+secondBatsmanId);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onreadystatechange =
    function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status == 200) {
                var overid =  document.getElementById("overId");
                var striker =  document.getElementById("strikerId");
                var scoreStats = httpRequest.response;
                var firstBatsman = document.getElementById("firstBatsman");
                var secondBatsman = document.getElementById("secondBatsman");
                var batsmanRun = document.getElementById("strikerRuns");
                var fourCount = document.getElementById("strikerFours");
                var sixCount = document.getElementById("strikerSixes");
                var firstBatsmanBalls = document.getElementById("strikerBalls");
                var secondBatsmanRun = document.getElementById("nonStrikerRuns");
                var secondFourCount = document.getElementById("nonStrikerFours");
                var secondSixCount = document.getElementById("nonStrikerSixes");
                var secondBatsmanBalls = document.getElementById("nonStrikerBalls");
                var overRun = document.getElementById("runsInOver");
                var wicketCount = document.getElementById("wicketsInOver");
                var ballInfo = document.getElementById("ballInfo");
                var overCount = document.getElementById("overCount");
                var overInformation = document.getElementById("overInformation").innerHTML;
                var scoreInformation = document.getElementById("scoreInformation").innerHTML;
                var wicketInformation = document.getElementById("wicketInformation").innerHTML;
                var extras = document.getElementById("extras").innerHTML;
                var extrasInfo = document.getElementById("extrasInfo");
                var ball = document.createElement("div");
                overid.value = scoreStats.overNumber;
                striker.value = scoreStats.strikerId;
                if ("Wide" === scoreStats.ballResult || "No_Ball" === scoreStats.ballResult) {
                    overRun.value = (overRun.value * 1) + scoreStats.ballRuns;
                    document.getElementById("extras").innerHTML = (extras * 1) +  scoreStats.ballRuns;
                    document.getElementById("scoreInformation").innerHTML = (scoreInformation * 1) 
                            + scoreStats.ballRuns;
                }
                if ("Leg_Byes" === scoreStats.ballResult || "Byes" === scoreStats.ballResult) {
                    if (striker.value == firstBatsmanId) {
                        firstBatsmanBalls.value = (firstBatsmanBalls.value * 1) + 1;
                        striker.value = secondBatsmanId;
                    } else {
                        secondBatsmanBalls.value = (secondBatsmanBalls.value * 1) + 1;
                        striker.value = firstBatsmanId;
                    }
                    overCount.value = (scoreStats.overNumber * 1 - 1) + "." + scoreStats.ballNumber;
                    document.getElementById("extras").innerHTML = (extras * 1) +  scoreStats.ballRuns;
                    document.getElementById("scoreInformation").innerHTML = (scoreInformation * 1) 
                            + scoreStats.ballRuns;
                    document.getElementById("overInformation").innerHTML = (scoreStats.overNumber * 1 - 1) 
                            + "." + scoreStats.ballNumber;
                }
                if ("One" === scoreStats.ballResult || "Three" === scoreStats.ballResult ||  
                        "Five" === scoreStats.ballResult) {
                    if (striker.value == firstBatsmanId) {
                        firstBatsmanBalls.value = (firstBatsmanBalls.value * 1) + 1;
                        batsmanRun.value = (batsmanRun.value * 1) + scoreStats.ballRuns;
                        striker.value = secondBatsmanId;
                    } else {
                        secondBatsmanBalls.value = (secondBatsmanBalls.value * 1) + 1;
                        secondBatsmanRun.value = (secondBatsmanRun.value * 1) + scoreStats.ballRuns;
                        striker.value = firstBatsmanId;
                    }
                    overRun.value = (overRun.value * 1) + scoreStats.ballRuns;
                    overCount.value = (scoreStats.overNumber * 1 - 1) + "." + scoreStats.ballNumber;
                    document.getElementById("scoreInformation").innerHTML = (scoreInformation * 1) 
                            + scoreStats.ballRuns;
                    document.getElementById("overInformation").innerHTML = (scoreStats.overNumber * 1 - 1) 
                            + "." + scoreStats.ballNumber;
                }
                if ("Two" === scoreStats.ballResult || "Four" === scoreStats.ballResult || 
                        "Six" === scoreStats.ballResult || "Dot" === scoreStats.ballResult) {
                    if (striker.value == firstBatsmanId) {
                        firstBatsmanBalls.value = (firstBatsmanBalls.value * 1) + 1;
                        batsmanRun.value = (batsmanRun.value * 1) + scoreStats.ballRuns;
                        striker.value = firstBatsmanId;
                    } else {
                        secondBatsmanBalls.value = (secondBatsmanBalls.value * 1) + 1;
                        secondBatsmanRun.value = (secondBatsmanRun.value * 1) + scoreStats.ballRuns;
                        striker.value = secondBatsmanId;
                    }
                    overRun.value = (overRun.value * 1) + scoreStats.ballRuns;
                    overCount.value = (scoreStats.overNumber * 1 - 1) + "." + scoreStats.ballNumber;
                    document.getElementById("scoreInformation").innerHTML = (scoreInformation * 1) 
                            + scoreStats.ballRuns;
                    document.getElementById("overInformation").innerHTML = (scoreStats.overNumber * 1 - 1) 
                            + "." + scoreStats.ballNumber;
                }
                if ("Four" === scoreStats.ballResult) {
                    if (striker.value == firstBatsmanId) {
                        fourCount.value = (fourCount.value * 1) + 1;
                    } else {
                        secondFourCount.value = (secondFourCount.value * 1) + 1;
                    }
                }
                if ("Six" === scoreStats.ballResult) {
                    if (striker.value == firstBatsmanId) {
                        sixCount.value = (sixCount.value * 1) + 1;
                    } else {
                        secondSixCount.value = (secondSixCount.value * 1) + 1;
                    } 
                }
                if ("Wicket" === scoreStats.ballResult) {
                    wicketCount.value = (wicketCount.value * 1) + scoreStats.wickets;
                    document.getElementById("wicketInformation").innerHTML = (wicketInformation * 1) 
                            + scoreStats.wickets;
                    if (striker.value == firstBatsmanId) {
                        firstBatsmanBalls.value = (firstBatsmanBalls.value * 1) + 1;
                    } else {
                        secondBatsmanBalls.value = (secondBatsmanBalls.value * 1) + 1;
                    }
                    document.getElementById("wicketId").value = striker.value;
                    document.getElementById("chooseBatsman").style.display = "block";
                    document.getElementById("footer").style.display = "none";
                }
                if (1 === scoreStats.ballNumber || 0 === scoreStats.ballNumber) {
                    ballInfo.innerHTML = "";
                    ball.setAttribute("class", "ball")
                    ball.innerHTML = scoreStats.ballResult;
                    ballInfo.appendChild(ball);
                    document.getElementById("footer").style.display = "block";
                    document.getElementById("chooseBowler").style.display = "none";
                } else {
                    ball.setAttribute("class", "ball")
                    ball.innerHTML = scoreStats.ballResult;
                    ballInfo.appendChild(ball);
                }
                if (6 == scoreStats.ballNumber && "Wicket" != scoreStats.ballResult) {
                    document.getElementById("footer").style.display = "none";
                    document.getElementById("chooseBowler").style.display = "block";
                }
                document.getElementById("scoreCountInfo").value = document
                        .getElementById("scoreInformation").innerHTML;
                document.getElementById("firstPlayerballs").value = firstBatsmanBalls.value;
                document.getElementById("secondPlayerBalls").value = secondBatsmanBalls.value;
                document.getElementById("firstPlayerSixes").value = sixCount.value;
                document.getElementById("secondPlayerSixes").value = secondSixCount.value;
                document.getElementById("firstPlayerFours").value = fourCount.value;
                document.getElementById("secondPlayerFours").value = secondFourCount.value;
                document.getElementById("firstPlayerRuns").value = batsmanRun.value;
                document.getElementById("secondPlayerBalls").value = secondBatsmanBalls.value;
                document.getElementById("secondPlayerRuns").value = secondBatsmanRun.value;
                document.getElementById("extrasCountInfo").value = document
                        .getElementById("extras").innerHTML;
                document.getElementById("ballNumber").value = scoreStats.ballNumber;
            } else {
                console.log('Something went wrong..!!');
            }
        }
    }
}

function chooseBowler() {
   document.playerChange.action = "chooseBowler";
}

function chooseBatsman() {
   document.playerChange.action = "chooseBatsman";
}
