<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel = "stylesheet" href = "/css/createplayerstyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="navbar">
            <form action="displayPlayers" method="get">
            <button type="submit" class="backbutton">
                 <i style="font-size:50px; color:white;" class="fa fa-arrow-left"></i>
            </button>
            </form>
             <span><center>Add Player</center></span>
            <a class="home" style="font-size:50px;" href="manage.jsp"><i class="fa fa-fw fa-home"></i></a>
        </div>
        <div align="center">  
            <form:form action = "createplayer" method ="post" modelAttribute="playerInfo" 
                enctype="multipart/form-data">
                <table>
                    <center> 
                        <input type = "file" name ="playerImage" value = "select profile picture"/> 
                    </center>
                    <tr>
                        <td>Name:</td>
                        <td style = "margin-top =10%;"><form:input type="text" path = "name" 
                            title="enter alphabets only No Integers allowed length should be less than 20"
                            placeholder="name" maxlength="20" pattern="[A-Za-z][A-Za-z\s]*{20}" 
                            required="required"/></td>
                    </tr>
                    <tr>
                        <td>Country:</td>
                        <td>
                            <form:select placeholder="country" default ="none" path = "country" 
                            required="required">
                                <option selected>India</option>
                                <option>Australia</option>
                                <option>South_Africa</option>
                                <option>West_Indies</option>
                                <option>New_Zealand</option>
                                <option>Pakistan</option>
                                <option>Bangladesh</option>
                                <option>Afghanistan</option>
                                <option>England</option>
                                <option>Sri_Lanka</option>
                            </form:select>   
                        </td>
                    </tr>
                    <tr>
                        <td>Role:</td>
                        <td>
                            <form:select placeholder ="role" path = "role" 
                            required="required">
                                <option selected>Batsman</option>
                                <option>Bowler</option>
                                <option>All-Rounder</option>
                                <option>Wicket-keeper</option>
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td>Batting-Style:</td>
                        <td>
                            <form:select path = "battingStyle">
                                <option selected>Right-handed</option>
                                <option>Left-handed</option>
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td>Bowling-Style:</td>
                        <td>
                            <form:select path = "bowlingStyle">
                                <option selected>Right Arm Fast</option>
                                <option>Left Arm Fast</option>
                                <option>Leg Spinner</option>
                                <option>Off Spinner</option>
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td>Date-Of-Birth:</td>
                        <td>
                            <form:input type="date" path="dateOfBirth" max = "2008-01-01" 
                                min = "1930-01-01" required="required"/>
                        </td>
                    </tr>  
                    <tr>
                        <td>Address</td>
                        <td> <form:input type="text" path = "address" placeholder = "address"/></td>
                    </tr>
                    <tr>
                        <td>Mobile Number</td>
                        <td><form:input type="tel" title="enter mobile number only accepted 10 digit long " 
                                 path= "mobileNumber" pattern="^\d{10}$" placeholder = "Mobile Number"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Pincode</td>
                        <td> <form:input type="tel" title="enter pincode 6 digit long only accepted "
                            path="pincode" 
                            pattern="^\d{6}$" placeholder = "pincode"/></td>
                    </tr>                  
                </table><br>
                    <center><button type="submit" class="savebutton" name = "submit">save</button></center>
            </form:form>
        </div>
    </body>
</html>
