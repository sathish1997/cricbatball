<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/addplayerstyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="navbar">
            <a class="back" href="fetchTeams"><i style="font-size:50px;" class="fa fa-arrow-left"></i></a>
            <h3><center>Add Players To Team</center></h3>
            <a class="home" href="createplayer.jsp"><i style="font-size:60px;float:right;" class="fa fa-plus-square"></i></a>
        </div>
        <div class = "teamform"><br>
        <form action = "teamcontroller" method =post>
                <center>Id: ${teamPaginationInfo.id}</center><center><br>
                Name: <input type="text" name = "name" 
                            title="enter alphabets only No Integers allowed length should be less than 20" 
                            placeholder="name" value="${teamPaginationInfo.teamInfo.name}" 
                            pattern="[A-Za-z][A-Za-z\s]*{20}" required>&nbsp;&nbsp;&nbsp;
               Country: <input type = "text" name = "country" value ="${teamPaginationInfo.teamInfo.country}">
            </form>
            </div><br>
        <form:form action = "addPlayers" method="post" align="center">
        <div class = "information" id="welcomeDiv">        
              <c:forEach var="playerInfo" items="${teamPaginationInfo.playersNotInTeam}">
                <div class= "cards">
                 <table>
                <tr>
                    <td>Id:</td>
                    <td><c:out value="${playerInfo.id}" /></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><c:out value="${playerInfo.name}" /></td>
                </tr>         
                <tr>
                    <td>Country:</td>
                    <td><c:out value="${playerInfo.country}" /></td>
                </tr>
                <tr>
                    <td>Role:</td>
                    <td><c:out value="${playerInfo.role}" /></td>
                </tr>
                <tr>
                    <td>Batting Style:</td>
                    <td><c:out value="${playerInfo.battingStyle}" /></td>
                </tr>
                <tr>
                    <td>Bowling Style:</td>
                    <td><c:out value="${playerInfo.bowlingStyle}" /></td>
                </tr>
                <tr>
                    <td>Date Of Birth:</td>
                    <td><c:out value="${playerInfo.dateOfBirth}" /></td>
                </tr><tr><td>
                <input type="checkbox" name="playersid" value="${playerInfo.id}"> click to select</td></tr>
               </table>
              </div>
           </c:forEach>
        </div>
        <footer>
            <center>
               <input type ="hidden" name = "teamId" value= "${teamPaginationInfo.id}">
               <button class = "viewbutton" type = "submit" name = "submit" value ="addPlayers">
                   Save Team
               </button>
            </center>
        </footer>
        </form:form>
    </body>
</html>
            
