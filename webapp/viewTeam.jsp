<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/viewTeamStyel.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="navbar">  
            <a href="fetchTeams">
                <button type="submit" name = "submit" value = "getTeams" class="backbutton">
                    <i style="font-size:30px; color:white;" class="fa fa-arrow-left"></i>
                </button>
            </a>
            <h3><center>Team Information</center></h3>
            <a class="home" style="font-size:30px;color:white;" href="manage.jsp"><i class="fa fa-fw fa-home"></i></a>
        </div><br>
        <div class = "teaminfo">
        <center>Id: ${teamPaginationInfo.teamInfo.id}</center>
        <center><br>
            Name:
            <input type="text" name = "name" 
                title="enter alphabets only No Integers allowed length should be less than 20" type="text"
                placeholder="name" value="${teamPaginationInfo.teamInfo.name}" 
                pattern="[A-Za-z][A-Za-z\s]*{20}" 
                maxlength="20" readonly>&nbsp;&nbsp;&nbsp;
            Country:
            <input type = "text" name = "country" value ="${teamPaginationInfo.teamInfo.country}" readonly>
            Status:
            <input type = "text" name = "status" value = "${status}" readonly>
        </center>
        </div>
        <div class = "information"><br>     
            <c:forEach var="player" items="${teamPaginationInfo.teamInfo.players}">
                <div class= "cards">
                    <table>
                    <tr>
                        <td>Id:</td>
                        <td><c:out value="${player.id}" /></td>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td><c:out value="${player.name}" /></td>
                    </tr>         
                    <tr>
                        <td>Country:</td>
                        <td><c:out value="${player.country}" /></td>
                    </tr>
                    <tr>
                        <td>Role:</td>
                        <td><c:out value="${player.role}" /></td>
                    </tr>
                    <tr>
                        <td>Batting Style:</td>
                        <td><c:out value="${player.battingStyle}" /></td>
                    </tr>
                    <tr>
                        <td>Bowling Style:</td>
                        <td><c:out value="${player.bowlingStyle}" /></td>
                    </tr>
                    <tr>
                        <td>Date Of Birth:</td>
                        <td><c:out value="${player.dateOfBirth}" /></td>
                    </tr>
                    </table>
                </div>
            </c:forEach>
        </div>
        <div class="teamStatus">
            <div class="roleSpecs">
                <span id ="count">Minimum Needed Player Roles</span>
                <table>
                    <tr>
                        <td>Batsman Count</td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>Bowler Count</td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>Wicket-Keeper Count</td>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td>other Count:</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>Total Count:</td>
                        <td>11</td>
                    </tr>
                </table>
            </div>
            <div class="roleSpecs">
                <span id ="count">Selected Team Count</span>
                <table>
                    <tr>
                        <td>Batsman Count</td>
                        <td>${batsmanCount}</td>
                    </tr>
                    <tr>
                        <td>Bowler Count</td>
                        <td>${bowlerCount}</td>
                    </tr>
                    <tr>
                        <td>Wicket-Keeper Count</td>
                        <td>${wicketKeeperCount}</td>
                    </tr>
                    <tr>
                        <td>All-Rounder Count:</td>
                        <td>${allRounderCount}</td>
                    </tr>
                    <tr>
                        <td>Total Count:</td>
                        <td>${totalCount}</td>
                    </tr>
                </table>
            </div>
      </form>
    </body>
</html>
