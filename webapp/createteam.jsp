<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/createteamstyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="navbar">
            <a href="fetchTeams">
            <button type="submit" name = "submit" value = "getTeams" class="backbutton">
                 <i style="font-size:30px; color:white;" class="fa fa-arrow-left"></i>
            </button>
            </a>
             <span><center>Create Team</center></span>
            <a class="home" style="font-size:40px;" href="manage.jsp"><i class="fa fa-fw fa-home"></i></a>
        </div>
        <div class="team"><br><br>
        <form:form action = "createTeam" method ="post" modelAttribute="team"> <center>
                Name <form:input type="text" path = "name" 
                            title="enter alphabets only No Integers allowed length should be less than 20"
                            placeholder="name" pattern="[A-Za-z][A-Za-z\s]*{20}" required="required"/>
               Country:
           <form:select placeholder="country" default ="none" path = "country" required = "required">
               <option selected>India</option>
               <option>Australia</option>
               <option>South_Africa</option>
               <option>West_Indies</option>
               <option>New_Zealand</option>
               <option>Pakistan</option>
               <option>Bangladesh</option>
               <option>Afghanistan</option>
               <option>England</option>
               <option>Sri_Lanka</option>
           </form:select>
           <button class="savebutton"> Save and Continue </button>
           </center> 
        </form:form>
        </div>
    </body>
</html>
            
