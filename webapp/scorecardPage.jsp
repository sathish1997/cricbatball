<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/scoreCardPage.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="/javascript/getBallResult.js">
        </script>
    </head>
    <body>
        <div class="navbar">
            <a class="back" href="manage.jsp">
                 <i style="font-size:50px;" class="fa fa-arrow-left"></i>
            </a>
            <h3>Let's Begin</h3>
            <a class="home" href="forwardToCreateMatch">
                <i style="font-size:60px;float:right;" class="fa fa-plus-square"></i>
            </a>
        </div>
        <input type = "hidden" id = "overId" value = "${overDetailInfo.overInfo.id}"/>
        <input type = "hidden" id = "strikerId" value = "${overDetailInfo.firstBatsman.id}"/>
        <div class = "score">
            <div id = "holder">
                <center> Over </center>
                <div id = "overInformation">${overDetailInfo.overInfo.overNumber - 1}
                        .${overDetailInfo.ballNumber}
                </div>
            </div>
            <div id = "holder">
                <center> Score </center>
                <div id = "scoreInformation">${overDetailInfo.scoreCountInfo}
                </div>
            </div>
            <div id = "holder">
                <center> Wicket </center>
                <div id = "wicketInformation">${overDetailInfo.wicketCountInfo}
                </div>
            </div>
        </div><br>
        <div class = "infoHeading"><center> Batsman Info </center></div>
        <div class = "scoreCard">
            <div class = "batsmanInfo">
                <div class = "name">
                    <div class = "heading"><center> Name </center></div>
                    <div id = "firstBatsman">
                        ${overDetailInfo.firstBatsman.name}
                    </div>
                    <div id = "secondBatsman">
                        ${overDetailInfo.secondBatsman.name}
                    </div> 
                </div>
                <div class = "batsmanRuns">
                    <div class = "heading"><center> Runs <center></div>
                    <div id = "firstBatsmanRun">
                        <input type="text" id = "strikerRuns" value = "${overDetailInfo.firstPlayerRuns}">
                    </div>
                    <div id = "secondBatsmanRun">
                        <input type="text" id = "nonStrikerRuns" value = "${overDetailInfo.secondPlayerRuns}">
                    </div>
                </div>
                <div class = "balls">
                    <div class = "heading"><center> balls </center></div>
                    <div id = "firstBatsmanBalls">
                        <input type="text" id = "strikerBalls" value = "${overDetailInfo.firstPlayerballs}">
                    </div>
                    <div id = "secondBatsmanBalls">
                        <input type="text" id = "nonStrikerBalls" value = "${overDetailInfo.secondPlayerBalls}">
                    </div>
                </div>
                <div class = "fours">
                    <div class = "heading"><center> fours </center></div>
                    <div id = "firstBatsmanFours">
                        <input type="text" id = "strikerFours" value = "${overDetailInfo.firstPlayerFours}">
                    </div>
                    <div id = "secondBatsmanFours">
                        <input type="text" id = "nonStrikerFours" value = "${overDetailInfo.secondPlayerFours}">
                    </div>
                </div>
                <div class = "sixes">
                    <div class = "heading"><center> sixes </center></div>
                    <div id = "firstBatsmanSixes">
                        <input type="text" id = "strikerSixes" value = "${overDetailInfo.firstPlayerSixes}">
                    </div>
                    <div id = "secondBatsmanSixes">
                        <input type="text" id = "nonStrikerSixes" value = "${overDetailInfo.secondPlayerSixes}">
                    </div>
                </div>
            </div><br>
            <div class = "infoHeading"><center> Bowler Info </center></div>
            <div class = "bowlerInfo">
                <div class = "name">
                    <div class = "heading"><center> Name </center></div>
                    <div id="bowlerName">
                        ${overDetailInfo.bowler.name}
                    </div>
                </div>
                <div class = "overs">
                    <div class = "heading"><center> Overs </center></div>
                    <div id = "overNumber">
                        <input type="text" id = "overCount" value = "0">
                    </div>
                </div>
                <div class = "maiden">
                    <div class = "heading"><center> Maiden </center></div>
                    <div id = "maidenInfo">
                        <input type="text" id = "maidenCount" value = "0">
                    </div>
                </div>
                <div class = "bowlerRuns">
                    <div class = "heading"><center> Runs </center></div>
                    <div id = "runsInfo">
                        <input type="text" id = "runsInOver" value = "0">
                    </div>
                </div>
                <div class = "wicket">
                    <div class = "heading"><center> Wicket </center></div>
                    <div id = "wicketInfo">
                        <input type="text" id = "wicketsInOver" value = "0">
                    </div>
                </div>
            </div>
            <div id= "holder">
                <center> Extras </center>
                <div id = "extras">${overDetailInfo.extrasCountInfo}
                </div>
            </div><br><br>
            <div class = "infoHeading"><center> This Over </center></div>
            <div id = "ballInfo">
                <div class = "ball">
                </div>
            </div>
            <footer> <center>
                <div id = "footer">
                <input type ="submit" onclick = "getBallresult(${overDetailInfo.overInfo.id},
                    ${overDetailInfo.firstBatsman.id}, ${overDetailInfo.secondBatsman.id}, 
                    ${overDetailInfo.bowler.id}, ${overDetailInfo.overInfo.match.id},
                    ${overDetailInfo.firstBatsman.id})" value = "Bowl Delivery"/>
                </center>
                </div>
                    <form:form name = "playerChange" action = "" method = "post" 
                        modelAttribute = "overDetailInfo">
                        <div id = "chooseBowler">
                            <center> <input type = "submit" onclick = "chooseBowler()"
                                value = "choose Next Bowler"/> </center>
                        </div>
                        <div id = "chooseBatsman">
                            <center> <input type = "submit" onclick = "chooseBatsman()" 
                                value = "choose Next Batsman"/> </center>
                        </div>
                        <form:hidden path = "firstBatsman.id"/>
                        <form:hidden path = "secondBatsman.id"/>
                        <form:hidden path = "overInfo.overNumber"/>
                        <form:hidden path = "overInfo.id"/>
                        <form:hidden path = "bowler.id"/>
                        <form:hidden path = "firstTeam.id"/>
                        <form:hidden path = "secondTeam.id"/>
                        <form:hidden path = "overInfo.match.id"/>
                        <form:input type = "hidden" id = "ballNumber"  path = "ballNumber"/>
                        <form:input type = "hidden" id = "firstPlayerRuns" path = "firstPlayerRuns" 
                            value = "0"/>
                        <form:input type = "hidden" id = "secondPlayerRuns" path = "secondPlayerRuns" 
                            value = "0"/>
                        <form:input type = "hidden" id = "firstPlayerballs" path = "firstPlayerballs" 
                            value = "0"/>
                        <form:input type = "hidden" id = "secondPlayerBalls" path = "secondPlayerBalls" 
                            value = "0"/>
                        <form:input type = "hidden" id = "firstPlayerSixes" path = "firstPlayerSixes" 
                            value = "0"/>
                        <form:input type = "hidden" id = "firstPlayerFours" path = "firstPlayerFours" 
                            value = "0"/>
                        <form:input type = "hidden" id = "secondPlayerSixes" path = "secondPlayerSixes" 
                            value = "0"/>
                        <form:input type = "hidden" id = "secondPlayerFours" path = "secondPlayerFours" 
                            value = "0"/>
                        <form:input type = "hidden" id = "scoreCountInfo" path = "scoreCountInfo" value = "0"/>
                        <form:input type = "hidden" id = "wicketCountInfo" path = "wicketCountInfo" 
                            value = "0"/>
                        <form:input type = "hidden" id = "extrasCountInfo" path = "extrasCountInfo"  
                            value = "0"/>
                        <input type = "hidden" id = "wicketId" name = "wicketId"/>
                    </form:form>
            </footer>
         </div>
    </body>
</html>
                
                    
                
                     
        
