<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel = "stylesheet" href = "css/createplayerstyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/
            font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <center>
            <p>${error}</p><br>
            <form action="usercontroller" method="post">
                <a href="manage.jsp">Back to Home</a>
            </form>
        </center>        
    </body>
</html>
