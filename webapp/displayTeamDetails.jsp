<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/displayTeamDetails.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="/javascript/matchPagination.js">
        </script>
        <script type="text/javascript" src="/javascript/checkBoxLimitBowler.js">
        </script>
        <script type="text/javascript" src="/javascript/checkBoxLimitBatsman.js">
        </script>
    </head>
    <body>
        <div class="navbar">
            <a class="back" href="manage.jsp">
                 <i style="font-size:50px;" class="fa fa-arrow-left"></i>
            </a>
            <h3>Team Information View</h3>
            <a class="home" href="forwardToCreateMatch">
                <i style="font-size:60px;float:right;" class="fa fa-plus-square"></i>
            </a>
        </div>
        <form action = "startMatch" name = "checkBoxLimit" method = "post">
            <div class ="teams">
                <div class = "firstTeam">
                    <h2>Select Two Batsman</h2>
                    <center>${matchPaginationInfo.firstTeam.name}</center><br>
                    <table>
                        <c:forEach var="player" items="${matchPaginationInfo.firstTeam.players}">
                            <tr>
                                <td> ${player.name} </td>
                                <td> ${player.role} </td>
                                <td> <input type ="checkbox" onclick = "batsmanCheckBoxLimit(2)" 
                                         name ="batsmanIds" value ="${player.id}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class = "secondTeam">
                    <h2>Select One Bowler</h2>
                    <center>${matchPaginationInfo.secondTeam.name}</center><br>
                    <table>
                        <c:forEach var="player" items="${matchPaginationInfo.secondTeam.players}">
                            <tr>
                                <td> ${player.name} </td>
                                <td> ${player.bowlingStyle} </td>
                                <td> <input type ="checkbox" onclick = "bowlerCheckBoxLimit(1)" name ="bowlerIds"
                                         value ="${player.id}"/> </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
            <footer>
                <center>
                    <input type = "hidden" name = "matchId" value = "${matchPaginationInfo.matchInfo.id}"/>
                    <input type = "hidden" name = "firstTeamId" value = "${matchPaginationInfo.firstTeam.id}"/>
                    <input type = "hidden" name = "secondTeamId" value = "${matchPaginationInfo.secondTeam.id}"/>
                    <input type ="submit"/>
                </center>
            </footer>
        </form>
    </body>
</html>
