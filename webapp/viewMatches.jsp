<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/viewMatchesStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="/javascript/matchPagination.js">
        </script>
    </head>
    <body>
        <div class="navbar">
            <a class="back" href="manage.jsp"><i style="font-size:50px;" class="fa fa-arrow-left"></i></a>
            <h3><center>Match Information</center></h3>
            <a class="home" href="forwardToCreateMatch">
                <i style="font-size:60px;float:right;" class="fa fa-plus-square"></i>
            </a>
        </div>
        <div class = "information" align="center">         
            <c:forEach var="match" items="${matchPaginationInfo.matchesInfo}">
                <div class= "cards">
                <div class ="matchesTable">
                <table>
                    <tr>
                        <td>Id:</td>
                        <td><a href="getMatch?matchId=${match.id}">
                            <c:out value="${match.id}"/></a></td>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td><c:out value="${match.name}" /></td>
                    </tr>         
                    <tr>
                        <td>Location:</td>
                        <td><c:out value="${match.location}" /></td>
                    </tr>
                    <tr>
                        <td>Date of Play:</td>
                        <td><c:out value="${match.dateOfPlay}" /></td>
                    </tr>
                    <tr>
                        <td>Starting Time:</td>
                        <td><c:out value="${match.startingTime}" /></td>
                    </tr>
                    <tr>
                        <td>Match Type:</td>
                        <td><c:out value="${match.matchType}" /></td>
                    </tr>
                </table>
                </div>
                    <div>
                        <center>
                            <form action="forwardToUpdateMatch" method="post">
                            <input type = "hidden" class="editMatch" name= "matchId" value = "${match.id}"/>
                            <button class="edit">
                                <i style="font-size:24px" class="fa">&#xf044;</i>
                            </button>
                            </form>
                            <form action="deleteMatch" method="post">
                            <input type = "hidden" class="deleteMatch" name= "matchId" value = "${match.id}"/>
                            <button class="delete">
                                <i style="font-size:24px" class="fa">&#xf014;</i>
                           </button>
                           </form>
                           <form action="startPlay" method="post">
                           <input type = "hidden" class="deleteMatch" name= "matchId" value = "${match.id}"/>
                           <button class="delete">
                               <i class="fa fa-play-circle" style="font-size:24px;"></i>
                           </button>
                           </form>
                        </center>
                    </div>
                </div>
            </c:forEach>
        </div>
        <center>
           <footer class = "matchfooter">
                <c:if test="${matchPaginationInfo.page gt 0}">
                    <button id = "prev" value=1
                          onclick ="callAjax(this.value, '-1', ${matchPaginationInfo.totalPages});"> 
                        Previous
                    </button>
                </c:if>
                <c:forEach begin="1" end="${matchPaginationInfo.totalPages}" var="i">
                    <input type="hidden" name="currentPage" class="currentpage" value="${i}">
                    <button onclick="callAjax(${i}, 'content', ${matchPaginationInfo.totalPages});">${i}</button>
                </c:forEach>
                <c:if test="${matchPaginationInfo.page lt matchPaginationInfo.totalPages}">
                    <button id ="next" value =1 
                        onclick="callAjax(this.value, '+1', ${matchPaginationInfo.totalPages});">Next</button>
                </c:if>
            </footer>
         </center>
    </body>
</html>
            
