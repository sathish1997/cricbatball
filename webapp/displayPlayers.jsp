<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/samplestyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src = "/javascript/playerPagination.js">
             
        </script>
    </head>
    <body>
        <div id = "modal">
            <div id = "content">
                Alert want to delete player?
                <center> <button> 
                             <i style="font-size:24px" class="fa">&#xf00d;</i>
                         </button>
                </center>
            </div>
        </div>
        <div class="navbar">
            <a class="back" href="manage.jsp"><i style="font-size:50px;" class="fa fa-arrow-left"></i></a>
            <h3><center>Player Information</center></h3>
            <a class="home" href="forwardToCreate"><i style="font-size:60px;float:right;" 
                 class="fa fa-plus-square"></i></a>
        </div>   
        <div class="information">
            <c:forEach var="player" items="${playerPaginationInfo.playersInfo}">
              <div class="cards">
                  <div class ="playersTable">
                  <table>
                    <tr>
                    <th>id</th>
                    <td><a href="fetchPlayer?id=${player.id}"><c:out value="${player.id}" />
                        </a></td>
                    </tr>
                    <tr>
                    <th>name</th>
                    <td><c:out value="${player.name}" /></td>
                    </tr>
                    <tr>
                    <th>country</th>
                    <td><c:out value="${player.country}" /></td>
                    </tr>
                    <tr>
                    <th>Role</th>
                    <td><c:out value="${player.role}" /></td>
                    </tr>
                    <tr>
                    <th>Batting Style</th>
                    <td><c:out value="${player.battingStyle}" /></td>
                    </tr>
                    <tr>
                    <th>Bowling Style</th>
                    <td><c:out value="${player.bowlingStyle}" /></td>
                    </tr>
                    <tr>
                    <th>Date Of Birth</th>
                    <td><c:out value="${player.dateOfBirth}" /></td>
                    </tr>
               </table>
               </div>
               <div><center>
               <form action = "getPlayer" method = "post">
               <input type="hidden" class="editplayer" name="editId" value="${player.id}">               
               <button class="edit" type = "submit" name = "submit">
                   <i style="font-size:24px" class="fa">&#xf044;</i></button>
               </form>
               &nbsp;
               <form action = "deletePlayer" method ="post">
               <input type="hidden" class="deleteplayer"  
                   name="deleteId" value="${player.id}">
               <button class="delete" type = "submit" onclick = "deletePopup()" name = "submit">
                   <i style="font-size:24px" class="fa">&#xf014;</i>
               </button>
               </form>
               <center>
               </div>
               </div>
           </c:forEach>
           </div>
           <center>
          <footer class = "playerfooter"> 
                <c:if test="${playerPaginationInfo.page gt 0}">
                    <button id = "prev" value=1 
                        onclick ="callAjax(this.value, '-1',${playerPaginationInfo.totalPages});"> 
                        Previous
                    </button>
                </c:if>
                <c:forEach begin="1" end="${playerPaginationInfo.totalPages}" var="i">
                    <button onclick="callAjax(${i}, 'content', ${playerPaginationInfo.totalPages});">${i}</button>
                </c:forEach>
                <c:if test="${playerPaginationInfo.page lt playerPaginationInfo.totalPages}">
                    <button id ="next" value =1 onclick="callAjax(this.value, '+1', ${playerPaginationInfo.totalPages});">Next</button>
                </c:if>
            </footer>
         </center>
    </body>
</html>
