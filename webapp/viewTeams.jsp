<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/viewTeamsStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="/javascript/teamPagination.js">
        </script>
    </head>
    <body>
        <div class="navbar">
            <a class="back" href="manage.jsp"><i style="font-size:50px;" class="fa fa-arrow-left"></i></a>
            <h3><center>Team Information</center></h3>
            <a class="home" href="forwardToCreateTeam"><i style="font-size:60px;float:right;" class="fa fa-plus-square"></i></a>
        </div>
        <div class = "information" align="center">         
            <c:forEach var="teamInfo" items="${teamPaginationInfo.teamsInfo}">
                <div class= "cards">
                    <div class="teamTable">
                    <table><br>
                        <tr>
                            <td>Id:</td>
                            <td><c:out value="${teamInfo.id}"/></td>
                        </tr>
                        <tr>
                            <td>Name:</td>
                            <td><a href="getTeam?teamid=${teamInfo.id}">
                            <c:out value="${teamInfo.name}" /></a></td>
                        </tr>         
                        <tr>
                            <td>country:</td>
                            <td><c:out value="${teamInfo.country}" /></td>
                        </tr>
                    </table>
                    </div>
                    <div>
                    <center>
                        <form action = "forwardToUpdatePage" method = "post">
                        <input type="hidden" class="editTeam" name="editId" value="${teamInfo.id}"/>
                            <button class="edit">
                                <i style="font-size:24px" class="fa">&#xf044;</i>
                            </button>
                        </form>
                        <form action = "deleteTeam" method = "post">
                        <input type="hidden" class="deleteTeam" name="deleteId" value="${teamInfo.id}"/>
                            <button class="delete"><i style="font-size:24px" class="fa">&#xf014;</i>
                            </button>
                        </form>
                    <center>
                    </div>
                </div>
           </c:forEach>
        </div>
        <center>
           <footer class = "teamfooter">
                <c:if test="${teamPaginationInfo.page gt 0}">
                    <button id = "prev" value=1 
                         onclick ="callAjax(this.value, '-1', ${teamPaginationInfo.totalPages});"> 
                        Previous
                    </button>
                </c:if>
                <c:forEach begin="1" end="${teamPaginationInfo.totalPages}" var="i">
                    <input type="hidden" name="currentPage" class="currentpage" value="${i}">
                    <button onclick="callAjax(${i}, 'content', ${teamPaginationInfo.totalPages});">${i}</button>
                </c:forEach>
                <c:if test="${teamPaginationInfo.page lt teamPaginationInfo.totalPages}">
                    <button id ="next" value =1 
                        onclick="callAjax(this.value, '+1', ${teamPaginationInfo.totalPages});">Next</button>
                </c:if>
            </footer>
         </center>
    </body>
</html>
            
