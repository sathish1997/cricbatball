<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Introduction</title>
        <link rel = "stylesheet" href="/css/introduction.css">
    </head>
    <body style ="width:100%;">
        <div class = "intro" align="center">
           <div>             
               <h1>Welcome To CricBatBall</h1>
                              
           </div>
        </div>        
        <div class = "option_logo">
            <form action="logOut" method="post" align="left">
                   <button>LogOut</button>
            </form>
            <div class = "manage">                
                <div class ="manageplayer"><br>
                <form action = "displayPlayers" method="get" align="center">
                    <button class = "viewbutton">
                    <img src="/images/batmanf.png" alt="player" class ="image">
                        <div class = "overlay">
                            <div class="tooltip"  align="center"> <center>Manage Player</center>
                            </div>
                        </div>
                    </button>
                </form>
                </div>
                <div class ="manageteam">
                    <form action = "fetchTeams" method="get" align="center">
                        <button class = "viewbutton">
                           <img id="teamimage" src="/images/1200px-Cricket_India_Crest.svg.png" alt="team" 
                               class ="image">
                           <div class = "overlay">
                               <div class="tooltip"> <center>Manage Team</center>
                               </div>
                          </div>
                        </button>
                    </form>
                </div>
                <div class ="managematch">
                    <form action = "getMatches" method="get" align="center">
                        <button class = "viewbutton">
                            <img src="/images/farnworth-vs-logo.png" alt="match" class ="image">
                            <div class = "overlay">
                                <div class="tooltip"> <center>Manage Match</center>
                                </div>
                            </div>
                        </button>
                    </form>
                </div>
            </div>
            <div class = "manage">  
                <div id = "outerlogo">
                    <img src = "/images/New Project.jpg" class = "logoimage">
                    <div id = "innermessage">
                        <pre>
                            <h1><center>CricBatBall</center></h1>
                            <center>CricBatBall is the web application which helps you to</center>
                            <center>Manage the Players, Teams, Matches.</center>
                            <center>You can add the player to the Teams.</center>
                            <center>You can create a match between the two teams.</center>
                        </pre>
                    </div>
                </div>
            </div>
        </div>
        <div class = "copyright"> 
            <br>
            <footer> Copyright &copy; Chennai 2019, All Rights Preserved</footer>
        </div>
    </body>
</html>
                
