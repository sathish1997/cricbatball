<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/updateMatchStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="navbar"> 
            <a href="getMatches">
            <button class="backbutton">
                 <i style="font-size:50px; color:white;" class="fa fa-arrow-left"></i>
            </button>
            </a>
        <h3><center>Team Information</center></h3>
        <a class="home" style="font-size:50px;color:white;" href="manage.jsp"><i class="fa fa-fw fa-home"></i></a>
        </div><br>
        <form:form action = "updateMatch" method="post" align="center" modelAttribute="matchInfo">
        <div class = "matchinfo">
        <table align="center">     <center>Id: ${matchInfo.id}</center><center>
                    <tr>
                    <br>
                        <td>Name:</td>
                        <td><form:input type="text" path = "name" 
                            title="enter alphabets only No Integers allowed length should be less than 20"
                            placeholder="name" value="${matchInfo.name}" maxlength="20" 
                           pattern="[A-Za-z][A-Za-z\s]*{20}" required="required"/></td>
                    </tr>
                    <tr>
                        <td>Location:</td>
                        <td><form:input type="text" path = "location" 
                            title="enter alphabets only No Integers allowed length should be less than 20"
                            placeholder="name" value="${matchInfo.location}" maxlength="20" 
                            pattern="[A-Za-z][A-Za-z\s]*{20}" required="required"/></td>
                    </tr>
                    <tr>
                        <td>Date of Play:</td>
                        <td><input type="date" name="matchDate" value="${date}" min = "2019-05-01"
                              required/></td>
                    </tr>
                    <tr>
                        <td>Starting Time:</td>
                        <td><form:input type="time" path="startingTime" value="${matchInfo.startingTime}" 
                            required="required"/></td>
                    </tr>
                    <tr>
                        <td>Match Type:</td>
                        <td><form:select path = "matchType">
                                <option selected>${matchInfo.matchType}</option>
                                <option>overs_20</option>
                                <option>overs_50</option>
                                <option>Test</option>
                            </form:select>
                        </td>
                    </tr>                    
                </table>
        </div><br> 
        <span><center>Teams In Match</center></span>
        <div class = "information" align="center">         
              <c:forEach var="team" items="${matchInfo.teams}">
                <div class= "cards">
                 <table><br>
                <tr>
                    <td>Id:</td>
                    <td><c:out value="${team.id}"/></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><c:out value="${team.name}" /></td>
                </tr>         
                <tr>
                    <td>country:</td>
                    <td><c:out value="${team.country}" /></td>
                </tr>
              </table>
              </div>
           </c:forEach>
        </div>
        <span><center>Select Two Teams To Replace</center></span>
        <div class = "information" align="center">         
              <c:forEach var="team" items="${matchPaginationInfo.teamsForMatch}">
                <div class= "cards">
                 <table><br>
                <tr>
                    <td>Id:</td>
                    <td><c:out value="${team.id}"/></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><c:out value="${team.name}" /></td>
                </tr>         
                <tr>
                    <td>country:</td>
                    <td><c:out value="${team.country}" /></td>
                </tr>
              <center> <input type = "checkbox" name = "addTeams" onclick="checkBoxLimit()" value="${team.id}">
                  select to add
              </table>
              </div>
           </c:forEach>
        </div>
        <footer>  
            <input type ="hidden" name = "matchId" value= "${matchInfo.id}">
            <button class = "viewbutton">Save Team</button>
       </footer>
        </form:form>
    </body>
</html>
