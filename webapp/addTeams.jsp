<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/addplayerstyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="/javascript/teamSelectionLimit.js">
        </script>
    </head>
    <body>
        <div class="navbar">
            <a class="back" href="getMatches"><i style="font-size:50px;" class="fa fa-arrow-left"></i></a>
            <h3><center>Add Teams To Match</center></h3>
            <a class="home" href="manage.jsp">
                <i style="font-size:60px;float:right;" class="fa fa-plus-square"></i>
            </a>
        </div>
        <form:form action="addTeams" method="post" name = "checkBoxLimit">
            <input type="hidden" name="matchId" value="${matchPaginationInfo.matchInfo.id}"/>
            <center>Id: ${matchInfo.id}<br>
            Name: <input type="text" name = "name" 
                    title="enter alphabets only No Integers allowed length should be less than 20"
                    placeholder="name" value="${matchPaginationInfo.matchInfo.name}" 
                    pattern="[A-Za-z][A-Za-z\s]*{20}" readonly>&nbsp;&nbsp;&nbsp;
            Location: <input type = "text" value="${matchPaginationInfo.matchInfo.location}"
                          readonly>&nbsp;&nbsp;&nbsp;
            Date: <input type ="date" value="${date}" readonly>&nbsp;&nbsp;&nbsp;
            Starting Time:<input type ="text" value="${matchPaginationInfo.matchInfo.startingTime}"
                              readonly>&nbsp;&nbsp;&nbsp;
            Match Type:<input type="text" value="${matchPaginationInfo.matchInfo.matchType}" readonly>
            </center><br>
            <div class = "information" align="center">
                <c:if test="${null == matchPaginationInfo.teamsForMatch}">
                    No Teams available to create match
                </c:if>        
                <c:forEach var="team" items="${matchPaginationInfo.teamsForMatch}">
                    <div class= "cards">
                        <table><br>
                            <tr>
                                <td>Id:</td>
                                <td>
                                    <c:out value="${team.id}"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Name:</td>
                                <td><c:out value="${team.name}" /></td>
                            </tr>         
                            <tr>
                                <td>country:</td>
                                <td><c:out value="${team.country}" /></td>
                            </tr>
                            <tr><td> 
                                     <input type = "checkbox" name ="teamsForMatch" 
                                         onclick = "teamSelectionLimit(2)" value = "${team.id}"> 
                                         click to add for match
                            </tr></td>
                        </table>
                    </div>
                </c:forEach>
            </div>
            <div>
                <footer>
                    <center>
                        <input type ="hidden" name = "matchId" value= "${matchPaginationInfo.matchInfo.id}">
                        <button class = "viewbutton">
                            Save Match
                        </button>
                    <center>
                </footer>
            </div>
        </form:form>
    </body>
</html>
