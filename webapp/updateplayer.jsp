<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel = "stylesheet" href = "/css/updateplayerstyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="navbar"> 
            <form action="displayPlayers" method="get">
            <button class="backbutton">
                 <i style="font-size:30px; color:white;" class="fa fa-arrow-left"></i>
            </button>
            </form>
             <span><center>Update Player</center></span>
            <a class="home" style="font-size:36px;color:white;" href="manage.jsp">
                <i class="fa fa-fw fa-home"></i>
            </a>
        </div>
        <div class="update">  
            <form:form action = "updatePlayer" method ="post"  modelAttribute="playerInfo" 
                enctype="multipart/form-data">
                <form:hidden path = "id"/>
                <form:hidden path = "contactId"/>
                <form:hidden path = "imagePath"/>
                <table align="center">
                    <tr>
                        <center><input type = "file" name="playerImage"/>
                        <td>Name:</td>
                        <td style = "margin-top =10%;"><form:input type= "text" path = "name" 
                            title="enter alphabets only No Integers allowed length should be less than 20"
                            placeholder="name" value="${playerInfo.name}" pattern="[A-Za-z][A-Za-z\s]*{20}"       
                            maxlength="20" required="required"/></td>
                    </tr>
                    <tr>
                        <td>Country:</td>
                        <td>
                            <form:select placeholder="country" default ="none" path = "country">
                                <option selected> 
                                    ${playerInfo.country}
                                </option> 
                                <option>India</option>
                                <option>Australia</option>
                                <option>South_Africa</option>
                                <option>West_Indies</option>
                                <option>New_Zealand</option>
                                <option>Pakistan</option>
                                <option>Bangladesh</option>
                                <option>Afghanistan</option>
                                <option>England</option>
                                <option>Sri_Lanka</option>
                            </form:select>   
                        </td>
                    </tr>
                    <tr>
                        <td>Role:</td>
                        <td>
                            <form:select placeholder ="role" path = "role">
                                <option selected> 
                                    ${playerInfo.role}
                                </option> 
                                <option>Batsman</option>
                                <option>Bowler</option>
                                <option>All-Rounder</option>
                                <option>Wicket-keeper</option>
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td>Batting-Style:</td>
                        <td>
                            <form:select path = "battingStyle">
                                <option selected> 
                                    ${playerInfo.battingStyle}
                                </option>  
                                <option>Right-handed</option>
                                <option>Left-handed</option>
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td>Bowling-Style:</td>
                        <td>
                            <form:select path = "bowlingStyle">
                                <option selected> 
                                    ${playerInfo.bowlingStyle}
                                </option> 
                                <option>Right Arm Fast</option>
                                <option>Left Arm Fast</option>
                                <option>Leg Spinner</option>
                                <option>Off Spinner</option>
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td>Date-Of-Birth:</td>
                        <td> <form:input type="date" path="dateOfBirth" value="${playerInfo.dateOfBirth}" 
                                 required="required"/></td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td> <form:input type="text" path = "address" placeholder = "address" 
                                 value="${playerInfo.address}" required="required"/></td>
                    </tr>
                    <tr>
                        <td>Mobile Number</td>
                        <td> <form:input type="tel" title="enter mobile number only accepted 10 digit long " 
                                path= "mobileNumber" pattern="^\d{10}$" placeholder = "Mobile Number" 
                                value="${playerInfo.mobileNumber}" required = "required"/></td>
                    </tr>
                    <tr>
                        <td>Pincode</td>
                        <td> <form:input type="tel" title="enter pincode 6 digit long only accepted " 
                                 path="pincode" pattern="^\d{6}$" placeholder = "pincode" 
                                 value ="${playerInfo.pincode}" required="required"/></td>
                    </tr>
                </table>
            <div><center>
                <button class="edit"> update </button>
                </center>
           </div>
            </form:form>
        </div>
    </body>
</html>
