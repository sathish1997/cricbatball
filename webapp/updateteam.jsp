<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/updateteamstyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="navbar"> 
            <a href="fetchTeams">
            <button type="submit" name = "submit" value = "getTeams" class="backbutton">
                 <i style="font-size:30px; color:white;" class="fa fa-arrow-left"></i>
            </button>
            </a>
             <h3><center>Team Information</center></h3>
            <a class="home" style="font-size:30px;color:white;" href="manage.jsp"><i class="fa fa-fw fa-home"></i></a>
        </div><br>
        <form:form action = "updateTeam" method="post" align="center">
        <div class = "teaminfo">
        <input type="hidden" name ="teamid" value="${teamPaginationInfo.teamInfo.id}"/>
        <center>Id: ${teamPaginationInfo.teamInfo.id}</center><center><br>
                Name: <input type="text" name = "teamname" 
                            title="enter alphabets only No Integers allowed length should be less than 20"
                            placeholder="name" value="${teamPaginationInfo.teamInfo.name}" 
                            pattern="[A-Za-z][A-Za-z\s]*{20}" 
                            maxlength="20" required/>&nbsp;&nbsp;&nbsp;
               Country: <input type = "text" name = "country" value ="${teamPaginationInfo.teamInfo.country}"
                           readonly></center>
        </div><br> 
        <span><center>Players In Team</center></span>
        <div class = "information"><br>   
              <c:forEach var="player" items="${teamPaginationInfo.teamInfo.players}">
                <div class= "cards">
                 <table>
                <tr>
                    <td>Id:</td>
                    <td><c:out value="${player.id}" /></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><c:out value="${player.name}" /></td>
                </tr>         
                <tr>
                    <td>Country:</td>
                    <td><c:out value="${player.country}" /></td>
                </tr>
                <tr>
                    <td>Role:</td>
                    <td><c:out value="${player.role}" /></td>
                </tr>
                <tr>
                    <td>Batting Style:</td>
                    <td><c:out value="${player.battingStyle}" /></td>
                </tr>
                <tr>
                    <td>Bowling Style:</td>
                    <td><c:out value="${player.bowlingStyle}" /></td>
                </tr>
                
                <center> <input type = "checkbox" name = "removePlayers" value="${player.id}">select to remove
               </table>
              </div>
           </c:forEach>
        </div>
        <span><center>Players Not In Team</center></span>
        <div class = "notTeamPlayer"><br>   
              <c:forEach var="player" items="${teamPaginationInfo.playersNotInTeam}">
                <div class= "card">
                 <table>
                <tr>
                    <td>Id:</td>
                    <td><c:out value="${player.id}" /></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><c:out value="${player.name}" /></td>
                </tr>         
                <tr>
                    <td>Country:</td>
                    <td><c:out value="${player.country}" /></td>
                </tr>
                <tr>
                    <td>Role:</td>
                    <td><c:out value="${player.role}" /></td>
                </tr>
                <tr>
                    <td>Batting Style:</td>
                    <td><c:out value="${player.battingStyle}" /></td>
                </tr>
                <tr>
                    <td>Bowling Style:</td>
                    <td><c:out value="${player.bowlingStyle}" /></td>
                </tr>
                <center> <input type = "checkbox" name = "addPlayers" value="${player.id}">select to add
               </table>
              </div>
           </c:forEach>
        </div>
        <footer>  
                        <input type ="hidden" name = "teamid" value= "${teamPaginationInfo.teamInfo.id}">
                        <button class = "viewbutton" type = "submit" name = "submit" value ="updateTeam">
                            Save Team
                        </button>
       </footer>
        </form:form>
    </body>
</html>
