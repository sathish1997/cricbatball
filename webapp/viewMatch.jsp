<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/viewMatchStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="navbar"> 
            <a href="getMatches">
            <button type="submit" name = "submit" value = "getMatches" class="backbutton">
                 <i style="font-size:50px; color:white;" class="fa fa-arrow-left"></i>
            </button>
            </a>
             <h3><center>Team Information</center></h3>
            <a class="home" style="font-size:50px;color:white;" href="manage.jsp"><i class="fa fa-fw fa-home"></i></a>
        </div><br>
        <div class = "teaminfo">
        <center>Id: ${matchInfo.id}<br>
               Name: <input type="text" name = "name" 
                            title="enter alphabets only No Integers allowed length should be less than 20" type="text"
                            placeholder="name" value="${matchInfo.name}" 
                            pattern="[A-Za-z][A-Za-z\s]*{20}" readonly>&nbsp;&nbsp;&nbsp;
               Location: <input type = "text" value="${matchInfo.location}" readonly>&nbsp;&nbsp;&nbsp;
               Date: <input type ="date" value="${date}" readonly>&nbsp;&nbsp;&nbsp;
               Starting Time:<input type ="text" value="${matchInfo.startingTime}" readonly>&nbsp;&nbsp;&nbsp;
               Match Type:<input type="text" value="${matchInfo.matchType}" readonly></center><br>
        </div>
        <div class = "information" align="center">         
              <c:forEach var="team" items="${matchInfo.teams}">
                <div class= "cards">
                 <table><br>
                <tr>
                    <td>Id:</td>
                    <td><c:out value="${team.id}"/></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><c:out value="${team.name}" /></td>
                </tr>         
                <tr>
                    <td>country:</td>
                    <td><c:out value="${team.country}" /></td>
                </tr>
              </table>
              </div>
           </c:forEach>
        </div>
      </form>
    </body>
</html>
