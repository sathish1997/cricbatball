<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel = "stylesheet" href = "/css/createMatchStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="navbar">
            <a href="getMatches">
            <button class="backbutton">
                 <i style="font-size:50px; color:white;" class="fa fa-arrow-left"></i>
            </button>
            </a>
             <span><center>Add Match</center></span>
            <a class="home" style="font-size:50px;" href="manage.jsp"><i class="fa fa-fw fa-home"></i></a>
        </div>
        <div align="center">  
            <form:form action = "createMatch" method ="post" modelAttribute="matchInfo">
                <table>
                    <tr>
                        <td>Name:</td>
                        <td><form:input type="text" path = "name" 
                            title="enter alphabets only No Integers allowed length should be less than 20"
                            placeholder="name" maxlength="20" pattern="[A-Za-z][A-Za-z\s]*{20}" 
                            required = "required"/></td>
                    </tr>
                    <tr>
                        <td>Location:</td>
                        <td><form:input type="text" path = "location" 
                            title="enter alphabets only No Integers allowed length should be less than 20"
                            placeholder="name" maxlength="20" pattern="[A-Za-z][A-Za-z\s]*{20}" 
                            required= "required"/></td>
                    </tr>
                    <tr>
                        <td>Date of Play:</td>
                        <td><input type="date" name="matchDate" min = "2019-05-01" required="required"/></td>
                    </tr>
                    <tr>
                        <td>Starting Time:</td>
                        <td><form:input type="time" path="startingTime" required= "required"/></td>
                    </tr>
                    <tr>
                        <td>Match Type:</td>
                        <td>
                            <form:select path = "matchType">
                                <option selected>overs_20</option>
                                <option>overs_50</option>
                                <option>Test</option>
                            </form:select>
                        </td>
                    </tr>
                    
                </table><br>
                    <center><button class="savebutton">Save and Continue</button></center>
            </form:form>
        </div>
    </body>
</html>
