<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/playerstyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="navbar">
            <form action="displayPlayers" method="get">
            <button type="submit" class="backbutton">
                 <i style="font-size:50px; color:white;" class="fa fa-arrow-left"></i>
            </button>
            </form>
             <h3><center>Player Information</center></h3>
            <a class="home" style="font-size:50px;color:white;" href="manage.jsp"><i class="fa fa-fw fa-home"></i></a>
        </div>
        <div class = "information">
            <center>
            <div id = "message"><br>
            <c:if test="${null != successMessage}">${successMessage}</c:if>
            <c:if test="${null != updateMessage}">${updateMessage}</c:if>
            </div>
            </center><br>
            <center><img src="${playerInfo.imagePath}"/></center>
            <table align="center">
                <tr>
                    <td>Id:</td>
                    <td><c:out value="${playerInfo.id}" /></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><c:out value="${playerInfo.name}" /></td>
                </tr>         
                <tr>
                    <td>Country:</td>
                    <td><c:out value="${playerInfo.country}" /></td>
                </tr>
                <tr>
                    <td>Role:</td>
                    <td><c:out value="${playerInfo.role}" /></td>
                </tr>
                <tr>
                    <td>Batting Style:</td>
                    <td><c:out value="${playerInfo.battingStyle}" /></td>
                </tr>
                <tr>
                    <td>Bowling Style:</td>
                    <td><c:out value="${playerInfo.bowlingStyle}" /></td>
                </tr>
                <tr>
                    <td>Date Of Birth:</td>
                    <td><c:out value="${playerInfo.dateOfBirth}" /></td>
                </tr>
                <tr>
                    <td>Age:</td>
                    <td><c:out value="${playerInfo.age}" /></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td><c:out value="${playerInfo.address}" /></td>
                </tr>
                <tr>
                    <td>Mobile Number:</td>
                    <td><c:out value="${playerInfo.mobileNumber}" /></td>
                </tr>
                <tr>
                    <td>Pincode:</td>
                    <td><c:out value="${playerInfo.pincode}" /></td>
                </tr>
            </table>
        </div>
    </body>
</html>
            
