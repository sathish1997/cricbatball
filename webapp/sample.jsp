<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "css/samplestyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="navbar">
            <a class="back" href="index.jsp"><i style="font-size:50px;" class="fa fa-arrow-left"></i></a>
            <h3><center>Player Information</center></h3>
            <a class="home" href="createplayer.jsp"><i style="font-size:60px;float:right;" class="fa fa-plus-square"></i></a>
        </div>
        <div class = "information">         
              <c:forEach var="player" items="${players}">
                <div class= "cards">
                 <table>
                <tr>
                    <td>Id:</td>
                    <td><a href="controller?submit=viewPlayer&id=${player.id}"><c:out value="${player.id}" /></a></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><c:out value="${player.name}" /></td>
                </tr>         
                <tr>
                    <td>Country:</td>
                    <td><c:out value="${player.country}" /></td>
                </tr>
                <tr>
                    <td>Role:</td>
                    <td><c:out value="${player.role}" /></td>
                </tr>
                <tr>
                    <td>Batting Style:</td>
                    <td><c:out value="${player.battingStyle}" /></td>
                </tr>
                <tr>
                    <td>Bowling Style:</td>
                    <td><c:out value="${player.bowlingStyle}" /></td>
                </tr>
                <tr>
                    <td>Date Of Birth:</td>
                    <td><c:out value="${player.dateOfBirth}" /></td>
                </tr>
            </table><div><center>
               <form action = "controller" method = "post">
                        <input type = "hidden" name= "id" value = "${player.id}"/>
                        <button class="edit" type = "submit" name = "submit" value ="getPlayer">
                            <i style="font-size:24px" class="fa">&#xf044;</i></button>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type = "hidden" name= "id" value = "${player.id}"/>
                        <button class="delete" type = "submit" name = "submit" 
                            onclick="alert('player deleted successfully')" 
                            value ="deletePlayer"><i style="font-size:24px" class="fa">&#xf014;</i></button>
              </form> <center> </div>
              </div>
           </c:forEach>
           </div>
           <center>
           <footer class = "playerfooter">
                <c:if test="${currentPage ne 1}">
                <a href="controller?submit=viewPlayers&page=${currentPage - 1}">Previous</a>
                </c:if>
                <table border="1" cellpadding="5" cellspacing="5">
                    <tr>
                        <c:forEach begin="1" end="${totalPages}" var="i">
                            <c:choose>
                                <c:when test="${currentPage eq i}">
                                    <td>${i}</td>
                                </c:when>
                            <c:otherwise>
                                <td><a href="controller?submit=viewPlayers&page=${i}">${i}</a></td>
                            </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </tr>
                </table>
                <c:if test="${currentPage lt totalPages}">
                    <a href="controller?submit=viewPlayers&page=${currentPage + 1}">Next</a>
                </c:if>
            </footer>
         </center>
    </body>
</html>
            
