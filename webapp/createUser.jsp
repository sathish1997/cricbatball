<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="jstl-core" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel = "stylesheet" href = "/css/createplayerstyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <h1><center>User Details</center></h1>
        <div align="center">  
            <form:form action = "createUser" method ="post" modelAttribute="userInfo">
                <c:if test="${duplicate != null}">${duplicate}</c:if><br>
                <table>
                    <tr>
                        <br>
                        <td>Name</td>
                        <td style = "margin-top =10%;"><form:input type="text" path = "name" 
                            title="enter alphabets only No Integers allowed length should be less than 20"
                            placeholder="name" maxlength="20" pattern="[A-Za-z][A-Za-z\s]*{20}" 
                            required="required"/></td>
                    </tr>
                    <tr>
                        <td>Email Id</td>
                        <td> <form:input type="email" path="emailId" required="required"/></td>
                    </tr>
                    <tr>
                        <td>Role</td>
                        <td> <form:input type="text" path = "role" value="admin" placeholder = "select Role"
                                 required="required"/></td>
                    </tr>
                    <tr>
                        <td>Mobile Number</td>
                        <td> <form:input type="tel" title="enter mobile number only accepted 10 digit long " 
                                 path= "mobileNumber" pattern="^\d{10}$" placeholder = "Mobile Number" 
                                 required="required"/></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td><form:radiobutton path ="gender" value="Male"/>Male
                            <form:radiobutton path ="gender" value="Female"/>Female</td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><form:input type="password" path ="password" placeHolder="password" 
                                pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number 
                                and one uppercase and lowercase letter, and at least 8 or more characters" 
                                required="required"/>
                        </td>
                    </tr>
                </table>
                <center>
                    <button class="savebutton">save</button>
                </center>
            </form:form>
        </div>
    </body>
</html>
