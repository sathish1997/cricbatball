<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel = "stylesheet" href = "/css/scoreCardPage.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="/javascript/checkBoxLimitBowler.js">
        </script>
    </head>
    <body>
        <div class="navbar">
            <a class="back" href="manage.jsp">
                 <i style="font-size:50px;" class="fa fa-arrow-left"></i>
            </a>
            <h3>Select Bowler</h3>
            <a class="home" href="manage.jsp">
                <i style="font-size:60px;float:right;" class="fa fa-plus-square"></i>
            </a>
        </div>
        <center>
        <form:form action="continuePlay" name ="checkBoxLimit" method ="post" modelAttribute="overDetailInfo">
            <center>${matchPaginationInfo.secondTeam.name}</center><br>
                <table>
                    <c:forEach var="player" items="${teamInfo.players}">
                       <c:if test="${overDetailInfo.bowler.id != player.id}">
                        <tr>
                            <td> ${player.name} </td>
                            <td> ${player.bowlingStyle} </td>
                             <td> <input type ="checkbox" name ="bowlerIds" onclick = "bowlerCheckBoxLimit(1)" 
                                          value ="${player.id}"/> </td>
                         </tr>
                         </c:if>
                    </c:forEach>
                </table>
            <form:hidden path = "firstBatsman.id"/>
            <form:hidden path = "secondBatsman.id"/>
            <form:hidden path = "overInfo.overNumber"/>
            <form:hidden path = "overInfo.id"/>
            <form:hidden path = "bowler.id"/>
            <form:hidden path = "firstTeam.id"/>
            <form:hidden path = "secondTeam.id"/>
            <form:hidden path = "overInfo.match.id"/>
            <form:hidden path = "ballNumber"/>
            <form:hidden path = "firstPlayerRuns"/>
            <form:hidden path = "secondPlayerRuns"/>
            <form:hidden path = "firstPlayerballs"/>
            <form:hidden path = "secondPlayerBalls"/>
            <form:hidden path = "firstPlayerSixes"/>
            <form:hidden path = "firstPlayerFours"/>
            <form:hidden path = "secondPlayerSixes"/>
            <form:hidden path = "secondPlayerFours"/>
            <form:hidden path = "scoreCountInfo"/>
            <form:hidden path = "wicketCountInfo"/>
            <form:hidden path = "extrasCountInfo"/>
            <input type = "submit" value = "Resume Play"/>       
        </form:form>
        </center>
    </body>
</html>
